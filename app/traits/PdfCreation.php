<?php
/**
 * Trait for pdf helper functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
trait PdfCreation {

    /**
     * Creates String for the timetable headline
     *
     * @param $startdate
     * @param $enddate
     * @param $overtime
     * @return string
     */
    private function buildDateHeadline($startdate, $enddate, $overtime)
    {
        $startdate = strtotime($startdate);
        $enddate = strtotime($enddate);

        $weekday = date("l, d. F Y", $startdate);
        $weekdayNum = date("d_m_Y", $startdate);
        $starttime = date("H.i", $startdate);
        $endtime = date("H.i", $enddate);


        setlocale(LC_ALL, 'de_DE@euro');
        $weekday = strftime("%A, %d %B %Y", $startdate);
        $weekday = $this->changeWeekday($weekday);

        $overtimeStr = "";
        if ($overtime > 0)
            $overtimeStr = " + $overtime Stunden";
        elseif ($overtime < 0)
            $overtimeStr = " $overtime Stunden";

        return [
            "headline" => "$weekday ($starttime bis $endtime)$overtimeStr",
            "date" => $weekdayNum
        ];
    }

    /**
     * Creates the PDF file
     *
     * @param $data : data for the pdf file
     */
    private function createPDF($data)
    {
        $pdf = new PdfSheet($data['date_string'], $data['tasks'], $data['complete_hours'], $data['username'], $data['number']);

        $pdf->createSheet($data['filename']);
    }

}