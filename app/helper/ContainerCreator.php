<?php

/**
 * Helper class to initialize the container of the application
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class ContainerCreator
{

    /**
     * Application container
     *
     * @var \Interop\Container\ContainerInterface
     */
    private $container;

    /**
     * Utils constructor.
     *
     * @param $container
     */
    function __construct(Interop\Container\ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Creates Slim container for storing general classes
     *
     * @param $config
     * @return Interop\Container\ContainerInterface
     */
    public function initContainer($config)
    {
        $this->container['config'] = $config;

        $this->container['logger'] = $this->initLogger();

        $this->container['loader'] = $this->initMainDatabase();

        $this->container['ftp'] = $this->initFtp();

        $this->container['mail'] = $this->initMail();

        $this->container['view'] = function () {
            return new Slim\Views\PhpRenderer("public/dist/");
        };

        return $this->container;
    }

    /**
     * Creates Monolog logger class
     *
     * @return \Monolog\Logger
     */
    private function initLogger()
    {
        $logger = new \Monolog\Logger('logger');
        $file_handler = new \Monolog\Handler\StreamHandler($this->container->get("config")['primaryLogFile']);

        $logger->pushHandler($file_handler);
        $logger->addNotice("Logger activated.");

        return $logger;
    }

    /**
     * Creates class for PDO Database connection
     *
     * @return Contentloader
     */
    private function initMainDatabase()
    {
        try {
            $con = $this->container->get('config')['main-DB'];

            $pdo = new MysqlPdo($con);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            if ($this->container->get("logger"))
                $this->container->get("logger")->addNotice("Database PDO object created.");

            $loader = new Contentloader($pdo, $this->container->get("logger"));

            return $loader;
        } catch (Exception $e) {
            if ($this->container->get("logger"))
                $this->container->get("logger")->addCritical("Cannot create database connection", array(
                    'message' => $e->getMessage(),
                    'stack_trace' => $e->getTraceAsString()
                ));
            return null;
        }
    }

    /**
     * Creates class for FTP connection
     *
     * @return SFTP
     */
    private function initFtp()
    {
        try {
            $con = $this->container->get('config')['ftp-upload'];

            $ftp = new SFtp($con['host'], $con['user'], $con['pass']);

            if ($this->container->get("logger"))
                $this->container->get("logger")->addNotice("FTP object created.");

            return $ftp;
        } catch (Exception $e) {
            if ($this->container->get("logger"))
                $this->container->get("logger")->addCritical("Cannot create FTP object", array(
                    'message' => $e->getMessage(),
                    'stack_trace' => $e->getTraceAsString()
                ));

            return null;
        }
    }

    /**
     * Creates class to send mails
     *
     * @return PHPMailer
     */
    private function initMail()
    {
        try {
            $con = $this->container->get('config')['smtp-mail'];

            $mail = new \PHPMailer();

            /* Mailserver connection */
            $mail->isSMTP();
            $mail->Host = $con['host'];
            $mail->SMTPAuth = true;
            $mail->Username = $con['user'];
            $mail->Password = $con['pass'];
            $mail->SMTPSecure = 'tls';
            $mail->Port = $con['port'];
            $mail->SMTPDebug = 0;
            $mail->setLanguage('de', 'private/php/classes/PHPMailer/language/phpmailer.lang-de.php');
            $mail->CharSet = 'UTF-8';

            if ($this->container->get("logger"))
                $this->container->get("logger")->addNotice("Mail object created.");

            return $mail;
        } catch (Exception $e) {
            if ($this->container->get("logger"))
                $this->container->get("logger")->addCritical("Cannot create Mail object", array(
                    'message' => $e->getMessage(),
                    'stack_trace' => $e->getTraceAsString()
                ));

            return null;
        }
    }
}