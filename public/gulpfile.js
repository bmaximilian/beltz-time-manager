/**
 * Script to build the JavaScript application with gulp
 *
 * @author <m.beck@beltz.de>
 */

var gulp = require('gulp'),
    gulpUtil = require('gulp-util'),
    webpack = require('webpack'),
    webserver = require('gulp-webserver'),
    inject = require('gulp-inject'),
    sass = require('gulp-sass'),
    cleanCss = require('gulp-clean-css'),
    uglyfly = require('gulp-uglyfly'),
    htmlMinifier = require('gulp-html-minifier2'),
    Server = require('karma').Server;

var webpackConfig = {
    context: __dirname,
    entry: {
        'build': [
            __dirname + '/src/main.js'
        ]
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/dist/js',
        publicPath: '/private/ngapp/dist/js/'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: require.resolve('babel-loader'),
                query: {
                    presets: [
                        require.resolve('babel-preset-es2015')
                    ]
                }
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ],
    node: {
        net: 'empty',
        dns: 'empty',
        fs: 'empty'
    }
};

gulp.task('test-javascript', ['webpack'], function (done) {
    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();

    gulpUtil.log('[karma]', 'Javascript tested');
});

gulp.task('compile-sass', function () {
    gulp.src(__dirname + '/src/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(__dirname + '/dist/css/compiled'));


});

gulp.task('minify-css', ['compile-sass'], function () {
    gulp.src(__dirname + '/dist/css/compiled/*.css')
        .pipe(cleanCss({
            debug: true,
            compatibility: 'ie11'
        }))
        .pipe(gulp.dest(__dirname + '/dist/css/minified'));

    gulpUtil.log('[cleanCSS]', 'CSS compressed');
});

gulp.task('webpack', function (done) {
    var localConfig = Object.create(webpackConfig);

    localConfig.output.filename = '[name].js';
    localConfig.devtool = 'source-map';
    localConfig.debug = false;

    webpack(localConfig).run(function (err, stats) {
        if (err) throw new gulpUtil.PluginError('webpack:dev', err);

        gulpUtil.log('[webpack:dev]', stats.toString({
            colors: true
        }));

        done();
    });
});

gulp.task('minify-js', ['webpack'], function () {
    gulp.src(__dirname + '/dist/js/*.js')
        .pipe(uglyfly())
        .pipe(gulp.dest(__dirname + '/dist/js/minified'));

    gulpUtil.log('[uglify]', 'Javascript compressed');
});

gulp.task('inject', ['minify-js', 'minify-css'], function () {
    return gulp.src(__dirname + '/src/index.html')
        .pipe(inject(gulp.src(
            [
                __dirname + '/dist/js/*.js',
                __dirname + '/dist/css/compiled/*.css'
            ], {
                read: false,
                cwd: __dirname + '/dist'
            })))
        .pipe(gulp.dest(__dirname + '/dist'));
});

gulp.task('minify-html', ['inject'], function () {
    gulp.src(__dirname + '/src/**/views/*.html')
        .pipe(htmlMinifier({collapseWhitespace: true}))
        .pipe(gulp.dest(__dirname + '/dist/views'));

    gulpUtil.log('[htmlMinifier]', 'HTML Templates compressed');

    gulp.src(__dirname + '/dist/index.html')
        .pipe(htmlMinifier({collapseWhitespace: true}))
        .pipe(gulp.dest(__dirname + '/dist'));

    gulpUtil.log('[htmlMinifier]', 'index.html compressed');
});

gulp.task('serve', function () {
    gulp.src('dist')
        .pipe(webserver({
            livereload: true,
            open: true
        }));
});

gulp.task('default', [
    'compile-sass',
    'minify-css',
    'webpack',
    'test-javascript',
    'minify-js',
    'inject',
    'minify-html'
]);

gulp.task('build:js', [
    'webpack',
    'minify-js'
]);
gulp.task('build:js:test', [
    'webpack',
    'test-javascript',
    'minify-js'
]);
gulp.task('build:css', [
    'compile-sass',
    'minify-css'
]);
gulp.task('build:html', [
    'inject',
    'minify-html'
]);