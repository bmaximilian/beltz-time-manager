<?php
include_once '../app/traits/DatabaseUser.php';

/**
 * Class DatabaseUserTest
 *
 * Test class for DatabaseUserTrait
 */
class DatabaseUserTest extends PHPUnit_Framework_TestCase
{
    use DatabaseUser;

    private $user = [
        [
            "firstname" => "Homer",
            "secondFirstname" => "J",
            "lastname" => "Simpson"
        ], [
            "firstname" => "Bart",
            "lastname" => "Simpson"
        ], [
            "prefix" => "Prof.",
            "firstname" => "John",
            "lastname" => "Frink"
        ]
    ];

    /**
     * Test for resolving the right name
     */
    public function testNameCreation()
    {

        $this->assertEquals(
            "Homer J Simpson",
            $this->getNameForUser($this->user[0])
        );

        $this->assertEquals(
            "Bart Simpson",
            $this->getNameForUser($this->user[1])
        );

        $this->assertEquals(
            "Prof. John Frink",
            $this->getNameForUser($this->user[2])
        );
    }
}
