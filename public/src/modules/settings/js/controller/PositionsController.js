import Helper from '../../../app/js/classes/Helper.class'
import PositionsDialogController from '../controller/PositionsDialogController'

var allPositions = [];

/**
 * controller for the position management of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdDialog, $mdToast, BaseUrl, PositionsFactory) {
    "use strict";

    $scope.positionsToggle = false;
    $scope.positions = [];

    PositionsFactory.getPositions(null, function (data) {
        $scope.positions = data;
    });

    $scope.changePositionsToggle = function () {
        $scope.positionsToggle = ($scope.positionsToggle ? false : true);
    };

    if (!$scope.text)
        $scope.text = {};
    $scope.text.positionsSearchInput = '';

    $scope.changePositionsSearch = function () {
        if ($scope.text.positionsSearchInput.length > 0) {
            if (allPositions.length === 0)
                allPositions = $scope.positions;

            $scope.positions = allPositions.filter(function (position) {
                if (position.description && position.departmentDescription)
                    return position.name.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase())
                        || position.description.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase())
                        || position.departmentName.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase())
                        || position.departmentDescription.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase());
                else if (!position.departmentDescription)
                    return position.name.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase())
                        || position.description.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase())
                        || position.departmentName.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase());
                else if (!position.description)
                    return position.name.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase())
                        || position.departmentName.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase())
                        || position.departmentDescription.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase());
                else
                    return position.name.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase())
                        || position.departmentName.toLowerCase().includes($scope.text.positionsSearchInput.toLowerCase());

            });
        } else {
            $scope.positions = allPositions;
        }
    };

    $scope.openPositionsEditDialog = function (ev, position) {
        $mdDialog.show({
            controller: PositionsDialogController,
            templateUrl: BaseUrl.viewPath + "settings/views/positionsDialog.html",
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                position: position,
                action: 'edit'
            }
        })
            .then(function (answer) {
                $scope.positions = answer.data;
                Helper.standardToast($mdToast, answer.message, 3000, "bottom left");
            });
    };

    $scope.openPositionsAddDialog = function (ev) {
        $mdDialog.show({
            controller: PositionsDialogController,
            templateUrl: BaseUrl.viewPath + "settings/views/positionsDialog.html",
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                position: null,
                action: 'add'
            }
        })
            .then(function (answer) {
                $scope.positions = answer.data;
                Helper.standardToast($mdToast, answer.message, 3000, "bottom left");
            });
    };

    $scope.deletePosition = function (id) {
        PositionsFactory.deletePosition(id, function () {
            PositionsFactory.getPositions(null, function (positions) {
                $scope.positions = positions;
                Helper.standardToast($mdToast, "Stelle erfolgreich gelöscht", 3000, "bottom left");
            });
        });
    };

}