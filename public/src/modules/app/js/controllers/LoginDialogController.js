import Helper from '../classes/Helper.class'

/**
 * controller for the login function of the app
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdDialog, $mdToast, Login) {
    "use strict";
    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.user = {
        email: '',
        password: ''
    };

    $scope.validEmail = function () {
        var re = /^\S+@\S+\.\S+$/;
        return $scope.user.email && $scope.user.email.length > 0;// && re.test($scope.user.email);
    };
    $scope.validPassword = function () {
        return $scope.user.password && $scope.user.password.length > 0;
    };

    $scope.login = function () {
        if ($scope.validEmail() && $scope.validPassword())
            Login.login($scope.user.email, $scope.user.password, function (user) {
                if (user) {
                    $mdDialog.hide(user);
                } else {
                    Helper.standardToast($mdToast, "Login fehlgeschlagen");
                }
            });
    }
}