import ControllerBase from '../../src/modules/daysheets/js/classes/ControllerBase'

/**
 * Test JavaScript file for main application container
 *
 * @author <m.beck@beltz.de>
 */
describe("ControllerBase", function () {

    it("Returns a storage item", function () {

        const test = {
            "message": "hallo"
        };
        localStorage.setItem("test", JSON.stringify(test));

        let item = ControllerBase._getStorageItem("test");

        localStorage.clear();

        let item2 = ControllerBase._getStorageItem("test");

        expect(item.message).toBe("hallo");

        expect(item2).toBe(null);
    });

});