import LoginDialogController from './LoginDialogController'
import OwnUserController from '../../../settings/js/controller/OwnUserController'

/**
 * controller for the angular material menu navigation
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $location, $mdDialog, $mdToast, $interval, Login, BaseUrl) {

    $scope.logout = function () {
        Login.logout(function (data) {
            var msg = "Logout fehlgeschlagen";

            if (data) {
                msg = "Logout erfolgreich";
                $location.path('/').replace();
            }

            $mdToast.show(
                $mdToast
                    .simple()
                    .textContent(msg)
                    .position('top left')
                    .hideDelay(3000)
                    .parent(angular.element(document.getElementById('content')))
            );
        });
    };

    $interval(function () {
        var tempUser = null;

        try {
            tempUser = JSON.parse(localStorage.getItem("activeUser"));

            if (tempUser) {
                let expire = new Date(tempUser.tokenExpire);
                let now = new Date();

                let minDifference = Math.ceil((expire.getTime() - now.getTime()) / (1000 * 60));

                if (minDifference < 1) {
                    Login.refresh(function (user) {
                        if (!user) {
                            $scope.logout();
                        }
                    });
                }
            }
        } catch (e) {
        }
    }, 1000);

    $scope.showLoginDialog = function (ev) {
        $mdDialog.show({
            controller: LoginDialogController,
            templateUrl: BaseUrl.distPath + 'views/modules/app/views/loginDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        })
            .then(function (answer) {
                if (answer) {
                    $mdToast.show(
                        $mdToast
                            .simple()
                            .textContent('Wilkommen ' + answer.name)
                            .position('top right')
                            .hideDelay(3000)
                            .parent(angular.element(document.getElementById('content')))
                    );
                }
            });
    };

    $scope.showEditOwnUserDialog = function (ev) {
        if (OwnUserController) {
            $mdDialog.show({
                controller: OwnUserController,
                templateUrl: BaseUrl.distPath + 'views/modules/settings/views/ownUserDialogView.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                multiple: true,
                locals: {
                    user: $scope.user
                }
            })
                .then(function (answer) {
                    if (answer) {
                        $scope.user = answer.data;
                        localStorage.setItem("activeUser", JSON.stringify(answer.data));

                        $mdToast.show(
                            $mdToast
                                .simple()
                                .textContent(answer.message)
                                .position('top right')
                                .hideDelay(3000)
                                .parent(angular.element(document.getElementById('content')))
                        );
                    }
                });
        }
    }
}