import Helper from '../../../app/js/classes/Helper.class'

/**
 * Factory to store the positions data
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default function ($mdToast, $http, BaseUrl) {
    var positions = [];

    $http.get(BaseUrl.baseAjax + "/api/positions").then(function (response) {
        if (response && response.data && response.data.length > 0) {
            positions = response.data;
        }
    }, Helper.connectionError($mdToast, "Stellen konnten nicht erfasst werden"));


    /**
     * Returns all positions or the position specified by id
     *
     * @param id
     * @param callback
     */
    function getPositions(id, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/positions";
        if (id)
            apiRoute += "/" + id;

        $http.get(apiRoute).then(function (response) {
            if (response && response.data) {
                positions = response.data;
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Stellen konnten nicht erfasst werden"));
    }


    /**
     * Inserts a position
     *
     * @param data
     * @param callback
     */
    function insertPosition(data, callback) {
        "use strict";

        var apiRoute = BaseUrl.baseAjax + "/api/positions";

        $http.post(apiRoute, data).then(function (response) {
            if (response && response.data) {
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Stelle konnt nicht hinzugefügt werden"));
    }


    /**
     * Updates a position specified by id
     *
     * @param id
     * @param data
     * @param callback
     */
    function updatePosition(id, data, callback) {
        "use strict";

        var apiRoute = BaseUrl.baseAjax + "/api/positions/" + id;

        if (id)
            $http.put(apiRoute, data).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Stelle konnte nicht geändert werden"));
    }


    /**
     * Deletes a position specified by id
     *
     * @param id
     * @param callback
     */
    function deletePosition(id, callback) {
        "use strict";

        var apiRoute = BaseUrl.baseAjax + "/api/positions/" + id;

        if (id)
            $http.delete(apiRoute).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Stelle konnte nicht gelöscht werden"));
    }

    return {
        positions: function () {
            return positions;
        },

        getPositions: getPositions,

        insertPosition: insertPosition,

        updatePosition: updatePosition,

        deletePosition: deletePosition
    }
}