<?php


use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for departments database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class DepartmentsController extends RestController
{

    /**
     * GET function for departments
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        if ($id)
            $departments = $this->loader->select("departments", [
                "pk" => "id",
                "designation" => "name",
                "description"
            ], [
                "pk" => $id
            ]);
        else
            $departments = $this->loader->select("departments", [
                "pk" => "id",
                "designation" => "name",
                "description"
            ]);

        $outDepartments = array();
        foreach ($departments as $department) {
            $positions = $this->loader->select("positions", [
                "pk" => "id",
                "designation" => "name",
                "description"
            ], [
                "department_pk" => $department['id']
            ]);

            $department['positions'] = $positions;
            array_push($outDepartments, $department);
        }

        return $response->getBody()->write(json_encode($outDepartments));
    }

    /**
     * POST function for departments
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        $insert_data = array(
            "designation" => @$request->getParsedBody()['name'],
            "description" => @$request->getParsedBody()['description']
        );
        $positions = @$request->getParsedBody()['positions'];

        $department = $this->loader->insert("departments", $insert_data);

        $newPositions = [];
        if ($positions)
            foreach ($positions as $position) {
                array_push($newPositions, $this->loader->update("positions", [
                    "department_pk" => $department['pk']
                ], [
                    "pk" => $position['id']
                ]));
            }
        $department['positions'] = $newPositions;

        return $response->getBody()->write(json_encode($department));
    }


    /**
     * PUT function for departments
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function put(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        $insert_data = array(
            "designation" => @$request->getParsedBody()['name'],
            "description" => @$request->getParsedBody()['description']
        );
        $positions = @$request->getParsedBody()['positions'];

        $department = $this->loader->update("departments", $insert_data, [
            "pk" => $id
        ]);

        $newPositions = [];
        if ($positions)
            foreach ($positions as $position) {
                array_push($newPositions, $this->loader->update("positions", [
                    "department_pk" => $department['pk']
                ], [
                    "pk" => $position['id']
                ]));
            }
        $department['positions'] = $newPositions;

        return $response->getBody()->write(json_encode($department));
    }

    /**
     * DELETE function for departments
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function delete(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        $department = $this->loader->delete("departments", [
            "pk" => $id
        ]);

        return $response->getBody()->write(json_encode($department));
    }
}