<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for REST API database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
abstract class RestController extends DatabaseBaseController
{
    /**
     * Function for a REST API get route
     *
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public abstract function get(Request $request, Response $response);

    /**
     * Function for a REST API post route
     *
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public abstract function post(Request $request, Response $response);

    /**
     * Function for a REST API put route
     *
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public abstract function put(Request $request, Response $response);

    /**
     * Function for a REST API delete route
     *
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public abstract function delete(Request $request, Response $response);
}