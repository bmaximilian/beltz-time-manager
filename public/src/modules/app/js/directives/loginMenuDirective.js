import LoginMenuController from '../controllers/LoginMenuController'

/**
 * directive for the login menu
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function (BaseUrl, Login) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: BaseUrl.viewPath + "app/views/loginMenu.html",
        controller: LoginMenuController,
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                scope.user = Login.user();
            });
        }
    };
}