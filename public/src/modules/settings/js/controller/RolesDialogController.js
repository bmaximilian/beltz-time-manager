import Helper from '../../../app/js/classes/Helper.class';

/**
 * controller for the role management functions of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdDialog, role, action, RolesFactory) {
    "use strict";

    if (action === 'edit')
        $scope.headline = "Bearbeite Rolle";
    else if (action === 'add')
        $scope.headline = "Füge Rolle hinzu";

    if (role)
        $scope.role = role;

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    function validate() {
        var valid = false;
        if (Helper.isEmpty($scope.rolesForm.$error))
            valid = true;
        else if ($scope.rolesForm.$error.required)
            valid = ($scope.rolesForm.$error.required.length == 0);
        else if ($scope.rolesForm.$error.pattern)
            valid = $scope.rolesForm.$error.pattern.length == 0;
        return valid;
    }

    $scope.submit = function () {
        if (validate()) {
            if (action === 'edit') {
                RolesFactory.updateRoles($scope.role.id, $scope.role, function (data) {
                    RolesFactory.getRoles(null, function (data) {
                        $mdDialog.hide({
                            message: 'Rolle erfolgreich bearbeitet',
                            data: data
                        });
                    });
                });
            }

            else if (action === 'add') {
                RolesFactory.insertRoles($scope.role, function (data) {
                    RolesFactory.getRoles(null, function (data) {
                        $mdDialog.hide({
                            message: 'Rolle erfolgreich hinzugefügt',
                            data: data
                        });
                    });
                });
            }
        }
    };
}