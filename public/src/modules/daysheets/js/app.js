/**
 * Daysheets application context which loads all required modules
 * and configurations for the daysheets module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
import angAccordion from 'angular-material-accordion'

import DaysheetFactory from './services/DaysheetFactory'
import JobsFactory from './services/JobsFactory'
import MaterialsFactory from './services/MaterialsFactory'
import CostcentersFactory from './services/CostcentersFactory'
import currentSheetDirective from './directives/currentSheetDirective'
import welcomeUserDirective from './directives/welcomeUserDirective'
import oldSheetsDirective from './directives/oldSheetsDirective'
import allSheetsDirective from './directives/allSheetsDirective'

var appName = "main.daysheets";

angular.module(appName, ["angAccordion"])
    .factory('DaysheetFactory', DaysheetFactory)
    .factory('JobsFactory', JobsFactory)
    .factory('MaterialsFactory', MaterialsFactory)
    .factory('CostcentersFactory', CostcentersFactory)
    .directive("currentSheet", currentSheetDirective)
    .directive("oldSheets", oldSheetsDirective)
    .directive("allSheets", allSheetsDirective)
    .directive("welcomeUser", welcomeUserDirective);

export default appName