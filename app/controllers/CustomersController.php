<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for jobs database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class CustomersController extends RestController
{

    /**
     * Selects all customers or the customer by id
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return int
     */
    public function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $where = null;

        if ($id) {
            $where = [
                "pk" => "id"
            ];
        }

        return $this->loader->select("customers", [
            "pk" => "id",
            "designation" => "name",
            "description" => "description"
        ], $where);
    }

    /**
     * POST function for jobs
     * (not available)
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return int
     */
    public function post(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * PUT function for jobs
     * (not available)
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return int
     */
    public function put(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * DELETE function for jobs
     * (not available)
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return int
     */
    public function delete(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }
}