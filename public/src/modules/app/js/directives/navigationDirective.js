/**
 * directive for the navigation menu
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function (BaseUrl, routeNavigationFactory, Login) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: BaseUrl.viewPath + "app/views/navigation.html",
        controller: function ($scope, $location) {
            $scope.routes = routeNavigationFactory.routes;
            $scope.activeRoute = routeNavigationFactory.activeRoute;
            $scope.requiredPower = routeNavigationFactory.authenticationRequired;
            $scope.redirect = function (path) {
                $location.path(path).replace();
            };
        },
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                scope.isLoggedIn = Login.user().isLoggedIn;
                scope.power = (Login.user().powers ? Login.user().powers.CHANGE_PATH : 0);
            });
        }
    };
}