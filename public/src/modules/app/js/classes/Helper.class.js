/**
 * Class with generalized helper functions
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default class Helper {

    /**
     * returns a function to display a Toast with the submitted message
     *
     * @param $mdToast
     * @param message
     * @returns {Function}
     */
    static connectionError($mdToast, message) {
        "use strict";
        if ($mdToast)
            return function () {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(message)
                        .position('bottom center')
                        .hideDelay(60000)
                        .parent(angular.element(document.getElementById('content')))
                );
            };
        else
            return function () {
                console.log("Database connection error");
            };
    }

    /**
     * Displays a toast in the ng-view
     *
     * @param $mdToast
     * @param message
     * @param time
     * @param position
     */
    static standardToast($mdToast, message, time = 3000, position = "top left") {
        $mdToast.show(
            $mdToast.simple()
                .textContent(message)
                .position(position)
                .hideDelay(time)
                .parent(angular.element(document.getElementById('content')))
        );
    }

    /**
     * Checks if the passed object ist empty
     * @param map
     * @returns {boolean}
     */
    static isEmpty(map) {
        for (var key in map) {
            return !map.hasOwnProperty(key);
        }
        return true;
    }

    /**
     * Cleans an object
     * @param map
     */
    static cleanup(map) {
        for (var key in map) {
            if (key != null)
                if (map[key] == null || map[key].toString().length == 0)
                    delete map[key];
        }
        return map;
    }

    /**
     * Validates an AngularJs Form
     * @param form
     * @returns {boolean}
     */
    static validateForm(form) {
        var valid = false;
        if (this.isEmpty(form.$error))
            valid = true;
        else {
            if (form.$error.required)
                valid = (form.$error.required.length == 0);
            else if (form.$error.pattern)
                valid = form.$error.pattern.length == 0;
        }
        return valid;
    }

}