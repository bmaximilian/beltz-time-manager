<?php

use Monolog\Logger;

/**
 * Helper class for loading database content
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class Contentloader
{

    /**
     * MySQL PDO database
     * @var MysqlPdo|null
     */
    private $_db = null;

    /**
     * Monolog logger
     * @var Logger|null
     */
    private $_logger = null;

    /**
     * Contentloader constructor.
     *
     * @param MysqlPdo $database
     * @param Logger $logger
     */
    public function __construct(MysqlPdo $database = null, Logger $logger = null)
    {
        if ($database)
            $this->_db = $database;

        if ($logger)
            $this->_logger = $logger;
    }

    /**
     * Executes an SQL query
     *
     * @param String $query
     * @return array|bool
     */
    public function query($query)
    {
        return $this->execute_statement($query);
    }

    /**
     * Executes SQL sataement and fetches the result to hash map
     * If there is no result a boolean with the success state of the request
     * will be returned.
     *
     * @param $query : String
     * @param $returnLast
     * @return array|bool
     */
    private function execute_statement($query, $returnLast = false)
    {
        if ($this->_logger)
            $this->_logger->addInfo("Bereite PDO Query vor: $query");

        $statement = $this->_db->prepare($query);
        $success = $statement->execute();

        if ($this->_logger)
            $this->_logger->addInfo("PDO Query erfolgreich");

        try {
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);

            if ($returnLast && sizeof($results) > 0)
                $results = $results[0];

        } catch (Exception $e) {
            $results = $success;
        }
        return $results;
    }

    /**
     * Executes an INSERT query and returns the inserted data set
     *
     * @param String $table
     * @param array $data
     * @return array|bool|null
     */
    public function insert($table, Array $data)
    {
        $return = null;
        if ($table && $data) {
            $insertQuery = $this->buildInsert($table, $data);

            if ($this->execute_statement($insertQuery)) {

                $selectQuery = $this->buildSelect($table, null, $data, false);


                $return = $this->execute_statement($selectQuery, true);
            }
        }
        return $return;
    }

    /**
     * Prepares an SQL INSERT statement.
     * Second parameter has to be an HashMap with the table columns
     *
     * @param $table : String
     * @param $data : HashMap
     * @return string
     */
    private function buildInsert($table, $data)
    {
        $q = "";

        if ($table && $data) {
            $columns = $values = "";

            $count = 0;
            foreach ($data as $key => $value) {
                if ($value) {
                    if ($count > 0) {
                        $columns .= ", ";
                        $values .= ", ";
                    }
                    $columns .= $table . "." . $key;
                    $values .= "'$value'";

                    $count++;
                }
            }

            if (strlen($columns) > 0 && strlen($values) > 0)
                $q = "INSERT INTO $table ($columns) VALUES ($values);";
        }
        return $q;
    }

    /**
     * Returns simple select statement
     *
     * @param String $table
     * @param array|null $rows
     * @param array|null $condition
     * @return null|string
     */
    public function buildSelect($table, $rows = null, $condition = null, $sensitive = true)
    {
        $selectQuery = null;
        if ($table) {
            $rowsQuery = $this->buildSelectRows($table, $rows);

            $whereQuery = $this->buildWhere($table, $condition, $sensitive);

            $selectQuery = "SELECT $rowsQuery FROM $table $whereQuery;";

        }
        return $selectQuery;
    }

    /**
     * Builds string to select specific rows (also with alias)
     *
     * @param String $table
     * @param array $rows
     * @return string
     */
    private function buildSelectRows($table, $rows)
    {
        $rowsQuery = '';

        if (!$rows)
            $rowsQuery = '*';
        else {
            $count = 0;

            foreach ($rows as $row => $alias) {
                if ($count > 0)
                    $rowsQuery .= ", ";


                if (gettype($row) == "string")
                    $rowsQuery .= "$table.$row AS $alias";
                else
                    $rowsQuery .= "$table.$alias";


                $count++;
            }
        }

        return $rowsQuery;
    }

    /**
     * Returns WHERE query with conditions
     *
     * @param String $table
     * @param array $condition
     * @param bool $sensitive
     * @return string
     */
    private function buildWhere($table, $condition, $sensitive = true)
    {
        $whereQuery = '';
        if ($condition) {
            $whereQuery .= " WHERE ";
            $count = 0;

            foreach ($condition as $key => $value) {
                if ($count > 0)
                    $whereQuery .= " AND ";

                if ($value === "!NULL" || $value === "IS NOT NULL")
                    $whereQuery .= "$table.$key IS NOT NULL";
                elseif ($value == "NULL" || $value == "IS NULL")
                    $whereQuery .= "$table.$key IS NULL";
                elseif (Utils::startsWith($value, ">") || Utils::startsWith($value, "<"))
                    $whereQuery .= "$table.$key $value";
                elseif ($value == '' && !$sensitive)
                    $whereQuery .= "($table.$key = '$value' OR $table.$key IS NULL)";
                else
                    $whereQuery .= "$table.$key = '$value'";

                $count++;
            }
        }
        return $whereQuery;
    }

    /**
     * Executes a simple SELECT query
     *
     * @param String $table
     * @param array $rows
     * @param array $data
     * @return array|bool
     */
    public function select($table, $rows = null, $data = null)
    {
        if ($table) {
            $selectQuery = $this->buildSelect($table, $rows, $data);

            return $this->execute_statement($selectQuery);
        }
        return [];
    }

    /**
     * Executes an UPDATE query and returns the updated data set
     *
     * @param String $table
     * @param array $data
     * @param array $condition
     * @return array|bool|null
     */
    public function update($table, Array $data, Array $condition)
    {
        $return = null;
        if ($table && $data && $condition) {
            $updateQuery = $this->buildUpdate($table, $data, $condition);

            if ($this->execute_statement($updateQuery)) {
                $selectQuery = $this->buildSelect($table, null, $condition, false);

                $return = $this->execute_statement($selectQuery, true);
            }
        }
        return $return;
    }

    /**
     * Prepares an SQL UPDATE statement.
     * Second Parameter has to be an HashMap with the table columns.
     * Third Parameter has to be the ID of the data set to update.
     *
     * @param $table : String
     * @param $data : HashMap
     * @param $condition : HashMap
     * @return string
     */
    private function buildUpdate($table, $data, $condition)
    {
        $q = "";

        if ($table && $data && $condition) {
            $update = "";

            $count = 0;
            foreach ($data as $key => $value) {
                if ($count > 0) {
                    $update .= ", ";
                }

                if ($value)
                    $update .= $table . ".$key = '$value'";
                else
                    $update .= $table . ".$key = null";

                $count++;
            }

            if (strlen($update) > 0) {
                $whereQuery = $this->buildWhere($table, $condition);

                $q = "UPDATE $table SET $update $whereQuery;";
            }
        }
        return $q;
    }

    /**
     * Executes a DELETE query
     *
     * @param String $table
     * @param array $condition
     * @return array|bool
     */
    public function delete($table, Array $condition)
    {
        $return = false;
        if ($table && $condition) {
            $selectQuery = $this->buildSelect($table, null, $condition);
            $return = $this->execute_statement($selectQuery);

            $deleteQuery = $this->buildDelete($table, $condition);

            if (!$this->execute_statement($deleteQuery))
                $return = false;
        }
        return $return;
    }

    /**
     * Prepares an SQL DELETE statement.
     * Second parameter specifies the data set to delete.
     * If the third parameter is false or unset the data set won't be deleted. A deletedate will be set.
     *
     * @param $table : String
     * @param $condition : HashMap
     * @return string
     */
    private function buildDelete($table, Array $condition)
    {
        $q = "";

        if ($table && $condition) {

            $whereQuery = $this->buildWhere($table, $condition);
            if ($whereQuery && strlen($whereQuery) > 0)
                $q = "DELETE FROM $table $whereQuery";

        }

        return $q;
    }

}