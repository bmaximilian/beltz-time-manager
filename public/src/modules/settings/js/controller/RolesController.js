import Helper from '../../../app/js/classes/Helper.class'
import RolesDialogController from '../controller/RolesDialogController'

var allRoles = [];

/**
 * controller for the role management of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdDialog, $mdToast, BaseUrl, RolesFactory) {
    "use strict";

    $scope.rolesToggle = false;
    $scope.roles = [];

    RolesFactory.getRoles(null, function (data) {
        $scope.roles = data;
    });

    $scope.changeRolesToggle = function () {
        $scope.rolesToggle = ($scope.rolesToggle ? false : true);
    };

    if (!$scope.text)
        $scope.text = {};
    $scope.text.rolesSearchInput = '';

    $scope.changeRolesSearch = function () {
        if ($scope.text.rolesSearchInput.length > 0) {
            if (allRoles.length === 0)
                allRoles = $scope.roles;

            $scope.roles = allRoles.filter(function (role) {
                if (role.description)
                    return role.name.toLowerCase().includes($scope.text.rolesSearchInput.toLowerCase())
                        || role.description.toLowerCase().includes($scope.text.rolesSearchInput.toLowerCase());
                else
                    return role.name.toLowerCase().includes($scope.text.rolesSearchInput.toLowerCase());
            });
        } else {
            $scope.roles = allRoles;
        }
    };

    $scope.openRolesEditDialog = function (ev, role) {
        $mdDialog.show({
            controller: RolesDialogController,
            templateUrl: BaseUrl.viewPath + "settings/views/rolesDialog.html",
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                role: role,
                action: 'edit'
            }
        })
            .then(function (answer) {
                $scope.roles = answer.data;
                Helper.standardToast($mdToast, answer.message);
            });
    };

    $scope.openRolesAddDialog = function (ev) {
        RolesFactory.getPowers(null, function (powers) {
            $mdDialog.show({
                controller: RolesDialogController,
                templateUrl: BaseUrl.viewPath + "settings/views/rolesDialog.html",
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    role: {
                        name: null,
                        description: null,
                        powers: powers
                    },
                    action: 'add'
                }
            })
                .then(function (answer) {
                    $scope.roles = answer.data;
                    Helper.standardToast($mdToast, answer.message);
                });
        });
    };

    $scope.deleteRole = function (id) {
        RolesFactory.deleteRoles(id, function () {
            RolesFactory.getRoles(null, function (roles) {
                $scope.roles = roles;
                Helper.standardToast($mdToast, "Rolle erfolgreich gelöscht");
            });
        });
    };

}