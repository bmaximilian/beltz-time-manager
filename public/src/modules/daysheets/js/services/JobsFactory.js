import Helper from '../../../app/js/classes/Helper.class'
import AjaxProvider from '../classes/AjaxProvider.class'

/**
 * Factory to store the jobs data
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default function ($mdToast, $http, BaseUrl) {
    var jobs = [];
    var Ajax = new AjaxProvider($http, $mdToast);

    $http.get(BaseUrl.baseAjax + "/api/jobs").then(function (response) {
        if (response && response.data && response.data.length > 0) {
            jobs = response.data;
        }
    }, Helper.connectionError($mdToast, "Aufträge konnten nicht erfasst werden"));

    /**
     * Returns all jobs or the job as id
     *
     * @param id
     * @param callback
     */
    function getJobs(id, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/jobs";
        if (id)
            apiRoute += "/" + id;

        $http.get(apiRoute).then(function (response) {
            if (response && response.data) {
                jobs = response.data;
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Aufträge konnten nicht erfasst werden"));
    }

    return {
        jobs: function () {
            return jobs;
        },

        getJobs: getJobs,

        insertJob: Ajax.fnInsert(BaseUrl.baseAjax + "/api/jobs"),

        getCustomers: Ajax.fnGet(BaseUrl.baseAjax + "/api/customers")
    }
}