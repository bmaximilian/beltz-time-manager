/**
 * provider for login
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
import Helper from '../classes/Helper.class'

export default function () {
    this.apiLoginRoute = '//localhost/api/auth/login';
    this.apiLogoutRoute = '//localhost/api/auth/logout';
    this.apiRefreshRoute = '//localhost/api/auth/refresh';

    this.setLoginRoute = function (loginRoute) {
        this.apiLoginRoute = loginRoute;
    };
    this.setLogoutRoute = function (logoutRoute) {
        this.apiLogoutRoute = logoutRoute;
    };
    this.setRefreshRoute = function (refreshRoute) {
        this.apiRefreshRoute = refreshRoute;
    };

    var user = {
        isLoggedIn: false
    };

    this.$get = function ($http, $mdToast) {
        var self = this;
        var tempUser = null;

        try {
            tempUser = JSON.parse(localStorage.getItem("activeUser"));
        } catch (e) {}

        if (tempUser) {
            user = tempUser;
        } else {
            $http.post(this.apiLoginRoute).then(function (response) {
                "use strict";
                var data = null;
                if (response)
                    data = response.data;

                if (data == '' || data == undefined)
                    data = null;

                if (data != null) {
                    data.isLoggedIn = true;
                    user = data;
                    localStorage.setItem("activeUser", JSON.stringify(user));
                } else {
                    localStorage.clear();
                }
            }, Helper.connectionError($mdToast, "Es konnte keine Verbindung zur Datenbank hergestellt werden"));
        }

        return {
            user: function () {
                let returnUser = user;

                try {
                    let tempUser = JSON.parse(localStorage.getItem("activeUser"));
                    if (tempUser)
                        returnUser = tempUser;

                } catch (e) {
                }

                return returnUser;
                //return user;
            },
            login: function (email, password, callback) {
                var send = {
                    "username": email,
                    "password": password
                };
                var tempUser = null;

                try {
                    tempUser = JSON.parse(localStorage.getItem("activeUser"));
                } catch (e) {
                }

                if (tempUser && (tempUser.emailBusiness == email || tempUser.emailPrivate == email || tempUser.personelNumber == email)) {
                    if (new Date(tempUser.tokenExpire).getTime() - new Date().getTime() > 0) {
                        user = tempUser;
                        callback(user)
                    }
                } else {
                    $http.post(self.apiLoginRoute, send).then(function (response) {
                        "use strict";
                        var data = null;

                        if (response)
                            data = response.data;

                        if (data == '' || data == undefined)
                            data = null;

                        if (data) {
                            data.isLoggedIn = true;
                            user = data;
                            localStorage.setItem("activeUser", JSON.stringify(user));
                        } else {
                            localStorage.clear();
                        }

                        callback(data);

                    }, Helper.connectionError($mdToast, "Es konnte keine Verbindung zur Datenbank hergestellt werden"));
                }
            },

            logout: function (callback) {
                try {
                    user = JSON.parse(localStorage.getItem("activeUser"));

                    $http.get(self.apiLogoutRoute + "/" + user.token).then(function (response) {
                        var data = false;
                        if (response)
                            data = response.data;

                        if (data == '' || data == undefined)
                            data = false;

                        user = {
                            isLoggedIn: false
                        };

                        localStorage.clear();

                        callback(data);
                    }, Helper.connectionError($mdToast, "Es konnte keine Verbindung zur Datenbank hergestellt werden"))
                } catch (e) {
                    console.error(e);
                    Helper.connectionError($mdToast, "Logout fehlgeschlagen")
                }
            },

            refresh: function (callback) {
                try {
                    tempUser = JSON.parse(localStorage.getItem("activeUser"));

                    let send = {
                        "token": tempUser.token
                    };

                    $http.post(self.apiRefreshRoute, send).then(function (response) {
                        "use strict";
                        var data = null;

                        if (response)
                            data = response.data;

                        if (data == '' || data == undefined)
                            data = null;

                        if (data) {
                            tempUser.tokenExpire = data.tokenExpire;
                            localStorage.setItem("activeUser", JSON.stringify(tempUser));
                        }

                        callback(data);

                    }, Helper.connectionError($mdToast, "Es konnte keine Verbindung zur Datenbank hergestellt werden"));
                } catch (e) {
                }
            }
        }
    }
}