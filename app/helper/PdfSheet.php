<?php

/**
 * Class extension to build PDF files
 *
 * @author <m.beck@beltz.de>
 */
class PdfSheet extends FPDF
{
    private $dateString;
    private $user;
    private $number;
    private $table = [];
    private $wholeHours;
    private $header;
    private $filePath = "bin/temp_pdf/";

    /**
     * Pdf constructor.
     * @param string $dateString : Headline String
     * @param array $tasks : Table rows
     * @param string $whole_hours : Calculated hours for that day
     * @param string $name : Name of committing user
     * @param string $number : Personel number of committing user
     * @param string $orientation : portrait or landscape
     * @param string $unit : Paper unit
     * @param string $size : Paper size
     */
    function __construct($dateString, $tasks, $whole_hours, $name, $number, $orientation = "L", $unit = "mm", $size = "A4")
    {
        $this->table = [];
        if ($tasks && sizeof($tasks) > 0)
            $this->table = $tasks;

        $this->dateString = \utf8_decode($dateString);
        $this->wholeHours = $whole_hours;
        $this->user = $name;
        $this->number = $number;

        $this->header = array(
            "KST" => 15,
            "Auftragsnr" => 30,
            "Projekt" => 70,
            "Zeitart" => 15,
            "Stunden" => 20,
            "Seiten" => 15,
            "Notizen" => 110
        );

        parent::__construct($orientation, $unit, $size);
    }

    /**
     * Function to create the daysheet PDF
     */
    public function createSheet($filename)
    {
        $this->AddPage();
        $this->createHeader();
        $this->createTable();

        $this->Output($this->filePath . $filename, "F", true);
    }

    /**
     * Function to create the header of the sheet
     */
    private function createHeader()
    {
        $this->SetFont('Helvetica', '', 8);
        $this->Cell(60, 10, "Name: " . utf8_decode($this->user), 0, 0, 'L');
        //tab
        $this->Cell(20);
        $this->Cell(60, 10, "Personalnummer: " . $this->number, 0, 0, 'L');
        // Line break
        $this->Ln(20);

        $this->SetFont('Helvetica', 'B', 16);
        // Move to the left
        $this->Cell(0.5);
        // Title
        $this->Cell(60, 10, $this->dateString, 0, 0, 'L');
        // Line break
        $this->Ln(10);
    }

    /**
     * Function to create the daysheet table
     */
    private function createTable()
    {
        // Colors, line width and bold font
        $this->SetFillColor(230, 113, 103);
        $this->SetTextColor(255);
        $this->SetDrawColor(0, 0, 0);
        $this->SetLineWidth(.15);
        $this->SetFont('Helvetica', 'B', 12);

        $whole_width = 0;

        foreach ($this->header as $title => $width) {
            $this->Cell($width, 9, $title, 1, 0, 'C', true);
            $whole_width += $width;
        }

        $this->Ln();

        // Color and font restoration
        $this->SetFillColor(247, 216, 216);
        $this->SetTextColor(0);
        $this->SetFont('Helvetica', '', 10);

        // Data
        $fill = false;
        foreach ($this->table as $task) {
            //Kostenstelle
            if ($task['costcenterId'] < 1) {
                $task['costcenterNumber'] = "";
                $task['jobnumber'] = "";
                $task['jobtitle'] = $task['costcenterName'];
                $task['timetypeNumber'] = "";
            }
            if ($task['jobnumber'] < 1)
                $task['jobnumber'] = "";


            //Calculate the height of the row
            $nb = 0;
            foreach ($this->header as $title => $width) {
                $taskDesignator = "costcenterNumber";
                switch ($title) {
                    case "KST":
                        $taskDesignator = "costcenterNumber";
                        break;
                    case "Auftragsnr":
                        $taskDesignator = "jobnumber";
                        break;
                    case "Projekt":
                        $taskDesignator = "jobtitle";
                        break;
                    case "Zeitart":
                        $taskDesignator = "timetypeNumber";
                        break;
                    case "Stunden":
                        $taskDesignator = "hours";
                        break;
                    case "Seiten":
                        $taskDesignator = "pages";
                        break;
                    case "Notizen":
                        $taskDesignator = "description";
                        break;
                }


                $nb = max($nb, $this->NbLines($width, $task[$taskDesignator]));
            }
            $h = 5 * $nb;

            //Issue a page break first if needed
            $this->CheckPageBreak($h);


            $this->Cell($this->header['KST'], 7, utf8_decode($task['costcenterNumber']), 'LR', 0, 'C', $fill);

            //Auftragsnummer
            $this->Cell($this->header['Auftragsnr'], 7, utf8_decode($task['jobnumber']), 'LR', 0, 'C', $fill);

            //Projekt
            $this->Cell($this->header['Projekt'], 7, utf8_decode($task['jobtitle']), 'LR', 0, 'L', $fill);

            //Zeitart
            $this->Cell($this->header['Zeitart'], 7, utf8_decode($task['timetypeNumber']), 'LR', 0, 'C', $fill);

            //Stunden
            if (!@$task['hours'])
                $task['hours'] = "";

            $this->Cell($this->header['Stunden'], 7, utf8_decode($task['hours']), 'LR', 0, 'C', $fill);

            //Seiten
            $this->Cell($this->header['Seiten'], 7, utf8_decode($task['pages']), 'LR', 0, 'C', $fill);

            //Notizen
            if (strlen($task['description']) > 70)
                $task['description'] = substr($task['description'], 0, 70) . "...";
            $this->Cell($this->header['Notizen'], 7, utf8_decode($task['description']), 'LR', 0, 'L', $fill);


            $this->Ln();
            $fill = !$fill;
        }


        // Closing line
        $this->SetFillColor(230, 113, 103);
        $this->SetFont('Helvetica', 'B', 10);

        $this->Cell($this->header['KST'] + $this->header['Auftragsnr'] + $this->header['Projekt'] + $this->header['Zeitart'], 9, utf8_decode("Geprüft"), 1, 0, 'L', true);
        // Gesamtstunden
        $this->Cell($this->header['Stunden'], 9, $this->wholeHours, 1, 0, 'C', true);
        $this->Cell($this->header['Seiten'] + $this->header['Notizen'], 9, "", 1, 0, 'L', true);
    }

    /**
     * Computes the number of lines a MultiCell of width w will take
     *
     * @param $w
     * @param $txt
     *
     * @return int
     */
    private function NbLines($w, $txt)
    {
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }

    /**
     * If the height h would cause an overflow, add a new page immediately
     *
     * @param $h
     */
    private function CheckPageBreak($h)
    {
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }
}