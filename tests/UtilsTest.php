<?php
include_once '../app/base/Utils.php';

/**
 * Class UtilsTest
 *
 * Test for the Utils class
 */
class UtilsTest extends PHPUnit_Framework_TestCase
{

    private $string = "Foo Bar Baz";

    /**
     * Tests the startsWith function
     */
    public function testStartsWith()
    {

        $this->assertEquals(
            true,
            Utils::startsWith($this->string, "Foo ")
        );

        $this->assertEquals(
            false,
            Utils::startsWith($this->string, "Bar")
        );

    }

    /**
     * Tests the endsWith function
     */
    public function testEndsWith()
    {

        $this->assertEquals(
            true,
            Utils::endsWith($this->string, "Baz")
        );

        $this->assertEquals(
            false,
            Utils::endsWith($this->string, "Bar")
        );

    }


    public function testGetIni()
    {
        $ini = Utils::getIni("slim.ini", "../bin/");
        $result = false;
        if ($ini)
            $result = true;


        $this->assertEquals(
            true,
            $result
        );
    }

}
