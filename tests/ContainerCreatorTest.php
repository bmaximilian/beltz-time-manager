<?php
include_once '../app/helper/ContainerCreator.php';
include_once '../app/helper/Contentloader.php';
include_once '../app/helper/MysqlPdo.php';
include_once '../app/helper/SFtp.php';
include_once '../app/base/Utils.php';
include_once '../vendor/autoload.php';

use Jgut\Slim\PHPDI\ContainerBuilder;

/**
 * Class ContainerCreatorTest
 */
class ContainerCreatorTest extends PHPUnit_Framework_TestCase
{
    private $container;

    /**
     * Tests if the logger is available
     */
    public function testLogger()
    {
        $this->initContainer();
        $test = ($this->container->get("logger") ? true : false);

        $this->assertEquals(
            true,
            $test
        );
    }

    /**
     * Initializes the application container
     */
    private function initContainer()
    {
        $config = Utils::getIni("slim.ini", "../bin/");

        $this->container = $container = ContainerBuilder::build();
        $builder = new ContainerCreator($container);

        $this->container = $builder->initContainer($config);
    }

    /**
     * Tests if database access is available
     */
    public function testLoader()
    {
        $this->initContainer();
        $test = ($this->container->get("loader") ? true : false);

        $this->assertEquals(
            true,
            $test
        );
    }

    /**
     * Tests if ftp access is available
     */
    public function testFtp()
    {
        $this->initContainer();
        $test = ($this->container->get("ftp") ? true : false);

        $this->assertEquals(
            true,
            $test
        );
    }

    /**
     * Tests if mailing class is available
     */
    public function testMail()
    {
        $this->initContainer();
        $test = ($this->container->get("mail") ? true : false);

        $this->assertEquals(
            true,
            $test
        );
    }

    /**
     * Tests if renderer is available
     */
    public function testView()
    {
        $this->initContainer();
        $test = ($this->container->get("view") ? true : false);

        $this->assertEquals(
            true,
            $test
        );
    }
}
