<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for dailytasks database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class DailytasksController extends RestController
{

    /**
     * Returns a dailytask selected by primary key
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function get(Request $request, Response $response)
    {
        $id = $request->getAttribute("id");

        $task = $this->loader->select("v_dailytasks", null, [
            "id" => $id
        ]);

        $out = null;
        if (sizeof($task) > 0)
            $out = $task[0];

        return $response->getBody()->write(json_encode($out));
    }

    /**
     * Inserts a new dailytask for a daysheet
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        $sheet_id = $request->getAttribute("id");
        $answer = null;

        $insert_data = $this->getRequestBody($request);
        if (!$insert_data['startdate'])
            $insert_data['startdate'] = date('Y-m-d H:i:s');


        /* Set enddate of previous task */
        $getTasksQuery = "SELECT daysheetsxdailytasks.dailytask_pk AS taskId
FROM daysheetsxdailytasks
  JOIN dailytasks ON daysheetsxdailytasks.dailytask_pk = dailytasks.pk
WHERE daysheetsxdailytasks.daysheet_pk = $sheet_id
      AND dailytasks.enddate IS NULL
ORDER BY dailytasks.startdate DESC
LIMIT 1;";
        $lastTaskPk = $this->loader->query($getTasksQuery);

        if ($lastTaskPk && sizeof($lastTaskPk) > 0) {
            $this->loader->update("dailytasks", [
                "enddate" => $insert_data['startdate']
            ], [
                "pk" => $lastTaskPk[0]['taskId']
            ]);
        }

        $task = $this->loader->insert("dailytasks", $insert_data);

        if ($task) {
            if ($this->loader->insert("daysheetsxdailytasks", [
                "daysheet_pk" => $sheet_id,
                "dailytask_pk" => $task['pk']
            ]))
                $answer = $task;
        }

        return $response->getBody()->write(json_encode($task));
    }

    /**
     * Resolves the request body for dailytasks request
     *
     * @param Request $request
     *
     * @return array
     */
    private function getRequestBody(Request $request)
    {
        return array(
            'job_pk' => @$request->getParsedBody()['jobId'],
            'timetype_pk' => @$request->getParsedBody()['timetypeId'],
            'cost_center_pk' => @$request->getParsedBody()['costcenterId'],
            'material_pk' => @$request->getParsedBody()['materialId'],
            'partjob_pk' => @$request->getParsedBody()['partjobId'],
            'startdate' => @$request->getParsedBody()['startdate'],
            'enddate' => @$request->getParsedBody()['enddate'],
            'pages' => @$request->getParsedBody()['pages'],
            'description' => @$request->getParsedBody()['description']
        );
    }

    /**
     * Updates a dailytask
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function put(Request $request, Response $response)
    {
        $task_id = $request->getAttribute("id");
        $update_data = $this->getRequestBody($request);

        if ($update_data['enddate'] == 'true')
            $update_data['enddate'] = date('Y-m-d H:i:s');

        $task = $this->loader->update("dailytasks", $update_data, [
            "pk" => $task_id
        ]);

        return $response->getBody()->write(json_encode($task));
    }

    /**
     * DELETE function
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function delete(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }
}