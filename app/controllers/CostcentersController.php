<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for costcenters database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class CostcentersController extends RestController
{

    /**
     * Selects all costcenters or the costcenter by id
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $out = [];

        if ($id)
            $costcenters = $this->loader->select("costcenters", [
                "pk" => "id",
                "designation" => "name",
                "identifier"
            ], [
                "deletedate" => "NULL",
                "pk" => $id
            ]);
        else
            $costcenters = $this->loader->select("costcenters", [
                "pk" => "id",
                "designation" => "name",
                "identifier"
            ], [
                "deletedate" => "NULL"
            ]);

        foreach ($costcenters as $costcenter) {
            $links = $this->loader->select("costcenterxtimetype", null, [
                "costcenter_pk" => $costcenter['id']
            ]);
            $timetypes = [];

            foreach ($links as $link) {
                $timetype = $this->loader->select("timetypes", [
                    "pk" => "id",
                    "designation" => "name",
                    "identifier"
                ], [
                    "deletedate" => "NULL",
                    "pk" => $link['timetype_pk']
                ]);

                if ($timetype && sizeof($timetype) > 0) {
                    $timetype = $timetype[0];
                    array_push($timetypes, $timetype);
                }
            }

            $costcenter['timetypes'] = $timetypes;
            array_push($out, $costcenter);
        }

        return $response->getBody()->write(json_encode($out));
    }

    /**
     * POST function for costcenters
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * PUT function for costcenters
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function put(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * DELETE function for costcenters
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function delete(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

}