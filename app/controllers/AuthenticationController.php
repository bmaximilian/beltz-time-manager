<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class AuthenticationController extends \DatabaseBaseController
{
    use DatabaseUser;

    /**
     * Function to authenticate
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function login(Request $request, Response $response) {
        $return = "";

        $username = $request->getParsedBody()["username"];
        $password = SHA1($request->getParsedBody()["password"]);

        if ($username && $password) {

            $usersQuery = "SELECT * FROM v_users WHERE 
  v_users.deletedate IS NULL 
  AND (v_users.token IS NULL OR v_users.tokenExpire < NOW())
  AND (v_users.personelNumber = '$username' 
       OR v_users.emailBusiness = '$username' 
       OR v_users.emailPrivate = '$username') 
  AND v_users.password = '$password'
  LIMIT 1";

            $result = $this->loader->query($usersQuery);

            if ($result && sizeof($result) == 1) {
                $result = $this->setToken($result[0]);
                $roleId = $result['roleId'];


                $result['name'] = $this->getNameForUser($result);

                $powersQuery = "SELECT * FROM v_powers WHERE v_powers.roleId = $roleId;";

                $re = $this->loader->query($powersQuery);

                $powers = array();

                if ($re) {
                    foreach ($re as $r) {
                        $powers[$r['name']] = $r['value'];
                    }
                }

                $result['powers'] = $powers;

                $return = json_encode($result);
            }

        }

        return $response->getBody()->write($return);
    }

    /**
     * Sets a database token with expire date for the user table
     *
     * @param $user
     * @return mixed
     */
    private function setToken($user)
    {
        $token = SHA1($user['id'] . date("Y-m-d H:i:s") . $user['password']);
        $expire = date("Y-m-d H:i:s", strtotime('+1 hours'));

        return $this->loader->update("v_users", [
            "token" => $token,
            "tokenExpire" => $expire
        ], [
            "id" => $user['id']
        ]);
    }

    /**
     * Function to logout from the application
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function logout(Request $request, Response $response) {
        $token = $request->getAttribute("token");

        if ($token) {

            $result = $this->loader->select("v_users", null,
                [
                    "token" => $token,
                ]);

            if ($result && sizeof($result) === 1) {
                $result = $result[0];

                if ($this->loader->update("v_users", [
                    "token" => null,
                    "tokenExpire" => null
                ], [
                    "id" => $result['id']
                ])
                ) {

                    return $response->getBody()->write(true);

                }
            }
        }

        return $response->getBody()->write(false);
    }

    /**
     * Returns the user for a token
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return int
     */
    public function refreshLogin(Request $request, Response $response)
    {
        $token = $request->getParsedBody()["token"];

        return $response->getBody()->write(json_encode($this->authenticate($token)));
    }

    /**
     * Selects user by authentication token
     * and sets a new expire date
     *
     * @param $token
     * @return mixed
     */
    public function authenticate($token) {
        $query = "SELECT * FROM v_users WHERE v_users.token = '$token' AND v_users.tokenExpire > NOW();";

        $user = $this->loader->query($query);

        if ($user && sizeof($user) > 0)
            $user = $user[0];


        return $this->setExpire($user);
    }

    /**
     * Sets the expire date for an authentication token
     *
     * @param $user
     *
     * @return mixed
     */
    private function setExpire($user)
    {
        $expire = date("Y-m-d H:i:s", strtotime('+1 hours'));

        return $this->loader->update("v_users", [
            "tokenExpire" => $expire
        ], [
            "token" => @$user['token'],
            "id" => @$user['id']
        ]);
    }

}