
import Helper from '../../../app/js/classes/Helper.class'

/**
 * Provider for the user objects
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default function () {
    this.apiRoute = "//localhost/api/users";

    this.setApiRoute = function (apiRoute) {
        "use strict";
        this.apiRoute = apiRoute
    };

    this.$get = function ($mdToast, $http) {
        var self = this;
        var users = [];

        $http.get(self.apiRoute).then(function (response) {
            if (response && response.data && response.data.length > 0) {
                users = response.data;
            }
        }, Helper.connectionError($mdToast, "Benutzer konnten nicht erfasst werden"));


        /**
         * Returns all users or the user specified by id
         * or all users specified by department (id)
         *
         * @param id
         * @param callback
         * @param byDepartment
         */
        function getUsers(id, callback, byDepartment = false) {
            var apiRoute = self.apiRoute;

            if (byDepartment)
                apiRoute += "/department";

            if (id)
                apiRoute += "/" + id;

            $http.get(apiRoute).then(function (response) {
                if (response && response.data) {
                    users = response.data;
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Benutzer konnten nicht erfasst werden"));
        }


        /**
         * Inserts a user
         *
         * @param data
         * @param callback
         */
        function insertUser(data, callback) {
            var apiRoute = self.apiRoute;

            $http.post(apiRoute, data).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Benutzer konnte nicht hinzugefügt werden"));
        }


        /**
         * Updates a user specified by id
         *
         * @param id
         * @param data
         * @param callback
         */
        function updateUser(id, data, callback) {
            var apiRoute = self.apiRoute + "/" + id;

            if (id)
                $http.put(apiRoute, data).then(function (response) {
                    if (response && response.data) {
                        callback(response.data);
                    }
                }, Helper.connectionError($mdToast, "Benutzer konnte nicht geändert werden"));
        }


        /**
         * Deletes a user specified by id
         *
         * @param id
         * @param callback
         */
        function deleteUser(id, callback) {
            var apiRoute = self.apiRoute + "/" + id;

            if (id)
                $http.delete(apiRoute).then(function (response) {
                    if (response && response.data) {
                        callback(response.data);
                    }
                }, Helper.connectionError($mdToast, "Benutzer konnte nicht gelöscht werden"));
        }

        return {
            users: function () {
                return users;
            },

            getUsers: getUsers,

            insertUser: insertUser,

            updateUser: updateUser,

            deleteUser: deleteUser
        }
    };
}