import AllSheetsController from '../controller/AllSheetsController'

/**
 * directive to manage all daysheets
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function (BaseUrl) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: BaseUrl.viewPath + "daysheets/views/allSheets.html",
        controller: AllSheetsController,
        scope: {
            currentUser: "=activeUser"
        }
    }
};