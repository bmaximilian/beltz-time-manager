<?php

use \Jgut\Slim\Controller\Resolver;

/**
 * Routing for the application
 * Calls controllers for defined routes
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class Router extends BaseRouter
{
    /**
     * Define Controllers
     * @var array
     */
    private $controllers = [
        "IndexController",
        "AuthenticationController",
        "ErrorsController",
        "UsersController",
        "RolesController",
        "DepartmentsController",
        "PositionsController",
        "PowersController",
        "SalutationsController",
        "DaysheetsController",
        "DailytasksController",
        "JobsController",
        "CostcentersController",
        "MaterialsController",
        "CustomersController"
    ];

    /**
     * Router constructor.
     *
     * @param \Slim\App $app
     */
    public function __construct(\Slim\App $app)
    {
        //$this->registerControllers();

        parent::__construct($app);
    }

    /**
     * Define Routes for the Application
     */
    public function route() {
        $this->app->get('/', '\IndexController:renderApp');

        $this->app->group("/api", function () {
            $this->group("/auth", function () {
                $this->post("/login", '\AuthenticationController:login');
                $this->post("/refresh", '\AuthenticationController:refreshLogin');
                $this->get("/logout/{token}", '\AuthenticationController:logout');
            });

            $this->group("/errors", function () {
                $this->get("/401", '\ErrorsController:forbidden');
                $this->get("/403", '\ErrorsController:forbidden');
                $this->get("/404", '\ErrorsController:notFound');
            });

            // Settings routes
            $this->group("/users", function () {
                $this->get("", '\UsersController:get');
                $this->get("/{id}", '\UsersController:get');
                $this->get("/department/{id}", '\UsersController:getByDepartment');
                $this->post("", '\UsersController:post');
                $this->put("/{id}", '\UsersController:put');
                $this->delete("/{id}", '\UsersController:delete');
            });

            $this->group("/roles", function () {
                $this->get("", '\RolesController:get');
                $this->get("/{id}", '\RolesController:get');
                $this->post("", '\RolesController:post');
                $this->put("/{id}", '\RolesController:put');
                $this->delete("/{id}", '\RolesController:delete');
            });

            $this->group("/departments", function () {
                $this->get("", '\DepartmentsController:get');
                $this->get("/{id}", '\DepartmentsController:get');
                $this->post("", '\DepartmentsController:post');
                $this->put("/{id}", '\DepartmentsController:put');
                $this->delete("/{id}", '\DepartmentsController:delete');
            });

            $this->group("/positions", function () {
                $this->get("", '\PositionsController:get');
                $this->get("/{id}", '\PositionsController:get');
                $this->post("", '\PositionsController:post');
                $this->put("/{id}", '\PositionsController:put');
                $this->delete("/{id}", '\PositionsController:delete');
            });

            $this->group("/powers", function () {
                $this->get("", '\PowersController:get');
                $this->get("/{id}", '\PowersController:get');
            });

            $this->group("/salutations", function () {
                $this->get("", '\SalutationsController:get');
                $this->get("/{id}", '\SalutationsController:get');
            });

            // Daysheets routes
            $this->post("/checkout/{id}", '\DaysheetsController:checkout');
            $this->post("/checkout/{id}/{timestamp}", '\DaysheetsController:checkout');

            $this->group("/daysheets", function () {
                $this->get("", '\DaysheetsController:get');
                $this->get("/{id}", '\DaysheetsController:get');
                $this->get("/byUser/{id}", '\DaysheetsController:getByUser');
                $this->get("/lastByUser/{id}", '\DaysheetsController:getLastByUser');
                $this->get("/activeByUser/{id}", '\DaysheetsController:getActiveByUser');
                $this->post("/{id}", '\DaysheetsController:post');
            });

            $this->group("/dailytasks", function () {
                $this->get("/{id}", '\DailytasksController:get');
                $this->post("/{id}", '\DailytasksController:post');
                $this->put("/{id}", '\DailytasksController:put');
            });

            $this->group("/jobs", function () {
                $this->get("", '\JobsController:get');
                $this->get("/{id}", '\JobsController:get');
                $this->post("", '\JobsController:post');
            });

            $this->group("/costcenters", function () {
                $this->get("", '\CostcentersController:get');
                $this->get("/{id}", '\CostcentersController:get');
            });

            $this->group("/materials", function () {
                $this->get("", '\MaterialsController:get');
                $this->get("/{id}", '\MaterialsController:get');
            });

            $this->group("/customers", function () {
                $this->get("", '\CustomersController:get');
                $this->get("/{id}", '\CustomersController:get');
            });
        });
    }

    /**
     * Registers the router
     */
    public function register()
    {
        $this->registerControllers();
    }

    /**
     * Register Controllers to make them usable
     */
    private function registerControllers() {
        foreach (Resolver::resolve($this->controllers) as $controller => $callback) {
            $container[$controller] = $callback;
        }
    }
}