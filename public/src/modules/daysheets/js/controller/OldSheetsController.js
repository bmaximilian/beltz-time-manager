import Daysheet from '../classes/Daysheet.class'
import Helper from '../../../app/js/classes/Helper.class'
import ControllerBase from '../classes/ControllerBase'
import AddTaskDialogController from './AddTaskDialogController'

/**
 * controller for managing old daysheets
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
class OldSheetController extends ControllerBase {

    /**
     * Opens dialog to add or edit tasks
     *
     * @param $mdDialog
     * @param $mdToast
     * @param activeUser
     * @param BaseUrl
     * @returns {Function}
     * @private
     */
    static _openTaskEditDialog($mdDialog, $mdToast, activeUser, BaseUrl) {
        return function (ev, sheet, task) {
            if (activeUser && activeUser.powers && activeUser.powers.OFFLINE_WORKING_MASTER && activeUser.powers.OFFLINE_WORKING_MASTER >= 255) {
                $mdDialog.show({
                    controller: AddTaskDialogController,
                    templateUrl: BaseUrl.viewPath + "daysheets/views/addTaskDialog.html",
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    multiple: true,
                    locals: {
                        sheet: sheet,
                        task: task
                    }
                })
                    .then(function (answer) {
                        Helper.standardToast($mdToast, answer.message);
                    });
            }
        };
    }

    /**
     * Returns the time (hh_mm) of a timestamp
     *
     * @param date
     * @returns {*}
     * @private
     */
    static _getTimeOfTimestamp(date) {
        return Daysheet.timestampToTime(date);
    }

    /**
     * Returns the date (dd.mm.yyyy) of a timestamp
     *
     * @param date
     * @param withWeekday
     * @returns {*}
     * @private
     */
    static _getDateOfTimestamp(date, withWeekday = false) {
        return Daysheet.timestampToDate(date, withWeekday);
    }

    /**
     * Creates filter for searching old daysheets
     *
     * @param query
     * @returns {Function}
     * @private
     */
    static _createFilterFor(query) {
        return function filterFn(sheet) {
            return (
                OldSheetController._getDateOfTimestamp(sheet.startdate, true).toLowerCase().includes(query.toLowerCase())
                || OldSheetController._getTimeOfTimestamp(sheet.startdate).toLowerCase().includes(query.toLowerCase())
                || OldSheetController._getTimeOfTimestamp(sheet.enddate).toLowerCase().includes(query.toLowerCase())
                || query.length == 0
            );
        }
    }

    /**
     * Filters the old sheets with the query
     *
     * @param scope
     * @returns {Function}
     * @private
     */
    static _filterFunction(scope) {
        return function (query) {
            return query ? scope.oldSheet.sheets.filter(OldSheetController._createFilterFor(query)) : scope.oldSheet.sheets;
        }
    }

    /**
     * OldSheetController
     *
     * @param $mdDialog
     * @param $mdToast
     * @param $scope
     * @param DaysheetFactory
     * @param BaseUrl
     */
    static controller($mdDialog, $mdToast, $scope, DaysheetFactory, BaseUrl) {

        let user = null;
        if (!$scope.ownerOfSheet)
            user = OldSheetController._getStorageItem("activeUser");
        else
            user = $scope.ownerOfSheet;

        var activeUser = OldSheetController._getStorageItem("activeUser");

        $scope.oldSheet = {
            sheets: [],
            timestampToTime: OldSheetController._getTimeOfTimestamp,
            timestampToDate: OldSheetController._getDateOfTimestamp,
            openTaskEditDialog: OldSheetController._openTaskEditDialog($mdDialog, $mdToast, activeUser, BaseUrl),
            fnFilter: OldSheetController._filterFunction($scope),
            toggle: false,
            searchText: null,
            userId: user.id
        };

        $scope.changeOldSheetToggle = function () {
            $scope.oldSheet.toggle = !$scope.oldSheet.toggle;
        };

        if ($scope.ownerOfSheet)
            $scope.oldSheet.toggle = true;

        if (user) {
            DaysheetFactory.getDaysheets(user.id, function (daysheets) {
                for (let sheet of daysheets) {
                    if (sheet.enddate || $scope.ownerOfSheet) {

                        if (!sheet.enddate)
                            sheet.enddate = Daysheet._dateToTimestamp(new Date());

                        if (sheet.tasks) {
                            var newTasks = [];
                            for (let task of sheet.tasks) {
                                if (!task.enddate)
                                    task.enddate = Daysheet._dateToTimestamp(new Date());

                                task.time = Daysheet.calculate(task.startdate, task.enddate);
                                newTasks.push(task);
                            }
                            sheet.tasks = newTasks;
                        }


                        $scope.oldSheet.sheets.push(sheet);
                    }

                }
            }, true)
        }

    }
}

export default OldSheetController.controller