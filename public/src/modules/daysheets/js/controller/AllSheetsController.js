import ControllerBase from '../classes/ControllerBase'
/**
 * controller for managing old daysheets
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
class AllSheetsController extends ControllerBase {

    /**
     * Opens dialog with old sheets of given user
     *
     * @param $mdDialog
     * @param BaseUrl
     * @returns {Function}
     * @private
     */
    static _openOldSheets($mdDialog, BaseUrl) {
        return function (ev, user) {
            $mdDialog.show({
                controller: function ($scope, user) {
                    $scope.oldSheetUser = user;
                    $scope.cancelOldSheets = function () {
                        $mdDialog.cancel();
                    }
                },
                templateUrl: BaseUrl.viewPath + "daysheets/views/oldSheetsDialog.html",
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                multiple: true,
                locals: {
                    user: user
                }
            });
        };
    }

    /**
     * Creates filter for user search
     *
     * @param query
     * @returns {Function}
     * @private
     */
    static _createFilterFor(query) {
        return function filterFn(user) {
            return (
                user.name.toLowerCase().includes(query.toLowerCase())
                || user.emailBusiness.toLowerCase().includes(query.toLowerCase())
                || user.role.toLowerCase().includes(query.toLowerCase())
                || user.position.toLowerCase().includes(query.toLowerCase())
                || user.department.toLowerCase().includes(query.toLowerCase())
                || query.length == 0
            );
        }
    }

    /**
     * Filters the users with the search query
     *
     * @param scope
     * @returns {Function}
     * @private
     */
    static _filterFunction(scope) {
        return function (query) {
            return query ? scope.allSheets.users.filter(AllSheetsController._createFilterFor(query)) : scope.allSheets.users;
        }
    }

    /**
     * AllSheetsController
     *
     * @param $scope
     * @param $mdDialog
     * @param BaseUrl
     * @param Users
     */
    static controller($scope, $mdDialog, BaseUrl, Users) {

        let activeUser = $scope.currentUser;

        $scope.allSheets = {
            toggle: false,
            searchText: null,
            fnFilter: AllSheetsController._filterFunction($scope),
            openDialog: AllSheetsController._openOldSheets($mdDialog, BaseUrl),
            users: []
        };

        if (activeUser.powers.ACCESS_OTHER_DAYSHEETS >= 255) {
            Users.getUsers(null, function (users) {
                $scope.allSheets.users = users;
            });
        } else if (activeUser.powers.ACCESS_OTHER_DAYSHEETS >= 200 && activeUser.powers.ACCESS_OTHER_DAYSHEETS < 255) {
            Users.getUsers(activeUser.departmentId, function (users) {
                $scope.allSheets.users = users;
            }, true);
        }

        $scope.changeAllSheetsToggle = function () {
            $scope.allSheets.toggle = !$scope.allSheets.toggle;
        };
    }
}

export default AllSheetsController.controller