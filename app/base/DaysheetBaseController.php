<?php

/**
 * Base controller for daysheet database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
abstract class DaysheetBaseController extends DatabaseBaseController
{
    use Mailing;
    use TimeCalculation;
    use PdfCreation;

    private $config;
    private $mailer;


    /**
     * DaysheetBaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->config = $this->container->get("config")['mailing'];

        $mailer = $this->container->get("mail");
        if ($mailer instanceof PHPMailer) {
            $this->mailer = $mailer;
        }
    }

    /**
     * Inserts a ner Daysheet into the daysheets table
     * @param $userId
     * @return array|bool|mixed
     */
    protected function startDay($userId)
    {
        $out = false;

        $q = "SELECT COUNT(*) as ammount FROM daysheets WHERE daysheets.startdate >= CURDATE() AND daysheets.user_pk = $userId;";
        $r = $this->loader->query($q);
        if ($r && sizeof($r) > 0)
            $r = $r[0];

        if ($r['ammount'] == 0) {
            $start = date('Y-m-d H:i:s');
            $q = "INSERT INTO daysheets (daysheets.user_pk, daysheets.startdate) VALUES ($userId, '$start');";

            if ($this->loader->query($q)) {
                $q = "SELECT daysheets.pk FROM daysheets WHERE daysheets.user_pk = $userId AND daysheets.startdate = '$start';";

                $r = $this->loader->query($q);
                if ($r && sizeof($r) > 0) {
                    $r = $r[0];
                    $out = $this->getDaysheets($r['pk']);

                    if ($out && sizeof($out) > 0) {
                        $out = $out[0];
                    }
                }
            }
        }
        return $out;
    }


    /**
     * Returns all daysheets
     *
     * @param $id : primary key of the sheet or of the user
     * @param bool $byUser : if true it will select all sheets for an user
     * @param bool $lastSheet : declares if only the last sheet for the user will be selected or all (if selecting by user)
     * @param bool $activeSheet : if sheets enddate is null
     * @return array
     */
    protected function getDaysheets($id = null, $byUser = false, $lastSheet = false, $activeSheet = false)
    {
        $out = [];

        $q = "SELECT daysheets.pk AS id, daysheets.user_pk AS userId, daysheets.startdate, daysheets.enddate, daysheets.description, users.firstname, users.lastname, users.personelnr AS personelNumber, users.working_hours_per_day AS workingHoursPerDay, users.email_business AS emailBusiness FROM daysheets JOIN users ON daysheets.user_pk = users.pk ";

        if ($id)
            if ($byUser) {
                $q .= "WHERE daysheets.user_pk = $id ";
                if ($activeSheet)
                    $q .= "AND daysheets.enddate IS NULL ";
                $q .= "ORDER BY daysheets.startdate DESC";
                if ($lastSheet)
                    $q .= " LIMIT 1";
            } else
                $q .= "WHERE daysheets.pk = $id";

        $q .= ";";

        $result = $this->loader->query($q);

        if ($result)
            foreach ($result as $r) {
                $link_q = "SELECT daysheetsxdailytasks.dailytask_pk AS taskId, dailytasks.startdate FROM daysheetsxdailytasks JOIN dailytasks ON daysheetsxdailytasks.dailytask_pk = dailytasks.pk WHERE daysheetsxdailytasks.daysheet_pk = " . $r['id'] . " ORDER BY dailytasks.startdate ASC;";
                $links = $this->loader->query($link_q);

                $tasks = [];

                if ($links)
                    foreach ($links as $link) {
                        $id = $link['taskId'];
                        $task_q = "SELECT dailytasks.pk AS id, dailytasks.job_pk AS jobId, dailytasks.timetype_pk AS timetypeId, dailytasks.cost_center_pk AS costcenterId, dailytasks.partjob_pk AS partjobId, dailytasks.material_pk AS materialId, dailytasks.startdate, dailytasks.enddate, dailytasks.pages, dailytasks.description, jobs.jobnr AS jobnumber, jobs.designation AS jobtitle, timetypes.identifier AS timetypeNumber, timetypes.designation AS timetypeName, costcenters.identifier AS costcenterNumber, costcenters.designation AS costcenterName, materials.identifier AS materialIdentifier, materials.designation AS materialName, partjobs.designation AS partjobName, partjobs.identifier AS partjobNumber FROM dailytasks LEFT JOIN jobs ON (dailytasks.job_pk IS NOT NULL AND dailytasks.job_pk = jobs.pk) JOIN costcenters ON dailytasks.cost_center_pk = costcenters.pk LEFT JOIN timetypes ON (dailytasks.timetype_pk IS NOT NULL AND timetypes.pk = dailytasks.timetype_pk) LEFT JOIN materials ON (dailytasks.material_pk IS NOT NULL AND materials.pk = dailytasks.material_pk) LEFT JOIN partjobs ON (dailytasks.partjob_pk IS NOT NULL AND partjobs.pk = dailytasks.partjob_pk) WHERE dailytasks.pk = $id LIMIT 1;";

                        $task = $this->loader->query($task_q);
                        if (sizeof($task) > 0)
                            $task = $task[0];

                        array_push($tasks, $task);
                    }

                $r['tasks'] = $tasks;
                array_push($out, $r);
            }

        return $out;
    }

    /**
     * Selects the committing user from the database
     * writes the enddate into the daysheets table
     * calculates whole working time
     * calculates overtime
     * selects dailytasks for the daysheet
     * builds headline string
     *
     * @param $id : id of the daysheet
     * @param $byUser : if true last datasheet of the user passed by id will be selected
     * @param $timestamp : if forgotten checkout
     * 
*@return boolean
     */
    protected function calculateEnd($id, $byUser = false, $timestamp = null)
    {
        if (!$byUser) {
            if (!$timestamp)
                $q = "UPDATE daysheets SET daysheets.enddate = NOW() WHERE daysheets.pk = $id";
            else
                $q = "UPDATE daysheets SET daysheets.enddate = '$timestamp' WHERE daysheets.pk = $id";
            $this->loader->query($q);

            $q = "SELECT daysheetsxdailytasks.dailytask_pk AS taskId
FROM daysheetsxdailytasks
  JOIN dailytasks ON daysheetsxdailytasks.dailytask_pk = dailytasks.pk
WHERE daysheetsxdailytasks.daysheet_pk = $id
      AND dailytasks.enddate IS NULL
ORDER BY dailytasks.startdate DESC
LIMIT 1;";
            $lastTask = $this->loader->query($q);
            if ($lastTask && sizeof($lastTask) > 0) {
                $lastTask = $lastTask[0];
                if (!$timestamp)
                    $q = "UPDATE dailytasks SET dailytasks.enddate = NOW() WHERE dailytasks.pk = " . $lastTask['taskId'] . ";";
                else
                    $q = "UPDATE dailytasks SET dailytasks.enddate = '$timestamp' WHERE dailytasks.pk = " . $lastTask['taskId'] . ";";
                $this->loader->query($q);
            }
        }

        /**
         * Read Tasks for selected Daysheet
         */
        $daysheet = $this->getDaysheets($id, $byUser, true);
        if (sizeof($daysheet) == 1)
            $daysheet = $daysheet[0];

        $tasks = $daysheet['tasks'];

        /**
         * Calculate complete Hours from Daysheet
         */
        $calculated = $this->calculateHours($tasks, $daysheet['workingHoursPerDay']);
        $tasks = $calculated['tasks'];

        if ($calculated['overtime'] != 0) {
            if (!$byUser) {
                $userId = $daysheet['userId'];
            } else {
                $userId = $id;
            }
            $q = "UPDATE v_users SET v_users.overtimeValue = v_users.overtimeValue + " . $calculated['overtime'] . " WHERE v_users.id = $userId;";
            $this->loader->query($q);
        }

        /**
         * Build correct datestring (with overtime)
         */
        $dateString = $this->buildDateHeadline($daysheet['startdate'], $daysheet['enddate'], $calculated['overtime']);

        $pdfFilename = sha1($daysheet['personelNumber'] + $daysheet['enddate'] + $daysheet['firstname']) . ".pdf";

        $completeHours = $calculated['completeHours'] + $calculated['overtime'];
        if ($calculated['overtime'] < 0)
            $completeHours = $calculated['completeHours'];

        $pdf_data = array(
            "date_string" => $dateString['headline'],
            "tasks" => $tasks,
            "complete_hours" => $completeHours,
            "username" => $daysheet['firstname'] . " " . $daysheet['lastname'],
            "number" => $daysheet['personelNumber'],
            "filename" => $pdfFilename
        );

        $this->createPDF($pdf_data);

        $mailBody = $this->loadMailBody($daysheet['firstname'] . " " . $daysheet['lastname'], $this->config['recipientSalutation']);
        $mailHeadString = "Zeitnachweis_" . $dateString['date'] . "_" . $daysheet['firstname'] . "_" . $daysheet['lastname'];
        $mailResult = $this->sendMail($this->mailer, [
            "from" => $this->config['from'],
            "fromName" => $this->config['fromName'],
            "to" => $this->config['recipient'],
            "toName" => $this->config['recipientName'],
            "reply" => $daysheet['emailBusiness'],
            "replyName" => $daysheet['firstname'] . " " . $daysheet['lastname'],
            "subject" => $mailHeadString,
            "body" => $mailBody,
            "attachmentPath" => realpath("./bin/temp_pdf/$pdfFilename"),
            "attachmentName" => "$mailHeadString.pdf",
            "logoPath" => realpath("./bin/templates/bgb_logo.png")
        ]);

        $out = false;
        if ($mailResult)
            $out = unlink(realpath("./bin/temp_pdf/$pdfFilename"));

        return $out;
    }
}