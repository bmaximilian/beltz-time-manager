<?php

use Jgut\Slim\Controller\Base as BaseController;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for the first application route
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class IndexController extends \ViewBaseController
{

    /**
     * Renders the JavaScript application
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function renderApp(Request $request, Response $response) {

        return $this->view->render($response, "index.html");
    }
}