import Helper from '../../../app/js/classes/Helper.class';

/**
 * controller for the departments management functions of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdDialog, position, action, DepartmentsFactory, PositionsFactory) {
    "use strict";

    if (action === 'edit')
        $scope.headline = "Bearbeite Stelle";
    else if (action === 'add')
        $scope.headline = "Füge Stelle hinzu";

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.departments = [];
    DepartmentsFactory.getDepartments(null, function (departments) {
        $scope.departments = departments;
    });

    $scope.position = {
        name: null,
        description: null,
        departmentName: null,
        departmentId: null
    };
    if (position) {
        $scope.position = position;
        PositionsFactory.getPositions(position.id, function (pos) {
            if (pos && pos.length == 1)
                $scope.position = pos[0];
        });
    }

    $scope.submit = function () {
        if (Helper.validateForm($scope.positionsForm) && $scope.position.departmentId != null) {
            if (action === 'edit') {
                PositionsFactory.updatePosition($scope.position.id, $scope.position, function () {
                    PositionsFactory.getPositions(null, function (data) {
                        $mdDialog.hide({
                            message: 'Stelle erfolgreich bearbeitet',
                            data: data
                        });
                    });
                });
            }

            else if (action === 'add') {
                PositionsFactory.insertPosition($scope.position, function () {
                    PositionsFactory.getPositions(null, function (data) {
                        $mdDialog.hide({
                            message: 'Stelle erfolgreich hinzugefügt',
                            data: data
                        });
                    });
                });
            }
        }
    };
}