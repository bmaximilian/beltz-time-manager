
import Helper from '../../../app/js/classes/Helper.class'

/**
 * Factory to store the salutations data
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default function ($mdToast, $http, BaseUrl) {
    var salutations = [];

    $http.get(BaseUrl.baseAjax + "/api/salutations").then(function (response) {
        if (response && response.data && response.data.length > 0) {
            salutations = response.data;
        }
    }, Helper.connectionError($mdToast, "Anreden konnten nicht erfasst werden"));


    /**
     * Returns all salutations or the salutation specified by id
     *
     * @param id
     * @param callback
     */
    function getSalutations(id, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/salutations";
        if (id)
            apiRoute += "/" + id;

        $http.get(apiRoute).then(function (response) {
            if (response && response.data) {
                salutations = response.data;
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Anreden konnten nicht erfasst werden"));
    }

    return {
        salutations: function () {
            return salutations;
        },

        getSalutations: getSalutations
    }
}