<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for daysheets database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class DaysheetsController extends DaysheetBaseController
{

    /**
     * Reads the daysheet, calculates times, creates the pdf and sends it via mail
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function checkout(Request $request, Response $response) {
        $id = $request->getAttribute('id');
        $timestamp = $request->getAttribute('timestamp');

        if (!$timestamp)
            $out = $this->calculateEnd($id);
        else
            $out = $this->calculateEnd($id, false, $timestamp);

        return $response->getBody()->write(json_encode($out));
    }

    /**
     * Returns all daysheets or a daysheet selected by id
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function get(Request $request, Response $response)
    {
        $id = ($request->getAttribute("id") ? $request->getAttribute("id") : null);

        $sheet = $this->getDaysheets($id);

        return $response->getBody()->write(json_encode($sheet));
    }

    /**
     * Function to insert a new daysheet
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        $userId = $request->getAttribute('id');

        $sheet = $this->startDay($userId);

        return $response->getBody()->write(json_encode($sheet));
    }

    /**
     * Returns all daysheets of a user
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function getByUser(Request $request, Response $response) {
        $id = $request->getAttribute("id");

        $sheet = $this->getDaysheets($id, true);

        return $response->getBody()->write(json_encode($sheet));
    }

    /**
     * Returns the last daysheet of the user
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function getLastByUser(Request $request, Response $response) {
        $id = $request->getAttribute("id");

        $sheet = $this->getDaysheets($id, true, true);

        return $response->getBody()->write(json_encode($sheet));
    }

    /**
     * Returns the active daysheet of a user
     * (where finishdate is null)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function getActiveByUser(Request $request, Response $response) {
        $id = $request->getAttribute("id");

        $sheet = $this->getDaysheets($id, true, true, true);

        return $response->getBody()->write(json_encode($sheet));
    }

}