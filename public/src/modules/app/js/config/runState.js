/**
 * run configuration and listeners of the app
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($rootScope, $location, Login) {

    $rootScope.$on('$routeChangeStart', function (event, target) {
        var userPower = (Login.user().powers ? Login.user().powers.CHANGE_PATH : 0);

        if ((target.$$route.authenticationRequired && !Login.user().isLoggedIn) && target.$$route.requiredPower >= userPower) {
            $location.path('/').replace();
        }
    });

}