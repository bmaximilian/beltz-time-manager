<?php
include_once '../app/helper/ContainerCreator.php';
include_once '../app/helper/Contentloader.php';
include_once '../app/helper/MysqlPdo.php';
include_once '../app/helper/SFtp.php';
include_once '../app/base/Utils.php';
include_once '../vendor/autoload.php';

use Jgut\Slim\PHPDI\ContainerBuilder;

/**
 * Class ContentloaderTest
 *
 * Tests the database functions
 */
class ContentloaderTest extends PHPUnit_Framework_TestCase
{
    private $container;
    private $loader;

    /**
     * Tests Building of a select query
     */
    public function testBuildSelect()
    {
        $this->initContainer();

        $select = "";

        if ($this->loader instanceof Contentloader) {
            $select = $this->loader->buildSelect("status", [
                "designation" => "name",
                "icon",
                "description"
            ], [
                "pk" => "!NULL"
            ]);
        }

        $this->assertEquals(
            "SELECT status.designation AS name, status.icon, status.description FROM status  WHERE status.pk IS NOT NULL;",
            $select
        );
    }

    /**
     * Initializes the application container
     */
    private function initContainer()
    {
        $config = Utils::getIni("slim.ini", "../bin/");

        $this->container = $container = ContainerBuilder::build();
        $builder = new ContainerCreator($container);

        $this->container = $builder->initContainer($config);
        $this->loader = $this->container->get("loader");
    }

    /**
     * Tests INSERT, UPDATE and DELETE for the table status
     */
    public function testDatabase()
    {
        $this->initContainer();

        $insert = null;
        $update = null;
        $delete = null;

        if ($this->loader instanceof Contentloader) {
            $insert = $this->loader->insert("status", [
                "designation" => "Test",
                "description" => "Zu testzewcken erstellt"
            ]);

            if ($insert) {
                $pk = $insert['pk'];
                $insert = true;

                $update = ($this->loader->update("status", [
                    "description" => "Zu testzwecken geupdated"
                ], [
                    "pk" => $pk
                ]) ? true : false);

                $delete = ($this->loader->delete("status", [
                    "pk" => $pk
                ]) ? true : false);
            }
        }

        $this->assertEquals(
            true,
            $insert
        );

        $this->assertEquals(
            true,
            $update
        );

        $this->assertEquals(
            true,
            $delete
        );
    }
}
