<?php

use Jgut\Slim\PHPDI\ContainerBuilder;
use Jgut\Slim\Controller\Controller;

/**
 * Base class for controllers
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
abstract class BaseController implements Controller
{

    /**
     * Application Container
     *
     * @var \Interop\Container\ContainerInterface
     */
    protected $container;


    /**
     * Returns application container
     *
     * @return \Interop\Container\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }


    /**
     * Sets application container
     *
     * @param \Interop\Container\ContainerInterface $container
     */
    public function setContainer(\Interop\Container\ContainerInterface &$container)
    {
        $this->container = $container;
    }


    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $settings = [
            'settings' => [
                'php-di' => [
                    'use_autowiring' => true,
                    'use_annotations' => false,
                ],
            ],
        ];

        $container = ContainerBuilder::build($settings);
        $config = Utils::getIni("slim.ini");

        $builder = new ContainerCreator($container);
        $container = $builder->initContainer($config);

        $this->container = $container;
    }

}