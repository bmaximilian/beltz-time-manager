export default function() {
    function getUserToken() {
        try {

            let user = JSON.parse(localStorage.getItem("activeUser"));

            return user.token;

        } catch (e) {}
        return null;
    }

    return {
        request: function (config) {

            config.headers['x-auth-token'] = getUserToken();

            return config;
        }
    };
}