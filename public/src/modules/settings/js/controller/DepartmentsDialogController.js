import Helper from '../../../app/js/classes/Helper.class';

/**
 * controller for the departments management functions of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdDialog, department, action, DepartmentsFactory, PositionsFactory) {
    "use strict";

    if (action === 'edit')
        $scope.headline = "Bearbeite Abteilung";
    else if (action === 'add')
        $scope.headline = "Füge Abteilung hinzu";

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.department = {
        name: null,
        description: null,
        positions: []
    };

    if (department) {
        $scope.department = department;
        DepartmentsFactory.getDepartments(department.id, function (dep) {
            if (dep && dep.length == 1)
                $scope.department = dep[0];
        });
    }

    var positions = [];
    PositionsFactory.getPositions(null, function (data) {
        positions = data
    });


    $scope.selectedPositions = null;
    $scope.autocomplete = {
        selectedItem: null,
        searchText: null
    };
    $scope.positionsSearch = function (text) {
        return text ? positions.filter(createFilterFor(text)) : [];
    };


    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(position) {
            if (position.description)
                return (position.name.toLowerCase().includes(lowercaseQuery)) ||
                    (position.description.toLowerCase().includes(lowercaseQuery));
            else
                return (position.name.toLowerCase().includes(lowercaseQuery));
        };

    }

    /**
     * Return the proper object when the append is called.
     */
    $scope.transformChip = function (chip) {
        // If it is an object, it's already a known chip
        if (angular.isObject(chip)) {
            return chip;
        }

        // Otherwise, create a new one
        return {name: chip, description: 'new'}
    };

    /**
     * Validates the input data
     *
     * @returns {boolean}
     */
    function validate() {
        var valid = false;
        if (Helper.isEmpty($scope.departmentsForm.$error))
            valid = true;
        else if ($scope.departmentsForm.$error.required)
            valid = ($scope.departmentsForm.$error.required.length == 0);
        else if ($scope.departmentsForm.$error.pattern)
            valid = $scope.departmentsForm.$error.pattern.length == 0;
        return valid;
    }

    $scope.submit = function () {
        if (validate()) {
            if (action === 'edit') {
                DepartmentsFactory.updateDepartment($scope.department.id, $scope.department, function (data) {
                    DepartmentsFactory.getDepartments(null, function (data) {
                        $mdDialog.hide({
                            message: 'Abteilung erfolgreich bearbeitet',
                            data: data
                        });
                    });
                });
            }

            else if (action === 'add') {
                DepartmentsFactory.insertDepartment($scope.department, function (data) {
                    DepartmentsFactory.getDepartments(null, function (data) {
                        $mdDialog.hide({
                            message: 'Abteilung erfolgreich hinzugefügt',
                            data: data
                        });
                    });
                });
            }
        }
    };
}