import RolesController from '../controller/RolesController'

/**
 * directive for the roles panel
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function (BaseUrl) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: BaseUrl.viewPath + "settings/views/rolesView.html",
        controller: RolesController
    }
};