<?php

/**
 * Extension to PHP Database Objects
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class MysqlPdo extends PDO
{

    /**
     * MysqlPdo constructor.
     * @param $settings
     */
    public function __construct($settings)
    {
        $dns = $settings['driver'] .
            ':host=' . $settings['host'] .
            ((!empty($settings['port'])) ? (';port=' . $settings['port']) : '') .
            ';dbname=' . $settings['schema'] . ';charset=' . $settings['charset'];

        parent::__construct($dns, $settings['user'], $settings['password']);
    }
}