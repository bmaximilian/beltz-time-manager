import UsersDialogController from '../controller/UsersDialogController'

var allUsers = [];

/**
 * controller for the user management of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdDialog, $mdToast, BaseUrl, Users) {

    Users.getUsers(null, function (data) {
        $scope.users = data;
    });

    $scope.usersToggle = true;

    $scope.changeUsersToggle = function () {
        $scope.usersToggle = ($scope.usersToggle ? false : true);
    };

    if (!$scope.text)
        $scope.text = {};
    $scope.text.usersSearchInput = '';

    $scope.changeUserSearch = function () {
        if ($scope.text.usersSearchInput.length > 0) {
            if (allUsers.length === 0)
                allUsers = $scope.users;

            $scope.users = allUsers.filter(function (user) {
                return user.name.toLowerCase().includes($scope.text.usersSearchInput.toLowerCase())
                    || user.emailBusiness.toLowerCase().includes($scope.text.usersSearchInput.toLowerCase())
                    || user.role.toLowerCase().includes($scope.text.usersSearchInput.toLowerCase())
                    || user.position.toLowerCase().includes($scope.text.usersSearchInput.toLowerCase())
                    || user.department.toLowerCase().includes($scope.text.usersSearchInput.toLowerCase());
            });
        } else {
            $scope.users = allUsers;
        }
    };

    $scope.openUsersEditDialog = function (ev, user) {
        $mdDialog.show({
            controller: UsersDialogController,
            templateUrl: BaseUrl.viewPath + "settings/views/userDialog.html",
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                user: user,
                action: 'edit'
            }
        })
            .then(function (answer) {
                $scope.users = answer.data;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(answer.message)
                        .position('top left')
                        .hideDelay(3000)
                        .parent(angular.element(document.getElementById('content')))
                );
            });
    };

    $scope.openUsersAddDialog = function (ev) {
        $mdDialog.show({
            controller: UsersDialogController,
            templateUrl: BaseUrl.viewPath + "settings/views/userDialog.html",
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                user: {
                    positionId: null,
                    position: null,
                    departmentId: null,
                    department: null,
                    roleId: null,
                    role: null,
                    statusId: null,
                    status: null,
                    salutationId: null,
                    salutation: null,
                    picture: null,
                    prefix: null,
                    firstname: null,
                    secondFirstname: null,
                    lastname: null,
                    personelNumber: null,
                    password: null,
                    emailBusiness: null,
                    emailPrivate: null,
                    phoneBusiness: null,
                    phonePrivate: null,
                    workingHoursPerDay: null,
                    overtimeLimit: null,
                    overtimeValue: null
                },
                action: 'add'
            }
        })
            .then(function (answer) {
                $scope.users = answer.data;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(answer.message)
                        .position('top left')
                        .hideDelay(3000)
                        .parent(angular.element(document.getElementById('content')))
                );
            });
    };

    $scope.deleteUser = function (id) {
        Users.deleteUser(id, function () {
            Users.getUsers(null, function (data) {
                $scope.users = data;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent("Benutzer erfolgreich gelöscht")
                        .position('top left')
                        .hideDelay(3000)
                        .parent(angular.element(document.getElementById('content')))
                );
            });
        })
    };
}