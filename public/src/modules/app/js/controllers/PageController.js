/**
 * controller for the access management of the pages
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
class PageController {
    /**
     * Returns the Item passed by name
     * as JSON Object from local storage
     *
     * @param name
     * @returns {*}
     * @protected
     */
    static _getStorageItem(name) {
        "use strict";
        let item = null;
        try {
            item = JSON.parse(localStorage.getItem(name));
        } catch (e) {
        }
        return item;
    }

    /**
     * PageController
     *
     * @param $scope
     * @param $interval
     */
    static controller($scope, $interval) {
        $scope.page = {
            activeUser: PageController._getStorageItem('activeUser')
        };

        $interval(function () {
            $scope.page.activeUser = PageController._getStorageItem('activeUser');
        }, 60000);
    }
}

export default PageController.controller;