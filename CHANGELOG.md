### 1.0.0 (April 13, 2017)

* First stable
    * Frame app tested
    * Settings module tested
    * Daysheets module tested
* Fixed time calculation
* Icons changed
* Show all daysheets


### 0.2.2 (April 12, 2017)

* Show old daysheets
* Shorten notes of a task when longer than 70 characters
* Edit own user account
* Role management for some directives added


### 0.2.1 (April 11, 2017)

* Deny login when user is logged in on an other browser
* Autocomplete for Timetypes
* Welcome message
* BugFix in daysheets ajax call to get a daysheet


## 0.2.0 (April 10, 2017)

* Folder structure changed
* Database script added
* Ajax Base URL simply changeable in the BaseURL provider
* Autocomplete for costcenters in dialog for adding a task
* Set enddate of last task


### 0.1.1 (March 23, 2017)

* Fixed some backend bugs
    * INSERT, UPDATE, DELETE roles with powers
    * INSERT, UPDATE, DELETE departments with positions
    * Daysheet functions


## 0.1.0 (March 22, 2017)

* Backend Functions created
    * API for Authentication implemented
    * API for Settings module implemented
    * API for Daysheets module implemented
* PDF creator
* Mail functions
* Loading recipients from configuration file
* Basic frame of AngularJS view


### 0.0.2 (March 21, 2017)

* Installation manual for PHP app
* Routes for settings module
* Folder structure changed


### 0.0.1 (March 20, 2017)

* Project initialized
* Base framework created