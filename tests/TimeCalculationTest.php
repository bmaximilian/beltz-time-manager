<?php
include_once '../app/traits/TimeCalculation.php';

/**
 * Class TimeCalculationTest
 *
 * Test class tor time calculation trait
 */
class TimeCalculationTest extends PHPUnit_Framework_TestCase
{

    use TimeCalculation;

    private $tasks = [
        [
            "costcenterId" => 1,
            "startdate" => "2017-04-11 14:03:36",
            "enddate" => "2017-04-11 14:03:54"
        ],
        [
            "costcenterId" => -1,
            "startdate" => "2017-04-11 14:03:54",
            "enddate" => "2017-04-11 14:19:45"
        ],
        [
            "costcenterId" => 1,
            "startdate" => "2017-04-11 14:19:45",
            "enddate" => "2017-04-11 21:00:00"
        ]
    ];

    /**
     * Test if the working time (for the second task) is calculated right
     */
    public function testCanCalculateWorkingTime()
    {
        $calculated = $this->calculateTime();

        $this->assertEquals(
            6.68,
            $calculated['completeHours']
        );
    }

    /**
     * Calculates time with 8 working hours per day
     *
     * @return array
     */
    private function calculateTime()
    {

        return $this->calculateHours($this->tasks, 8);
    }

    /**
     * Test if the overtime (for the second task) is calculated right
     */
    public function testCanCalculateOverime()
    {
        $calculated = $this->calculateTime();

        $this->assertEquals(
            -1.32,
            $calculated['overtime']
        );
    }

    /**
     * Test if the duration in milliseconds (for the second task) is resolved right
     */
    public function testResolvesRightMs()
    {
        $this->assertEquals(
            24015000,
            $this->resolveMs()
        );
    }

    /**
     * Resolves the duration of the second task in milliseconds
     *
     * @return int
     */
    private function resolveMs()
    {
        return $this->getDifferenceInMilliseconds($this->tasks[2]["startdate"], $this->tasks[2]["enddate"]);
    }

    /**
     * Test if the weekday of the date string is changed right
     */
    public function testChangeWeekday()
    {

        $string = "Wednesday, 12 May 2017 (09.13 bis 19.46) + 1.01 Stunden";

        $this->assertEquals(
            "Mittwoch, 12 Mai 2017 (09.13 bis 19.46) + 1.01 Stunden",
            $this->changeWeekday($string)
        );
    }
}
