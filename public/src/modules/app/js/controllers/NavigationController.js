/**
 * controller for the angular material menu navigation
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdSidenav) {
    $scope.toggleLeftNav = function () {
        $mdSidenav('leftNavigation').toggle();
    };

    $scope.openPersonMenu = function ($mdMenu, e) {
        $mdMenu.open(e);
    };
}