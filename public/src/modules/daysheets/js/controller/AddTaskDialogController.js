import Daysheet from '../classes/Daysheet.class'
import Helper from '../../../app/js/classes/Helper.class'
import ControllerBase from '../classes/ControllerBase'

/**
 * controller to add a new task for the current day
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
class AddTaskDialogController extends ControllerBase {

    /**
     * Returns the powers of the user in the local storage
     *
     * @returns {{OFFLINE_WORKING: number, CREATE_JOBS: number}}
     * @private
     */
    static _getPowers() {
        let user = AddTaskDialogController._getStorageItem("activeUser");
        return (user ? user.powers : {
            OFFLINE_WORKING: 0,
            CREATE_JOBS: 0
        });
    }

    /**
     * AddTaskDialogController
     *
     * @param $scope
     * @param $mdDialog
     * @param $mdToast
     * @param sheet
     * @param task
     * @param JobsFactory
     * @param MaterialsFactory
     * @param CostcentersFactory
     * @param DaysheetFactory
     */
    static controller($scope, $mdDialog, $mdToast, sheet, task, JobsFactory, MaterialsFactory, CostcentersFactory, DaysheetFactory) {
        "use strict";

        $scope.newTask = {
            _powers: AddTaskDialogController._getPowers()
        };
        if (task) {
            $scope.newTask = task;

            $scope.newTask._starttime = Daysheet.timestampToTime(task.startdate);
            if (task.enddate)
                $scope.newTask._endtime = Daysheet.timestampToTime(task.enddate);
        }

        $scope.jobsComplete = {
            searchText: null,
            createFilterFor: function (query) {
                var lowercaseQuery = query.toLowerCase();

                return function filterFn(job) {
                    return (job.jobnumber.includes(lowercaseQuery)
                        || job.jobDesignation.toLowerCase().includes(lowercaseQuery))
                        || query.length == 0;
                };
            },
            querySearch: function (query) {
                return query ? $scope.jobs.filter(this.createFilterFor(query)) : $scope.jobs;
            }
        };

        $scope.costcentersComplete = {
            searchText: null,
            createFilterFor: function (query) {
                let lowercaseQuery = query.toLowerCase();

                return function filterFn(costcenter) {
                    return (costcenter.identifier.includes(lowercaseQuery)
                        || costcenter.name.toLowerCase().includes(lowercaseQuery))
                        || query.length == 0;
                }
            },
            querySearch: function (query) {
                return query ? $scope.costcenters.filter(this.createFilterFor(query)) : $scope.costcenters;
            }
        };

        $scope.timetypesComplete = {
            searchText: null,
            createFilterFor: function (query) {
                let lowercaseQuery = query.toLowerCase();

                return function filterFn(timetype) {
                    return (timetype.identifier.includes(lowercaseQuery)
                        || timetype.name.toLowerCase().includes(lowercaseQuery))
                        || query.length == 0;
                }
            },
            querySearch: function (query) {
                return query ? $scope.newTask._costcenter.timetypes.filter(this.createFilterFor(query)) : $scope.newTask._costcenter.timetypes;
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function validate(task) {
            var valid = true;
            if (!task.costcenterId)
                valid = false;
            else if (task.costcenterId > 0) {
                if (!task.timetypeId)
                    valid = false;
                if (!task.jobId)
                    valid = false;
            }

            return valid;
        }

        $scope.submit = function () {
            if ($scope.newTask._starttime) {
                $scope.newTask.startdate = Daysheet.parseTimeToTimestamp($scope.newTask._starttime);
                $scope.newTask = Daysheet.checkStartdate($scope.newTask, sheet.startdate, sheet.tasks)
            }

            if ($scope.newTask._endtime) {
                $scope.newTask.enddate = Daysheet.parseTimeToTimestamp($scope.newTask._endtime);
                $scope.newTask = Daysheet.checkEnddate($scope.newTask, sheet.startdate, sheet.tasks)
            }

            let newTask = Helper.cleanup($scope.newTask);

            if (Helper.validateForm($scope.newTaskForm) && validate(newTask)) {
                if ((!task || !task.id) && newTask) {
                    DaysheetFactory.insertTask(newTask, sheet.id, function (data) {
                        if (data) {
                            DaysheetFactory.getDaysheets(sheet.id, function (daysheet) {
                                if (daysheet && daysheet.length == 1)
                                    $mdDialog.hide({
                                        message: 'Aufgabe erfolgreich hinzugefügt',
                                        data: daysheet[0]
                                    });
                            });
                        }
                    });
                } else if ((task && task.id) && newTask.id) {
                    DaysheetFactory.updateTask(newTask.id, newTask, function (data) {
                        if (data) {
                            DaysheetFactory.getDaysheets(sheet.id, function (daysheet) {
                                if (daysheet && daysheet.length == 1)
                                    $mdDialog.hide({
                                        message: 'Aufgabe erfolgreich geändert',
                                        data: daysheet[0]
                                    });
                            });
                        }
                    });
                }
            }
        };

        $scope.costcenters = [];
        $scope.timetypes = [];
        $scope.jobs = [];
        $scope.partjobs = [];
        $scope.materials = [];

        JobsFactory.getJobs(null, function (jobs) {
            $scope.jobs = jobs;

            if ($scope.newTask.jobId) {
                for (let job of jobs) {
                    if (job.id == $scope.newTask.jobId)
                        $scope.newTask._job = job;
                }
            }

            $scope.setPartjobs();
        });

        MaterialsFactory.getMaterials(function (materials) {
            $scope.materials = materials;
        });

        CostcentersFactory.getCostcenters(function (costcenters) {
            $scope.costcenters = costcenters;

            if ($scope.newTask.costcenterId) {
                for (let costcenter of costcenters) {
                    if (costcenter.id == $scope.newTask.costcenterId)
                        $scope.newTask._costcenter = costcenter;
                }
            }

            $scope.setTimetypes();
        });

        $scope.setTimetypes = function () {
            $scope.newTask.costcenterId = ($scope.newTask._costcenter ? $scope.newTask._costcenter.id : null);

            var idFound = false;

            for (var center of $scope.costcenters) {
                if ($scope.newTask.costcenterId == center.id) {
                    $scope.timetypes = center.timetypes;
                    idFound = true;

                    var typeFound = false;
                    for (var type of $scope.timetypes) {
                        if (type.id == $scope.newTask.timetypeId) {
                            typeFound = true;
                            $scope.newTask._timetype = type;
                        }
                    }
                    if (!typeFound) {
                        $scope.newTask.timetypeId = null;
                        $scope.newTask._timetype = null;
                    }
                }
            }
        };

        $scope.updateNewtaskTimetype = function () {
            $scope.newTask.timetypeId = ($scope.newTask._timetype ? $scope.newTask._timetype.id : null);
        };

        $scope.setPartjobs = function () {
            var idFound = false;

            for (var job of $scope.jobs) {
                if ($scope.newTask._job && $scope.newTask._job.id == job.id) {
                    $scope.newTask.jobId = job.id;
                    $scope.partjobs = job.parts;
                    idFound = true;

                    var partFound = false;
                    for (var part of $scope.partjobs) {
                        if (part.id == $scope.newTask.partjobId)
                            partFound = true;
                    }
                    if (!partFound)
                        $scope.newTask.partjobId = null;
                }
            }

            if (!idFound)
                $scope.partjobs = [];
        };

        $scope.newJob = function (title) {
            if ($scope.newTask._powers.CREATE_JOBS >= 200) {
                JobsFactory.insertJob({
                    name: title
                }, function () {
                    Helper.standardToast($mdToast, `Auftrag ${title} erstellt`);

                    JobsFactory.getJobs(null, function (jobs) {
                        $scope.jobs = jobs;

                        for (let job of jobs) {
                            if (job.name === title)
                                $scope.newTask._job = job;
                        }
                    });
                });
            } else {
                Helper.standardToast($mdToast, `Keine Berechtigung um Auftrag ${title} zu erstellen`);
            }
        }
    }

}

export default AddTaskDialogController.controller;