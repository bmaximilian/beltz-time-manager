/**
 * Main application context which loads all required modules
 * and configurations for the main angular application
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
// import angular dependencys
import angular from 'angular'
import ngRoute from 'angular-route'
import ngAnimate from 'angular-animate'
import ngAria from 'angular-aria'
import ngMaterial from 'angular-material'

/**
 * To add a module in the dependency configuration
 * of the main module.
 *
 * If the module needs an extra page add it with
 * the path to the template to the route configuration
 * in configState.
 */
// import modules
import settings from './modules/settings/js/app'
import daysheets from './modules/daysheets/js/app'

// import configs and dependencies for main app
import Login from './modules/app/js/services/Login'
import BaseUrl from './modules/app/js/services/BaseUrl'
import HttpRequestInterceptor from './modules/app/js/services/HttpRequestInterceptor'
import configState from './modules/app/js/config/configState'
import runState from './modules/app/js/config/runState'
import routeNavigationFactory from './modules/app/js/services/routeNavigationFactory'
import navigationDirective from './modules/app/js/directives/navigationDirective'
import loginMenuDirective from './modules/app/js/directives/loginMenuDirective'
import onEnterDirective from './modules/app/js/directives/onEnterDirective'
import NavigationController from './modules/app/js/controllers/NavigationController'

/**
 * Defining main app
 */
angular.module('main', [
    ngRoute,
    ngAnimate,
    ngAria,
    ngMaterial,

    settings,
    daysheets
])
    .provider('Login', Login)
    .provider('BaseUrl', BaseUrl)
    .factory('HttpRequestInterceptor', HttpRequestInterceptor)
    .config(configState)
    .factory('routeNavigationFactory', routeNavigationFactory)
    .directive('navigation', navigationDirective)
    .directive('loginMenu', loginMenuDirective)
    .directive('onEnter', onEnterDirective)
    .controller('NavigationController', NavigationController)
    .run(runState);

