<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for jobs database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class JobsController extends RestController
{

    /**
     * Selects all jobs or the job by id
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $return = [];

        if ($id)
            $jobs = $this->loader->select("v_jobs", null, [
                "deletedate" => "NULL",
                "id" => $id
            ]);
        else
            $jobs = $this->loader->select("v_jobs", null, [
                "deletedate" => "NULL"
            ]);

        foreach ($jobs as $job) {
            $job['parts'] = $this->loader->select("partjobs", [
                "pk" => "id",
                "identifier",
                "designation",
                "createdate"
            ], [
                "deletedate" => "NULL",
                "job_pk" => $job['id']
            ]);
            array_push($return, $job);
        }

        return $response->getBody()->write(json_encode($return));
    }


    /**
     * POST function for jobs
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        $insert_data = array(
            "customer_pk" => @$request->getParsedBody()['customerId'],
            "designation" => @$request->getParsedBody()['name'],
            "jobnr" => @$request->getParsedBody()['jobnumber'],
            "description" => @$request->getParsedBody()['description'],
            "recieptdate" => @$request->getParsedBody()['recieptdate']
        );
        $parts = @$request->getParsedBody()['partjobs'];

        if (!$insert_data['customer_pk'] && !$insert_data['jobnr']) {
            $insert_data['recieptdate'] = date("Y-m-d");
            $lastNr = $this->loader->query("SELECT jobs.jobnr AS jobnumber FROM jobs ORDER BY jobs.jobnr ASC LIMIT 1;");
            if ($lastNr && sizeof($lastNr) > 0)
                $lastNr = $lastNr[0];
            else
                $lastNr = 0;

            $insert_data['jobnr'] = $lastNr['jobnumber'] - 1;
            $insert_data['customer_pk'] = -1;
        }

        $job = $this->loader->insert("jobs", $insert_data);
        $finalParts = [];

        if ($parts)
            foreach ($parts as $part) {
                array_push($finalParts, $this->loader->insert("partjobs", [
                    "job_pk" => $job['pk'],
                    "identifier" => $part['identifier'],
                    "designation" => $part['name'],
                ]));
            }
        $job['parts'] = $finalParts;

        return $response->getBody()->write(json_encode($job));
    }

    /**
     * PUT function for jobs
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function put(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * DELETE function for jobs
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function delete(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }
}