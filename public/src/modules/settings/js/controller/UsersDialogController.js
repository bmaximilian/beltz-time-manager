/**
 * controller for the user management of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdDialog, user, action, Users, DepartmentsFactory, RolesFactory, SalutationsFactory) {
    $scope.departments = $scope.positions = $scope.roles = $scope.salutations = [];

    let activeUser = null;
    try {
        activeUser = JSON.parse(localStorage.getItem('activeUser'));
    } catch (e) {
    }
    $scope.userDialogActiveUser = activeUser;

    if (user) {
        user.password = null;

        $scope.user = user;
        $scope.departments = [{
            id: user.departmentId,
            name: user.department
        }];
        $scope.positions = [{
            id: user.positionId,
            name: user.position
        }];
        $scope.roles = [{
            id: user.roleId,
            name: user.role
        }];
        $scope.salutations = [{
            id: user.salutationId,
            name: user.salutation
        }]
    }

    $scope.updatePositions = function () {
        angular.forEach($scope.departments, function (department) {
            if ($scope.user.departmentId == department.id) {
                $scope.positions = department.positions;

                var idFound = false;
                angular.forEach(department.positions, function (position) {
                    if (position.id == $scope.user.positionId) {
                        idFound = true;
                    }
                });
                if (!idFound)
                    $scope.user.positionId = $scope.user.position = null;
            }
        });
    };

    DepartmentsFactory.getDepartments(null, function (data) {
        $scope.departments = data;
        $scope.updatePositions();
    });

    RolesFactory.getRoles(null, function (data) {
        $scope.roles = data;
    });

    SalutationsFactory.getSalutations(null, function (data) {
        $scope.salutations = data;
    });

    if (action === 'edit')
        $scope.headline = "Bearbeite Benutzer";
    else if (action === 'add')
        $scope.headline = "Füge Benutzer hinzu";

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    function validate() {
        //console.log($scope.userForm.$error);
        return ($scope.user.salutationId
            && $scope.user.firstname
            && $scope.user.lastname
            && $scope.user.personelNumber
            && $scope.user.emailBusiness
            && $scope.user.departmentId
            && $scope.user.positionId
            && $scope.user.roleId
            && $scope.user.workingHoursPerDay
            && $scope.user.overtimeLimit
            && $scope.user.overtimeValue
            && ($scope.user.password || $scope.user.id)
        );
    }

    $scope.submit = function () {

        if (validate()) {
            if (action === 'edit') {
                if (!$scope.user.password)
                    delete $scope.user.password;

                Users.updateUser($scope.user.id, $scope.user, function () {
                    Users.getUsers(null, function (data) {
                        $mdDialog.hide({
                            message: 'Benutzer erfolgreich bearbeitet',
                            data: data
                        });
                    });
                });
            }

            else if (action === 'add') {
                Users.insertUser($scope.user, function () {
                    Users.getUsers(null, function (data) {
                        $mdDialog.hide({
                            message: 'Benutzer erfolgreich hinzugefügt',
                            data: data
                        });
                    });
                });
            }
        }
    };
}