<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for salutations database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class SalutationsController extends RestController
{

    /**
     * GET function for salutations
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $out = [];

        if ($id)
            $powers = $this->loader->select("salutations", [
                "pk" => "id",
                "designation" => "name",
                "description"
            ], [
                "pk" => $id
            ]);
        else
            $powers = $this->loader->select("salutations", [
                "pk" => "id",
                "designation" => "name",
                "description"
            ]);

        return $response->getBody()->write(json_encode($powers));
    }

    /**
     * POST function for salutations
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * PUT function for salutations
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function put(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * DELETE function for salutations
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function delete(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

}