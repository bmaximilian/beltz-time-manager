/**
 * Class for time calculating
 * and other functions in Daysheets and Dailytasks
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class Daysheet {

    /**
     * Constructor of Daysheets
     * @param DaysheetFactory
     */
    constructor(DaysheetFactory) {
        this.factory = DaysheetFactory;
    }

    /**
     * Returns the Date object in Milliseconds
     *
     * @param dateString
     * @returns {number}
     */
    static _getMilliseconds(dateString) {
        var date = dateString;

        if (!(dateString instanceof Date))
            date = new Date(dateString);

        let totalMs = 0;
        totalMs += (date.getMonth() + 1) * 2630000000;
        totalMs += date.getDate() * 86400000;
        totalMs += date.getHours() * 3600000;
        totalMs += date.getMinutes() * 60000;
        totalMs += date.getSeconds() * 1000;

        return totalMs;
    }

    static _getDifferenceInMilliseconds(startdate, enddate = null) {
        return Math.abs(Daysheet._getMilliseconds(startdate) - Daysheet._getMilliseconds(enddate));
    }


    /**
     * Checks if the given Date object is today
     *
     * @param date
     * @returns {boolean}
     */
    static _isToday(date) {
        date = new Date(date);
        let today = new Date();

        return date.setHours(0, 0, 0, 0) == today.setHours(0, 0, 0, 0);
    }

    /**
     * Returns the Difference between the dates in minutes
     *
     * @param startdate
     * @param enddate
     * @returns {number}
     */
    static _getDifferenceInMinutes(startdate, enddate = null) {
        if (!enddate)
            enddate = new Date();
        else
            enddate = new Date(enddate);

        startdate = new Date(startdate);

        //let timeDiff = Math.abs(enddate.getTime() - startdate.getTime());
        let timeDiff = Daysheet._getDifferenceInMilliseconds(startdate, enddate);
        return timeDiff / (1000 * 60);
    }

    /**
     * Returns the whole duration in Minutes for the given tasks
     *
     * @param tasks
     * @returns {number}
     */
    static _getTimeFromTasks(tasks) {
        let timeInMinutes = 0;

        if (tasks)
            for (let task of tasks) {
                if (task && task.hasOwnProperty("costcenterId") && task.hasOwnProperty("startdate") && task.hasOwnProperty("enddate"))
                    if (task.costcenterId > 0) {
                        timeInMinutes += Daysheet._getDifferenceInMinutes(task.startdate, task.enddate);
                    }
            }
        return Math.round(timeInMinutes);
    }

    /**
     * Resolves the weekday for a Date object
     *
     * @param day
     * @returns {*}
     */
    static _resolveWeekday(day) {
        switch (day) {
            case 0:
                return "Sonntag";
            case 1:
                return "Montag";
            case 2:
                return "Dienstag";
            case 3:
                return "Mittwoch";
            case 4:
                return "Donnerstag";
            case 5:
                return "Freitag";
            case 6:
                return "Samstag";
        }
    }

    /**
     * Converts a Date object to SQL timestamp
     *
     * @param date Date
     * @returns {string}
     */
    static _dateToTimestamp(date) {

        let month = date.getMonth() + 1;
        if (month < 10)
            month = "0" + month;

        let day = date.getDate();
        if (day < 10)
            day = "0" + day;

        let hour = date.getHours();
        if (hour < 10)
            hour = "0" + hour;

        let minute = date.getMinutes();
        if (minute < 10)
            minute = "0" + minute;

        let second = date.getSeconds();
        if (second < 10)
            second = "0" + second;

        return date.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }


    /**
     * Calculates the full hours
     *
     * @param diffMins
     * @param fillZero
     * @returns {number}
     */
    static _getWorkedHours(diffMins, fillZero) {
        let diffHours = Math.floor(diffMins / 60);

        if (diffHours < 10 && fillZero)
            diffHours = "0" + diffHours;

        return diffHours;
    }

    /**
     * Calculates the minutes to the next full hour
     *
     * @param diffMins : int : Time in minutes
     * @param fillZero
     * @returns {number}
     */
    static _getWorkedMins(diffMins, fillZero) {
        let mins = diffMins % 60;

        if (mins < 10 && fillZero)
            mins = "0" + mins;

        return mins;
    }

    /**
     * Returns the time between startdate and enddate as object
     *
     * @param startdate
     * @param enddate
     * @param fillZero
     * @returns {{hours: *, minutes: *}}
     */
    static returnWorkedTime(startdate, enddate = null, fillZero = true) {
        let diffMins = Math.round(Daysheet._getDifferenceInMinutes(startdate, enddate));

        let diffHours = Daysheet._getWorkedHours(diffMins, fillZero);
        let mins = Daysheet._getWorkedMins(diffMins, fillZero);

        return {
            hours: diffHours,
            minutes: mins
        };
    }

    /**
     * Checks if there is aready a daysheet for the current day
     * and returns it or creates a new one
     *
     * @param userId
     * @param newSheet
     * @param callback
     */
    startDay(userId, newSheet, callback) {
        var self = this;
        this.factory.getLastDaysheetByUser(userId, function (lastSheet) {
            if (lastSheet && lastSheet.length === 1)
                lastSheet = lastSheet[0];

            if (Daysheet._isToday(lastSheet.startdate) && !newSheet)
                callback(lastSheet);
            else {

                self.factory.getActiveDaysheetByUser(userId, function (daysheet) {

                    if (daysheet && daysheet.length === 1) {
                        callback(daysheet[0]);
                    } else {
                        self.factory.insertDaysheet({}, userId, function (daysheet) {
                            callback(daysheet);
                        });
                    }
                });

            }

        });
    }

    /**
     * Calls the ceckout function for the daysheet selected by id
     *
     * @param id
     * @param callback
     * @param timestamp
     */
    endDay(id, callback, timestamp = null) {
        this.factory.checkoutDaysheet(id, callback, timestamp);
    }


    /**
     * Returns the whole time from the startdate of the first task
     * to the enddate of the last task as object
     *
     * @param tasks
     * @param fillZero
     * @returns {{hours: *, minutes: *}}
     */
    static returnWorkedTimeFromTasks(tasks, fillZero = true) {
        let diffMins = Daysheet._getTimeFromTasks(tasks);

        let diffHours = Daysheet._getWorkedHours(diffMins, fillZero);
        let mins = Daysheet._getWorkedMins(diffMins, fillZero);

        return {
            hours: diffHours,
            minutes: mins
        };
    }

    /**
     * Returns the time (hh:mm) between startdate and enddate
     *
     * @param startdate
     * @param enddate
     * @returns {string}
     */
    static calculate(startdate, enddate = null) {
        let time = Daysheet.returnWorkedTime(startdate, enddate);

        return time.hours + ":" + time.minutes;
    }


    /**
     * Returns the whole time (hh:mm) from the startdate of the first task
     * to the enddate of the last task
     *
     * @param tasks
     * @returns {string}
     */
    static calculateTasks(tasks) {
        let timeInMinutes = Daysheet._getTimeFromTasks(tasks);

        let hours = Math.floor(timeInMinutes / 60);
        let minutes = timeInMinutes % 60;

        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10)
            minutes = "0" + minutes;

        return hours + ":" + minutes;
    }

    /**
     * Returns the time (hh:mm) from timestamp
     *
     * @param timestamp
     * @returns {*}
     */
    static timestampToTime(timestamp) {
        if (timestamp) {
            timestamp = new Date(timestamp);

            let hour = timestamp.getHours();
            if (hour < 10)
                hour = "0" + hour;

            let minute = timestamp.getMinutes();
            if (minute < 10)
                minute = "0" + minute;

            return hour + ":" + minute;
        }
        return timestamp;
    }

    /**
     * Converts SQL timestamp into german date string
     *
     * @param timestamp
     * @param withDay
     * @returns {*}
     */
    static timestampToDate(timestamp, withDay = false) {
        if (timestamp) {
            timestamp = new Date(timestamp);

            let weekday = Daysheet._resolveWeekday(timestamp.getDay());

            let day = timestamp.getDate();
            if (day < 10)
                day = "0" + day;

            let month = timestamp.getMonth() + 1;
            if (month < 10)
                month = "0" + month;

            let ret = `${day}.${month}.${timestamp.getFullYear()}`;

            if (withDay)
                ret = `${weekday}, ` + ret;

            return ret;
        }
        return timestamp;
    }


    /**
     * Parses time (hh:mm) to SQL timestamp
     *
     * @param time
     * @param startdate
     * @returns {*}
     */
    static parseTimeToTimestamp(time, startdate = null) {
        let timestamp = null;

        const matchPattern = /^([0-2]\d):([0-5]\d)$/;
        let match = matchPattern.exec(time);

        if (match) {
            let hours = match[1];
            let minutes = match[2];

            if (hours < 24) {
                timestamp = new Date();
                timestamp.setHours(hours, minutes, 0, 0);

                if (startdate) {
                    let date = new Date(startdate);
                    timestamp.setMonth(date.getMonth());
                    timestamp.setDate(date.getDate());
                    timestamp.setFullYear(date.getFullYear());
                }

                timestamp = Daysheet._dateToTimestamp(timestamp);
            }
        }

        return timestamp;
    }

    /**
     * Checks if enddate is between enddate of previous task and enddate
     * Returns task
     *
     * @param task
     * @param sheetStartDate
     * @param tasks
     * @returns {*}
     */
    static checkStartdate(task, sheetStartDate, tasks) {
        if (task && sheetStartDate && tasks && tasks.length > 0) {
            let taskIdentifier = 0;
            for (let i = 0; i < tasks.length; i++) {
                if (task.id == tasks[i].id) {
                    taskIdentifier = i;
                }
            }


            let previousEnd = new Date(sheetStartDate);
            let previousEndString = sheetStartDate;

            if (taskIdentifier > 0) {
                previousEnd = new Date(tasks[taskIdentifier - 1].enddate);
                previousEndString = tasks[taskIdentifier - 1].enddate;
            }

            let thisStart = new Date(task.startdate);

            if (task.enddate) {
                let thisEnd = new Date(task.enddate);

                if (thisEnd < thisStart)
                    task.startdate = task.enddate;
            }

            if (previousEnd > thisStart)
                task.startdate = previousEndString;


        }

        return task;
    }


    /**
     * Checks if enddate of task is between startdate
     * and startdate of next task
     * Returns task
     *
     * @param task
     * @param sheetStartDate
     * @param tasks
     * @returns {*}
     */
    static checkEnddate(task, sheetStartDate, tasks) {
        if (task && sheetStartDate && tasks && tasks.length > 0) {
            let taskIdentifier = 0;
            for (let i = 0; i < tasks.length; i++) {
                if (task.id == tasks[i].id) {
                    taskIdentifier = i;
                }
            }

            let nextStart = new Date();

            if (taskIdentifier + 1 < tasks.length) {
                nextStart = new Date(tasks[taskIdentifier + 1].startdate);
            }

            let thisEnd = new Date(task.enddate);
            let thisStart = new Date(task.startdate);

            if (thisEnd < thisStart)
                task.enddate = task.startdate;

            if (thisEnd > nextStart)
                task.enddate = Daysheet._dateToTimestamp(nextStart);

        }

        return task;
    }
}

export default Daysheet;