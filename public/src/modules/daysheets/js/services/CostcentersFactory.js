import AjaxProvider from '../classes/AjaxProvider.class'

/**
 * Factory to store the costcenters data
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default function ($mdToast, $http, BaseUrl) {
    var Ajax = new AjaxProvider($http, $mdToast);
    return {
        getCostcenters: Ajax.fnGet(BaseUrl.baseAjax + "/api/costcenters")
    }
}