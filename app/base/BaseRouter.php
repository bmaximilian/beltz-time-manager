<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Lightweight middleware for Slim application
 * Includes all files used for the routing of the app
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
abstract class BaseRouter
{
    /**
     * Slim Application
     *
     * @var \Slim\App
     */
    protected $app;


    /**
     * BaseRouter constructor.
     * @param \Slim\App $app
     */
    protected function __construct(\Slim\App $app)
    {
        $this->app = $app;

        /* Add Middleware */
        $this->resolveTrailingSlash();
        $this->addCrossOrigin();
        $this->authenticate();
    }

    /**
     * Adds Middleware to resolve a trailing slash in passed uris
     */
    private function resolveTrailingSlash() {

        $this->app->add(function (Request $request, Response $response, callable $next) {
            $uri = $request->getUri();
            $path = $uri->getPath();
            if ($path != '/' && substr($path, -1) == '/') {
                // permanently redirect paths with a trailing slash
                // to their non-trailing counterpart
                $uri = $uri->withPath(substr($path, 0, -1));
                if ($request->getMethod() == 'GET') {
                    return $response->withRedirect((string)$uri, 301);
                } else {
                    return $next($request->withUri($uri), $response);
                }
            }
            return $next($request, $response);
        });

    }


    /**
     * Enables cross origin support for uris
     */
    private function addCrossOrigin() {
        $this->app->add(function (Request $request, Response $response, callable $next) {
            $nextResponse = $next($request, $response);
            return $nextResponse
                ->withHeader('Access-Control-Allow-Origin', 'http://localhost:8000')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        });
    }

    /**
     * Authenticates with passed token
     * if no token is passed and route requires token
     * a redirect will be send
     */
    private function authenticate() {
        $this->app->add(function (Request $request, Response $response, callable $next) {
            $auth = new AuthenticationController();
            $uri = $request->getUri();
            $path = $uri->getPath();

            if ($path != '/' && !Utils::startsWith($path, "/api/auth/login") && !Utils::startsWith($path, "/api/errors")) {
                $token = $request->getHeaderLine("x-auth-token");

                if ($token) {
                    if ($auth->authenticate($token))
                        return $next($request, $response);
                }

                return $response->withRedirect("/api/errors/401", 401);
            }

            return $next($request, $response);
        });
    }
}