import Helper from '../../../app/js/classes/Helper.class'

/**
 * Factory to store the daysheet data
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default function ($mdToast, $http, BaseUrl) {
    var daysheets = [];

    $http.get(BaseUrl.baseAjax + "/api/daysheets").then(function (response) {
        if (response && response.data && response.data.length > 0) {
            daysheets = response.data;
        }
    }, Helper.connectionError($mdToast, "Tageszettel konnten nicht erfasst werden"));


    /**
     * Returns all daysheets or all daysheets of the given user
     * or the daysheet given by id
     *
     * @param id
     * @param callback
     * @param byUser
     */
    function getDaysheets(id, callback, byUser = false) {
        var apiRoute = BaseUrl.baseAjax + "/api/daysheets";

        if (id && byUser)
            apiRoute += "/byUser";

        if (id)
            apiRoute += "/" + id;

        $http.get(apiRoute).then(function (response) {
            if (response && response.data) {
                daysheets = response.data;
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Tageszettel konnten nicht erfasst werden"));
    }

    /**
     * Returns the last daysheet of the user given as id
     *
     * @param id
     * @param callback
     */
    function getLastDaysheetByUser(id, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/daysheets/lastByUser/" + id;

        if (id) {
            $http.get(apiRoute).then(function (response) {
                if (response && response.data) {
                    daysheets = response.data;
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Tageszettel konnten nicht erfasst werden"));
        }
    }

    /**
     * Returns the last daysheet (where the enddate is null)
     * of the user given as id
     *
     * @param id
     * @param callback
     */
    function getActiveDaysheetByUser(id, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/daysheets/activeByUser/" + id;

        if (id) {
            $http.get(apiRoute).then(function (response) {
                if (response && response.data) {
                    daysheets = response.data;
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Tageszettel konnten nicht erfasst werden"));
        }
    }

    /**
     * Inserts a new daysheet for the user given as id
     *
     * @param data
     * @param userId
     * @param callback
     */
    function insertDaysheet(data, userId, callback) {
        if (userId) {
            var apiRoute = BaseUrl.baseAjax + "/api/daysheets/" + userId;

            $http.post(apiRoute, data).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Tageszettel konnten nicht hinzugefügt werden"));
        }
    }

    /**
     * Calls the function to end the day and send the daysheet as mail
     *
     * @param id
     * @param callback
     * @param timestamp
     */
    function checkoutDaysheet(id, callback, timestamp = null) {
        if (id) {
            var apiRoute = BaseUrl.baseAjax + "/api/checkout/" + id;

            if (timestamp)
                apiRoute += `/${timestamp}`;

            $http.post(apiRoute, {}).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Tageszettel konnten nicht hinzugefügt werden"));
        }
    }

    /**
     * Updates the task given as id
     *
     * @param id
     * @param data
     * @param callback
     */
    function updateTask(id, data, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/dailytasks/" + id;

        if (id)
            $http.put(apiRoute, data).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Aufgabe konnte nicht geändert werden"));
    }

    /**
     * Inserts a task to the daysheet given as id
     *
     * @param data
     * @param sheetId
     * @param callback
     */
    function insertTask(data, sheetId, callback) {
        if (sheetId) {
            var apiRoute = BaseUrl.baseAjax + "/api/dailytasks/" + sheetId;

            $http.post(apiRoute, data).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Aufgabe konnte nicht hinzugefügt werden"));
        }
    }

    return {
        daysheets: function () {
            return daysheets;
        },

        getDaysheets: getDaysheets,

        getLastDaysheetByUser: getLastDaysheetByUser,

        getActiveDaysheetByUser: getActiveDaysheetByUser,

        checkoutDaysheet: checkoutDaysheet,

        insertDaysheet: insertDaysheet,

        insertTask: insertTask,

        updateTask: updateTask
    }
}