import DepartmentsController from '../controller/DepartmentsController'

/**
 * directive for the roles panel
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */

export default function (BaseUrl) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: BaseUrl.viewPath + "settings/views/departmentsView.html",
        controller: DepartmentsController
    }
};