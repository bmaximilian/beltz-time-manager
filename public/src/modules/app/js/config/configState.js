import PageController from '../controllers/PageController'

/**
 * provides the provider configuration of the app
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($routeProvider, $httpProvider, $qProvider, LoginProvider, BaseUrlProvider, UsersProvider) {
    $qProvider.errorOnUnhandledRejections(false);

    BaseUrlProvider.setPath('/public/');
    BaseUrlProvider.setAjax("//localhost");

    LoginProvider.setLoginRoute(BaseUrlProvider.baseAjax + '/api/auth/login');
    LoginProvider.setLogoutRoute(BaseUrlProvider.baseAjax + '/api/auth/logout');

    UsersProvider.setApiRoute(BaseUrlProvider.baseAjax + "/api/users");

    $httpProvider.interceptors.push('HttpRequestInterceptor');

    $routeProvider
        .when("/", {
            templateUrl: BaseUrlProvider.viewPath + 'app/views/start.html',
            controller: PageController,
            name: 'Index',
            icon: 'fa-home',
            authenticationRequired: false,
            requiredPower: 0
        })
        .when("/sheet", {
            templateUrl: BaseUrlProvider.viewPath + 'app/views/daysheet.html',
            controller: PageController,
            name: 'Tageszettel',
            icon: 'fa-clock-o',
            authenticationRequired: true,
            requiredPower: 50
        })
        .when("/settings", {
            templateUrl: BaseUrlProvider.viewPath + 'app/views/settings.html',
            controller: PageController,
            name: 'Settings',
            icon: 'fa-cogs',
            authenticationRequired: true,
            requiredPower: 255
        })
        .otherwise({
            redirectTo: '/'
        });
}