<?php
/**
 * Trait for mailing helper functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
trait Mailing {

    /**
     * Loads html template for the mail body
     *
     * @param $name
     * @param string $recipient
     * @return string
     */
    private function loadMailBody($name, $recipient = "")
    {
        $recipient = " $recipient";
        return "
            <!DOCTYPE html>
            <html lang='de'>
            <head>
                <meta charset='utf-8'>
                <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
                <style>
                    body {
                        font-family: Helvetica,Arial,sans-serif;
                        font-size: 12px;
                    }
                    table {
                        width: 100%;
                    }
                    h1 {
                        color: #1c3583;
                    }
                    header {
                        border-bottom: 2px solid #1c3583;
                        margin-bottom: 40px;
                    }
                    header table {
                        width: 100%;
                    }
                    header table td.headline {
                        vertical-align: bottom;
                        font-size: 16px;
                        font-weight: bold;
                    }
                    header table td.logo {
                        text-align: right;
                    }
                    header table td.logo img {
                        float:right;
                        width:100px;
                    }
                </style>
            </head>
            <body>
                <header>
                    <table>
                        <tr>
                            <td class='headline'>Arbeitszeitnachweis</td>
                            <td class='logo'><img src='cid:logo' /></td>
                        </tr>
                    </table>
                </header>
                <main>
                    <p>Guten Tag$recipient,</p>
                    <br/>
                    <p>Im Anang befindet sich mein Arbeitszeitnachweis für den heutigen Tag.</p>
                </main>
                <footer>
                    <p>Viele Grüße</p>
                    <br/>
                    <p>$name</p>
                </footer>
            </body>
            ";
    }

    /**
     * Sends a mail containing the passed data
     *
     * @param PHPMailer $mail : Mailer Class in Container
     * @param $data : Mail Data
     * @return bool
     * @throws phpmailerException
     */
    private function sendMail(PHPMailer $mail, $data)
    {
        $mail->setFrom($data['from'], $data['fromName']);

        $mail->addAddress($data['to'], $data['toName']);

        $mail->addReplyTo($data['reply'], $data['replyName']);

        $mail->addAttachment($data['attachmentPath'], $data['attachmentName']);

        $mail->Subject = $data['subject'];
        $mail->isHTML(true);

        $mail->addEmbeddedImage($data['logoPath'], 'logo');

        $mail->Body = $data['body'];

        return $mail->send();
    }

}