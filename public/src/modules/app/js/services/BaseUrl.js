/**
 * Provider for the basePath of the Application
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function () {
    this.path = "/public/";
    this.distPath = this.path + "dist/";
    this.viewPath = this.distPath + 'views/modules/';

    this.baseAjax = "//localhost";

    this.setPath = function (path) {
        this.path = path;
    };

    this.setAjax = function (ajaxPath) {
        this.baseAjax = ajaxPath;
    };

    this.$get = function () {
        var self = this;

        return {
            path: self.path,
            distPath: self.distPath,
            viewPath: self.viewPath,
            baseAjax: self.baseAjax
        }
    }
}