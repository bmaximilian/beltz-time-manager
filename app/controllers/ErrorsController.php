<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for materials database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class ErrorsController extends BaseController
{
    private $notFoundMessage = "Die angeforderte Seite wurde nicht gefunden";
    private $forbiddenMessage = "Der Zugriff ist verboten.";


    /**
     * Message for 404 error
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function notFound(Request $request, Response $response) {
        return $response->getBody()->write($this->notFoundMessage);
    }

    /**
     * Message for 401 or 403 error
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function forbidden(Request $request, Response $response) {
        return $response->getBody()->write($this->forbiddenMessage);
    }

}