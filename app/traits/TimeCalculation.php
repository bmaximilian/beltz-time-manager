<?php
/**
 * Trait for time calculation helper functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
trait TimeCalculation {

    /**
     * Calculates the number of hours worked on a task and inserts it into tasks
     * Calculates Complete worked hours and overtime
     *
     * @param $tasks
     * @param $dailyWorkingHours
     * @return array
     */
    private function calculateHours($tasks, $dailyWorkingHours)
    {
        $out = array(
            "tasks" => $tasks,
            "completeHours" => 0,
            "overtime" => 0
        );
        $newTasks = [];

        for ($i = 0; $i < sizeof($tasks); $i++) {
            $task = $tasks[$i];

            if ($task['costcenterId'] > 0) {
                $start = $task['startdate'];
                $end = $task['enddate'];

                if (!$end && @$task[$i + 1])
                    $end = $task[$i + 1]['startdate'];
                elseif (!$end)
                    $end = date('Y-m-d H:i:s');

                $taskMs = $this->getDifferenceInMilliseconds($start, $end);
                $taskMin = $taskMs / (1000 * 60);
                $taskHours = $taskMin / 60;

                if ($out['completeHours'] + $taskHours <= $dailyWorkingHours)
                    $out['completeHours'] += $taskHours;
                elseif ($out['completeHours'] <= $dailyWorkingHours) {
                    $sub = $dailyWorkingHours - $out['completeHours'];
                    $out['completeHours'] = $dailyWorkingHours;
                    $out['overtime'] += $taskHours - $sub;
                } else {
                    $out['overtime'] += $taskHours;
                }

                $task['hours'] = round($taskHours, 2, PHP_ROUND_HALF_UP);
            }
            array_push($newTasks, $task);
        }

        if ($out["completeHours"] < $dailyWorkingHours) {
            $out["overtime"] = $out["completeHours"] - $dailyWorkingHours;
        }

        $out['tasks'] = $newTasks;
        $out["overtime"] = round($out['overtime'], 2, PHP_ROUND_HALF_UP);
        $out["completeHours"] = round($out['completeHours'], 2, PHP_ROUND_HALF_UP);

        return $out;
    }

    private function getDifferenceInMilliseconds($start, $end)
    {
        $startTime = new DateTime($start);
        $endTime = new DateTime($end);

        $interval = $startTime->diff($endTime);

        $totalMiliseconds = 0;
        $totalMiliseconds += $interval->m * 2630000000;
        $totalMiliseconds += $interval->d * 86400000;
        $totalMiliseconds += $interval->h * 3600000;
        $totalMiliseconds += $interval->i * 60000;
        $totalMiliseconds += $interval->s * 1000;

        return $totalMiliseconds;
    }

    /**
     * Changes names of weeks and months from english to german
     *
     * @param $weekday
     * @return string
     */
    private function changeWeekday($weekday)
    {
        preg_match('/^(\w+)(,\s\d+\s)(\w+)(\s\d+.*?$)/', $weekday, $match);
        $out = $weekday;
        if ($match && sizeof($match) > 3) {
            switch ($match[1]) {
                case "Monday":
                    $match[1] = "Montag";
                    break;
                case "Tuesday":
                    $match[1] = "Dienstag";
                    break;
                case "Wednesday":
                    $match[1] = "Mittwoch";
                    break;
                case "Thursday":
                    $match[1] = "Donnerstag";
                    break;
                case "Friday":
                    $match[1] = "Freitag";
                    break;
                case "Saturday":
                    $match[1] = "Samstag";
                    break;
                case "Sunday":
                    $match[1] = "Sonntag";
                    break;
            }

            switch ($match[3]) {
                case "January":
                    $match[3] = "Januar";
                    break;
                case "February":
                    $match[3] = "Februar";
                    break;
                case "March":
                    $match[3] = "März";
                    break;
                case "April":
                    $match[3] = "April";
                    break;
                case "May":
                    $match[3] = "Mai";
                    break;
                case "June":
                    $match[3] = "Juni";
                    break;
                case "July":
                    $match[3] = "Juli";
                    break;
                case "August":
                    $match[3] = "August";
                    break;
                case "September":
                    $match[3] = "September";
                    break;
                case "October":
                    $match[3] = "Oktober";
                    break;
                case "November":
                    $match[3] = "November";
                    break;
                case "December":
                    $match[3] = "Dezember";
                    break;
            }

            $out = "";
            for ($i = 1; $i < sizeof($match); $i++) {
                $out .= $match[$i];
            }
        }
        return $out;
    }
}