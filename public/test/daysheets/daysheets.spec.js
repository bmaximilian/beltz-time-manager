import Daysheet from '../../src/modules/daysheets/js/classes/Daysheet.class'

/**
 * Test JavaScript file for main application container
 *
 * @author <m.beck@beltz.de>
 */
describe("Daysheet", function () {

    it("calculates", function () {

        let calculation = Daysheet.calculate("1970-01-01 08:00:00", "1970-01-01 09:02:00");

        expect(calculation).toEqual("01:02");
    });

    it("resolves today", function () {
        let today = Daysheet._isToday("1970-01-01 08:00:00");

        expect(today).toBe(false);
    });

    it("calculates whole time", function () {
        const tasks = [
            {
                "costcenterId": 1,
                "startdate": "2017-04-11 14:03:36",
                "enddate": "2017-04-11 14:03:54"
            },
            {
                "costcenterId": -1,
                "startdate": "2017-04-11 14:03:54",
                "enddate": "2017-04-11 14:19:45"
            },
            {
                "costcenterId": 1,
                "startdate": "2017-04-11 14:19:45",
                "enddate": "2017-04-11 21:00:00"
            }
        ];

        let wholeTime = Daysheet.calculateTasks(tasks);
        let min = Math.round(Daysheet._getDifferenceInMinutes(tasks[0].startdate, tasks[0].enddate) + Daysheet._getDifferenceInMinutes(tasks[2].startdate, tasks[2].enddate));
        let mill = Daysheet._getDifferenceInMilliseconds(tasks[2].startdate, tasks[2].enddate);

        expect(wholeTime).toEqual("06:41");
        expect(min).toEqual(401);
        expect(mill).toEqual(24015000);
    });

    it("can parse a date object to SQL timestamp", function () {
        const ts = "1970-01-01 08:00:00";
        let date = new Date(ts);

        expect(Daysheet._dateToTimestamp(date)).toEqual(ts);
    });

    it("merges wrong startdates", function () {
        "use strict";
        const tasks = [
            {
                "id": 1,
                "startdate": "2017-04-10 11:00:00",
                "enddate": "2017-04-10 11:30:00"
            },
            {
                "id": 2,
                "startdate": "2017-04-10 11:30:00",
                "enddate": "2017-04-10 12:00:00"
            },
            {
                "id": 3,
                "startdate": "2017-04-10 11:45:00",
                "enddate": "2017-04-10 13:00:00"
            }
        ];

        let task = {
            "id": 3,
            "startdate": "2017-04-10 11:45:00",
            "enddate": "2017-04-10 13:00:00"
        };

        task = Daysheet.checkStartdate(task, "2017-04-10 11:00:00", tasks);

        expect(task.startdate).toEqual("2017-04-10 12:00:00");
    });

    it("merges wrong startdate of sheet", function () {
        "use strict";
        const tasks = [
            {
                "id": 1,
                "startdate": "2017-04-10 11:00:00",
                "enddate": "2017-04-10 11:30:00"
            },
            {
                "id": 2,
                "startdate": "2017-04-10 11:30:00",
                "enddate": "2017-04-10 12:00:00"
            },
            {
                "id": 3,
                "startdate": "2017-04-10 12:00:00",
                "enddate": "2017-04-10 13:00:00"
            }
        ];

        let task = {
            "id": 1,
            "startdate": "2017-04-10 10:00:00",
            "enddate": "2017-04-10 11:30:00"
        };

        task = Daysheet.checkStartdate(task, "2017-04-10 11:00:00", tasks);

        expect(task.startdate).toEqual("2017-04-10 11:00:00");
    });

    it("does nothing by right startdates", function () {
        "use strict";
        const tasks = [
            {
                "id": 1,
                "startdate": "2017-04-10 11:00:00",
                "enddate": "2017-04-10 11:30:00"
            },
            {
                "id": 2,
                "startdate": "2017-04-10 11:30:00",
                "enddate": "2017-04-10 12:00:00"
            },
            {
                "id": 3,
                "startdate": "2017-04-10 12:00:00",
                "enddate": "2017-04-10 13:00:00"
            }
        ];

        let task = {
            "id": 2,
            "startdate": "2017-04-10 11:30:00",
            "enddate": "2017-04-10 12:00:00"
        };

        task = Daysheet.checkStartdate(task, "2017-04-10 11:00:00", tasks);

        expect(task.startdate).toEqual("2017-04-10 11:30:00");
    });

    it("merges wrong enddates", function () {
        "use strict";
        const tasks = [
            {
                "id": 1,
                "startdate": "2017-04-10 11:00:00",
                "enddate": "2017-04-10 11:30:00"
            },
            {
                "id": 2,
                "startdate": "2017-04-10 11:30:00",
                "enddate": "2017-04-10 12:00:00"
            },
            {
                "id": 3,
                "startdate": "2017-04-10 11:45:00",
                "enddate": "2017-04-10 13:00:00"
            }
        ];

        let task = {
            "id": 2,
            "startdate": "2017-04-10 11:30:00",
            "enddate": "2017-04-10 12:00:00"
        };

        task = Daysheet.checkEnddate(task, "2017-04-10 11:00:00", tasks);

        expect(task.enddate).toEqual("2017-04-10 11:45:00");
    });

    it("does nothing by right enddates", function () {
        "use strict";
        const tasks = [
            {
                "id": 1,
                "startdate": "2017-04-10 11:00:00",
                "enddate": "2017-04-10 11:30:00"
            },
            {
                "id": 2,
                "startdate": "2017-04-10 11:30:00",
                "enddate": "2017-04-10 12:00:00"
            },
            {
                "id": 3,
                "startdate": "2017-04-10 12:00:00",
                "enddate": "2017-04-10 13:00:00"
            }
        ];

        let task = {
            "id": 2,
            "startdate": "2017-04-10 11:30:00",
            "enddate": "2017-04-10 12:00:00"
        };

        task = Daysheet.checkEnddate(task, "2017-04-10 11:00:00", tasks);

        expect(task.enddate).toEqual("2017-04-10 12:00:00");
    });
});