<?php

/**
 * PHP entry point
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */

// Set timezone to germany
date_default_timezone_set('Europe/Berlin');

// Require composer dependencys
require 'vendor/autoload.php';

// Require internal dependencys for the app
require 'app/base/Utils.php';
Utils::requireApp(__DIR__);

// Specific settings for PHP-DI container
$settings = Utils::getIni("slim.ini");
$settings['php-di'] = [
    'use_autowiring' => true,
    'use_annotations' => false,
];

// Create the Slim application
$app = new \Slim\App(['settings' => $settings]);

// Resolve routes
$router = new Router($app);
$router->route();

// Start the app
$app->run();