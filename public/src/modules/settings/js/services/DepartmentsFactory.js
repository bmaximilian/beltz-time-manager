import Helper from '../../../app/js/classes/Helper.class'

/**
 * Factory to store the department data
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default function ($mdToast, $http, BaseUrl) {
    var departments = [];

    $http.get(BaseUrl.baseAjax + "/api/departments").then(function (response) {
        if (response && response.data && response.data.length > 0) {
            departments = response.data;
        }
    }, Helper.connectionError($mdToast, "Abteilungen konnten nicht erfasst werden"));


    /**
     * Returns all departments
     * or the department selected by id
     *
     * @param id
     * @param callback
     */
    function getDepartments(id, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/departments";
        if (id)
            apiRoute += "/" + id;

        $http.get(apiRoute).then(function (response) {
            if (response && response.data) {
                departments = response.data;
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Abteilungen konnten nicht erfasst werden"));
    }


    /**
     * Inserts a department
     *
     * @param data
     * @param callback
     */
    function insertDepartment(data, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/departments";

        $http.post(apiRoute, data).then(function (response) {
            if (response && response.data) {
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Abteilung konnt nicht hinzugefügt werden"));
    }


    /**
     * Updates a department specified by id
     *
     * @param id
     * @param data
     * @param callback
     */
    function updateDepartment(id, data, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/departments/" + id;

        if (id)
            $http.put(apiRoute, data).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Abteilung konnte nicht geändert werden"));
    }


    /**
     * Deletes a department specified by id
     *
     * @param id
     * @param callback
     */
    function deleteDepartment(id, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/departments/" + id;

        if (id)
            $http.delete(apiRoute).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Abteilung konnte nicht gelöscht werden"));
    }

    return {
        departments: function () {
            return departments;
        },

        getDepartments: getDepartments,

        insertDepartment: insertDepartment,

        updateDepartment: updateDepartment,

        deleteDepartment: deleteDepartment
    }
}