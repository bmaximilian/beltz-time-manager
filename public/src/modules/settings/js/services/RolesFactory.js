import Helper from '../../../app/js/classes/Helper.class'

/**
 * Factory to store the roles data
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default function ($mdToast, $http, BaseUrl) {
    var roles = [];

    $http.get(BaseUrl.baseAjax + "/api/roles").then(function (response) {
        if (response && response.data && response.data.length > 0) {
            roles = response.data;
        }
    }, Helper.connectionError($mdToast, "Rollen konnten nicht erfasst werden"));


    /**
     * Returns all roles or a role specified by id
     *
     * @param id
     * @param callback
     */
    function getRoles(id, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/roles";
        if (id)
            apiRoute += "/" + id;

        $http.get(apiRoute).then(function (response) {
            if (response && response.data) {
                roles = response.data;
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Rollen konnten nicht erfasst werden"));
    }


    /**
     * Inserts a role
     *
     * @param data
     * @param callback
     */
    function insertRoles(data, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/roles";

        $http.post(apiRoute, data).then(function (response) {
            if (response && response.data) {
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Rolle konnte nicht hinzugefügt werden"));
    }


    /**
     * Updates a role specified by id
     *
     * @param id
     * @param data
     * @param callback
     */
    function updateRoles(id, data, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/roles/" + id;

        if (id)
            $http.put(apiRoute, data).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Rolle konnte nicht geändert werden"));
    }


    /**
     * Deletes a role specified by id
     *
     * @param id
     * @param callback
     */
    function deleteRoles(id, callback) {
        var apiRoute = BaseUrl.baseAjax + "/api/roles/" + id;

        if (id)
            $http.delete(apiRoute).then(function (response) {
                if (response && response.data) {
                    callback(response.data);
                }
            }, Helper.connectionError($mdToast, "Rolle konnte nicht gelöscht werden"));
    }


    /**
     * Returns all powers or the power specified by id
     *
     * @param id
     * @param callback
     */
    function getPowers(id, callback) {
        "use strict";
        var apiRoute = BaseUrl.baseAjax + "/api/powers";
        if (id)
            apiRoute += "/" + id;

        $http.get(apiRoute).then(function (response) {
            if (response && response.data) {
                for (var power of response.data) {
                    if (power.value == undefined || power.value == null)
                        power.value = 0;

                }
                callback(response.data);
            }
        }, Helper.connectionError($mdToast, "Die Berechtigungen konnten nicht geladen werden"));
    }

    return {
        roles: function () {
            return roles;
        },

        getRoles: getRoles,

        getPowers: getPowers,

        insertRoles: insertRoles,

        updateRoles: updateRoles,

        deleteRoles: deleteRoles
    }
}