/**
 * Settings application context which loads all required modules
 * and configurations for the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */

import Users from './services/Users'
import DepartmentsFactory from './services/DepartmentsFactory'
import RolesFactory from './services/RolesFactory'
import SalutationsFactory from './services/SalutationsFactory'
import PositionsFactory from './services/PositionsFactory'
import users from './directives/usersDirective'
import roles from './directives/rolesDirective'
import departments from './directives/departmentsDirective'
import positions from './directives/positionsDirective'

var appName = "main.settings";

angular.module("main.settings", [])
    .provider("Users", Users)
    .factory("DepartmentsFactory", DepartmentsFactory)
    .factory("RolesFactory", RolesFactory)
    .factory("SalutationsFactory", SalutationsFactory)
    .factory("PositionsFactory", PositionsFactory)
    .directive("users", users)
    .directive("roles", roles)
    .directive("departments", departments)
    .directive("positions", positions);

export default appName