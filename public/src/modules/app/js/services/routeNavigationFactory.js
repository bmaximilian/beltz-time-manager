/**
 * factory to read out the configured routes object
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($route, $location) {
    var routes = [];

    angular.forEach($route.routes, function (route, path) {
        if (route.name) {
            routes.push({
                path: path,
                name: route.name,
                icon: (route.icon ? route.icon : ''),
                authenticationRequired: (route.authenticationRequired ? route.authenticationRequired : false),
                requiredPower: (route.requiredPower ? route.requiredPower : 0)
            });
        }
    });

    return {
        routes: routes,
        activeRoute: function (route) {
            return route.path == $location.path()
        }
    };
}