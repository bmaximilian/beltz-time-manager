import WelcomeController from '../controller/WelcomeController'

/**
 * directive to welcome the active user
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function (BaseUrl) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: BaseUrl.viewPath + "daysheets/views/welcome.html",
        controller: WelcomeController
    }
};