<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for positions database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class PositionsController extends RestController
{

    private $selectQuery = "
SELECT 
  positions.pk AS id,
  positions.department_pk AS departmentId,
  positions.designation AS name,
  positions.description,
  departments.designation AS departmentName,
  departments.description AS departmentDescription
FROM
  positions
JOIN departments ON positions.department_pk = departments.pk
    ";


    /**
     * Resolve request body for positions
     *
     * @param Request $request
     * @return array
     */
    private function getRequestBody(Request $request) {
        return array(
            "department_pk" => @$request->getParsedBody()['departmentId'],
            "designation" => @$request->getParsedBody()['name'],
            "description" => @$request->getParsedBody()['description']
        );
    }


    /**
     * GET function for positions
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $out = [];

        if ($id)
            $positions = $this->loader->query($this->selectQuery . "WHERE positions.pk = $id LIMIT 1;");
        else
            $positions = $this->loader->query($this->selectQuery);

        return $response->getBody()->write(json_encode($positions));
    }


    /**
     * POST function for positions
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        $position = $this->loader->insert("positions", $this->getRequestBody($request));

        return $response->getBody()->write(json_encode($position));
    }


    /**
     * PUT function for positions
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function put(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        $position = $this->loader->update("positions", $this->getRequestBody($request), [
            "pk" => $id
        ]);

        return $response->getBody()->write(json_encode($position));
    }


    /**
     * DELETE function for positions
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function delete(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        $position = $this->loader->delete("positions", [
            "pk" => $id
        ]);

        return $response->getBody()->write(json_encode($position));
    }

}