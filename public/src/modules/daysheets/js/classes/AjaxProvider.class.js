import Helper from '../../../app/js/classes/Helper.class'

/**
 * Class to store basic REST API Ajax Functions for provider
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 */
export default class AjaxProvider {

    /**
     * Constructor of AjaxProvider
     *
     * @param httpProvider
     * @param toastProvider
     */
    constructor(httpProvider, toastProvider = null) {
        this.$http = httpProvider;
        this.$mdToast = toastProvider;
    }

    /**
     * Returns a GET HTTP request
     *
     * @param apiRoute
     * @returns {Function}
     */
    fnGet(apiRoute) {
        var self = this;
        return function (callback, id = null) {
            if (apiRoute) {
                if (id)
                    apiRoute += "/" + id;

                self.$http.get(apiRoute).then(function (response) {
                    if (response && response.data) {
                        callback(response.data);
                    }
                }, Helper.connectionError(self.$mdToast, "Tageszettel konnten nicht erfasst werden"));
            }
        }
    }

    /**
     * Returns a POST HTTP request
     *
     * @param apiRoute
     * @returns {Function}
     */
    fnInsert(apiRoute) {
        var self = this;
        return function (data, callback, id = null) {
            if (apiRoute) {
                if (id)
                    apiRoute += "/" + id;

                self.$http.post(apiRoute, data).then(function (response) {
                    if (response && response.data) {
                        callback(response.data);
                    }
                }, Helper.connectionError(self.$mdToast, "Tageszettel konnten nicht hinzugefügt werden"));
            }
        }
    }

    /**
     * Returns a DELETE HTTP request
     *
     * @param apiRoute
     * @returns {Function}
     */
    fnDelete(apiRoute) {
        var self = this;
        return function (id, callback) {
            if (apiRoute && id) {
                apiRoute += "/" + id;

                self.$http.delete(apiRoute).then(function (response) {
                    if (response && response.data) {
                        callback(response.data);
                    }
                }, Helper.connectionError(self.$mdToast, "Tageszettel konnten nicht hinzugefügt werden"));
            }
        }
    }

    /**
     * Returns an UPDATE HTTP request
     *
     * @param apiRoute
     * @returns {Function}
     */
    fnUpdate(apiRoute) {
        var self = this;
        return function (id, data, callback) {
            if (apiRoute && id) {
                apiRoute += "/" + id;

                self.$http.put(apiRoute, data).then(function (response) {
                    if (response && response.data) {
                        callback(response.data);
                    }
                }, Helper.connectionError(self.$mdToast, "Tageszettel konnte nicht geändert werden"));
            }
        }
    }
}