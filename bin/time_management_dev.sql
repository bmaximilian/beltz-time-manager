-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.1.13-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Datenbank Struktur für time_management
CREATE DATABASE IF NOT EXISTS `time_management` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `time_management`;


-- Exportiere Struktur von Tabelle time_management.costcenters
CREATE TABLE IF NOT EXISTS `costcenters` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle costcenters',
  `designation` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung der Kostenstelle',
  `identifier` int(11) NOT NULL COMMENT 'Identifiketionsnummer der Kostenstelle',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Erstelldatum der Kostenstelle',
  `deletedate` timestamp NULL DEFAULT NULL COMMENT 'Loeschdatum der Kostenstelle',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`),
  UNIQUE KEY `identifier` (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Kostenstellen des Management Informationssystems';

-- Exportiere Daten aus Tabelle time_management.costcenters: ~4 rows (ungefähr)
/*!40000 ALTER TABLE `costcenters` DISABLE KEYS */;
INSERT INTO `costcenters` (`pk`, `designation`, `identifier`, `createdate`, `deletedate`) VALUES
	(-2, 'Mittagspause', -2, '2017-03-22 10:32:20', NULL),
	(-1, 'Frühstückspause', -1, '2017-03-22 10:32:06', NULL),
	(1, 'Elektronic Publishing', 181, '2017-02-07 09:39:32', NULL),
	(2, 'allgemeine Satzarbeiten', 122, '2017-02-07 09:39:53', NULL);
/*!40000 ALTER TABLE `costcenters` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.costcenterxtimetype
CREATE TABLE IF NOT EXISTS `costcenterxtimetype` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Linktabelle costcenterXtimetype',
  `timetype_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, der die Zeitart referenziert',
  `costcenter_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, der die Kostenstelle referenziert',
  PRIMARY KEY (`pk`),
  KEY `FK_timetypes_X` (`timetype_pk`),
  KEY `FK_costcenters_X` (`costcenter_pk`),
  CONSTRAINT `FK_costcenters_X` FOREIGN KEY (`costcenter_pk`) REFERENCES `costcenters` (`pk`),
  CONSTRAINT `FK_timetypes_X` FOREIGN KEY (`timetype_pk`) REFERENCES `timetypes` (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Linktabelle die den Kostenstellen verschiedene Zeitarten zuordnet';

-- Exportiere Daten aus Tabelle time_management.costcenterxtimetype: ~5 rows (ungefähr)
/*!40000 ALTER TABLE `costcenterxtimetype` DISABLE KEYS */;
INSERT INTO `costcenterxtimetype` (`pk`, `timetype_pk`, `costcenter_pk`) VALUES
	(1, 3, 2),
	(2, 3, 1),
	(3, 2, 2),
	(4, 1, 2),
	(5, 1, 1);
/*!40000 ALTER TABLE `costcenterxtimetype` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle customers',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung des Kunden',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung des Kunden',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Erstelldatum des Benutzers',
  `deletedate` timestamp NULL DEFAULT NULL COMMENT 'Loeschdatum des Benutzers',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin
  COMMENT = 'Tabelle, die die Kunden beinhaltet';

-- Exportiere Daten aus Tabelle time_management.customers: ~2 rows (ungefähr)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`pk`, `designation`, `description`, `createdate`, `deletedate`) VALUES
  (-1, 'INTERN', NULL, '2017-03-22 09:13:05', NULL),
  (1, 'WILEY WEIN', NULL, '2017-02-07 09:41:13', NULL),
  (3, 'TEST', NULL, '2017-04-10 01:03:00', NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.dailytasks
CREATE TABLE IF NOT EXISTS `dailytasks` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle dailytasks',
  `job_pk` bigint(20) DEFAULT NULL COMMENT 'Fremdschluessel, welcher den bearbeiteten Auftrag referenziert',
  `timetype_pk` bigint(20) DEFAULT NULL COMMENT 'Fremdschluessel, welcher die Zeitart der Taetigkeit referenziert',
  `cost_center_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, welcher die Kostenstelle der Taetigkeit referenziert',
  `material_pk` bigint(20) DEFAULT NULL COMMENT 'Fremdschluessel, welcher das genutzte Material referenziert',
  `partjob_pk` bigint(20) DEFAULT NULL COMMENT 'Fremdschlüssel, welcher den zugeordneten Teilauftrag referenziert',
  `startdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Anfangszeit der Taetigkeit',
  `enddate` timestamp NULL DEFAULT NULL COMMENT 'Abschlusszeit der Taetigkeit',
  `pages` int(11) DEFAULT NULL COMMENT 'Anzahl der Seiten',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung der Taetigkeit',
  PRIMARY KEY (`pk`),
  KEY `FK_dailytask_job` (`job_pk`),
  KEY `FK_dailytask_timetype` (`timetype_pk`),
  KEY `FK_dailytask_costcenter` (`cost_center_pk`),
  KEY `FK_dailytask_material` (`material_pk`),
  KEY `FK_dailytask_partjob` (`partjob_pk`),
  CONSTRAINT `FK_dailytask_costcenter` FOREIGN KEY (`cost_center_pk`) REFERENCES `costcenters` (`pk`),
  CONSTRAINT `FK_dailytask_job` FOREIGN KEY (`job_pk`) REFERENCES `jobs` (`pk`),
  CONSTRAINT `FK_dailytask_material` FOREIGN KEY (`material_pk`) REFERENCES `materials` (`pk`),
  CONSTRAINT `FK_dailytask_partjob` FOREIGN KEY (`partjob_pk`) REFERENCES `partjobs` (`pk`),
  CONSTRAINT `FK_dailytask_timetype` FOREIGN KEY (`timetype_pk`) REFERENCES `timetypes` (`pk`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 56
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin
  COMMENT = 'Taegliche Aufgaben des Benutzers';

-- Exportiere Daten aus Tabelle time_management.dailytasks: ~42 rows (ungefähr)
/*!40000 ALTER TABLE `dailytasks` DISABLE KEYS */;
INSERT INTO `dailytasks` (`pk`, `job_pk`, `timetype_pk`, `cost_center_pk`, `material_pk`, `partjob_pk`, `startdate`, `enddate`, `pages`, `description`) VALUES
	(3, 3, 1, 1, NULL, NULL, '2017-03-21 08:45:00', '2017-03-21 09:00:00', NULL, 'Einrichtung Büro'),
	(4, 4, 1, 1, NULL, NULL, '2017-03-21 10:00:00', '2017-03-21 12:15:00', NULL, 'Mail Funktion'),
	(5, 3, 1, -1, NULL, NULL, '2017-03-21 09:00:00', '2017-03-21 09:15:00', NULL, NULL),
	(6, 3, 1, 1, NULL, NULL, '2017-03-21 09:15:00', '2017-03-21 10:00:00', NULL, 'Einrichtung Büro'),
	(7, 3, 1, -2, NULL, NULL, '2017-03-21 12:15:00', '2017-03-21 12:45:00', NULL, NULL),
	(8, 4, 1, 1, NULL, NULL, '2017-03-21 12:45:00', '2017-03-21 19:00:00', NULL, 'Backend REST API'),
	(15, 4, 1, 1, NULL, NULL, '2017-03-24 13:36:46', NULL, NULL, 'Test'),
	(21, 4, NULL, -1, NULL, NULL, '2017-03-24 13:45:26', NULL, NULL, NULL),
	(22, 4, 1, 1, NULL, NULL, '2017-03-24 13:57:36', NULL, NULL, 'Bla'),
	(23, 4, 1, 1, NULL, NULL, '2017-03-24 14:22:07', NULL, NULL, 'Projektangabe bei Pausen weglassen.'),
	(24, NULL, NULL, -2, NULL, NULL, '2017-03-24 14:35:33', NULL, NULL, NULL),
	(25, 3, 1, 1, NULL, NULL, '2017-04-08 23:14:05', NULL, NULL, 'Backend mit Controllerstruktur implementiert'),
	(26, 4, 1, 1, NULL, NULL, '2017-04-09 20:13:36', NULL, NULL, 'Fehlerbereinigung'),
	(27, 4, NULL, -1, NULL, NULL, '2017-04-09 20:17:39', NULL, NULL, 'Test'),
	(28, 3, 1, 1, NULL, NULL, '2017-04-09 20:40:06', NULL, NULL, 'TEst 2'),
	(29, 4, 3, 1, NULL, NULL, '2017-04-09 21:18:48', '2017-04-09 21:31:44', NULL, 'Erster Test des Tageszettels'),
  (30, 4, 1, 1, NULL, NULL, '2017-04-09 21:31:44', NULL, NULL, 'Zweiter Test'),
  (31, 4, 1, 1, NULL, NULL, '2017-04-10 11:24:31', '2017-04-10 11:41:59', NULL, 'Generalisierung der Ajax Route, Überprüfung der Formatierung, BugFix StartDay()'),
  (32, 4, 3, 1, NULL, NULL, '2017-04-10 11:41:59', '2017-04-10 12:15:00', NULL, 'Test gefüllte Nullen'),
  (33, 4, NULL, -2, NULL, NULL, '2017-04-10 12:15:00', '2017-04-10 12:45:00', NULL, NULL),
  (34, 4, 1, 1, NULL, NULL, '2017-04-10 12:45:00', '2017-04-10 19:34:05', NULL, 'Test der Zeitvalidierung'),
  (35, 5, 1, 1, NULL, 2, '2017-04-11 08:50:00', '2017-04-11 09:35:37', NULL, 'Vorbereitung der Entwicklungsumgebung, Einstellungen am Webserver, Besprechen der Änderungen'),
  (36, NULL, NULL, -1, NULL, NULL, '2017-04-11 09:43:22', '2017-04-11 09:43:22', NULL, NULL),
  (37, 5, 1, 1, NULL, 2, '2017-04-11 09:43:22', '2017-04-11 09:53:19', NULL, 'Erstellen einer PDF der HTML Ansicht'),
  (38, 5, 1, 1, NULL, 2, '2017-04-11 09:53:19', '2017-04-11 10:54:51', NULL, 'Erstellen einer Versionsverwaltung unter GIT'),
  (39, 4, 1, 1, NULL, NULL, '2017-04-11 10:54:51', '2017-04-11 12:17:42', NULL, 'Überstundenkonto einpflegen'),
  (40, NULL, NULL, -2, NULL, NULL, '2017-04-11 12:17:42', '2017-04-11 13:08:17', NULL, NULL),
  (41, 4, 1, 1, NULL, NULL, '2017-04-11 13:08:17', '2017-04-11 15:11:02', NULL, 'Custom Jobs hinzufügen, Rollenverwaltung'),
  (42, 1, 3, 1, NULL, 1, '2017-04-11 14:03:36', '2017-04-11 14:03:54', NULL, 'Test'),
  (43, NULL, NULL, -1, NULL, NULL, '2017-04-11 14:03:54', '2017-04-11 14:19:45', NULL, NULL),
  (44, 1, 3, 1, NULL, 1, '2017-04-11 14:19:45', '2017-04-11 21:00:00', NULL, 'Test 2'),
  (45, 5, 1, 1, NULL, 3, '2017-04-11 15:11:02', '2017-04-11 20:22:59', NULL, 'Texte überarbeiten'),
  (46, 4, 1, 1, NULL, NULL, '2017-04-12 09:14:11', '2017-04-12 11:45:46', NULL, 'Anzeigen alter Tageszettel, Testfallerstellung, Rollenmanagement'),
  (47, 1, 1, 1, NULL, NULL, '2017-04-12 11:38:31', '2017-04-12 17:21:53', NULL, 'test'),
  (48, NULL, NULL, -2, NULL, NULL, '2017-04-12 11:45:46', '2017-04-12 13:16:55', NULL, NULL),
  (49, 4, 1, 1, NULL, NULL, '2017-04-12 13:16:55', '2017-04-12 19:46:05', NULL, 'Anzeigen alter Tageszettel, Testfallerstellung, Rollenmanagement'),
  (50, 1, 1, 1, NULL, NULL, '2017-04-12 17:15:57', '2017-04-12 17:16:13', NULL, 'Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt Testtestt'),
  (51, 5, 1, 1, NULL, NULL, '2017-04-13 08:45:09', '2017-04-13 09:17:00', NULL, 'Besprechung Gruppenbild Dateigröße'),
  (52, NULL, NULL, -1, NULL, NULL, '2017-04-13 09:17:00', '2017-04-13 09:32:00', NULL, NULL),
  (53, 4, 1, 1, NULL, NULL, '2017-04-13 09:32:00', '2017-04-13 11:46:06', NULL, 'PDF Dateiendung in Mail, Tageszettel von anderen Benutzern'),
  (54, NULL, NULL, -2, NULL, NULL, '2017-04-13 11:46:06', '2017-04-13 12:33:00', NULL, NULL),
  (55, 4, 1, 1, NULL, NULL, '2017-04-13 12:33:27', NULL, NULL, 'Testfallerstellung, Dokumentation');
/*!40000 ALTER TABLE `dailytasks` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.daysheets
CREATE TABLE IF NOT EXISTS `daysheets` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschuessel der Tabelle daysheets',
  `user_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, der den Arbeitszeitnachweis einem Benutzer zuordnet',
  `startdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Anfangszeit des Arbeitszeitnachweises',
  `enddate` timestamp NULL DEFAULT NULL COMMENT 'Abschlusszeit des Arbeitszeitnachweises',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung des Arbeitszeitnachweises',
  PRIMARY KEY (`pk`),
  KEY `FK_daysheets_user` (`user_pk`),
  CONSTRAINT `FK_daysheets_user` FOREIGN KEY (`user_pk`) REFERENCES `users` (`pk`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 20
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin
  COMMENT = 'Tabelle, die die Tageszettel beinhaltet';

-- Exportiere Daten aus Tabelle time_management.daysheets: ~12 rows (ungefähr)
/*!40000 ALTER TABLE `daysheets` DISABLE KEYS */;
INSERT INTO `daysheets` (`pk`, `user_pk`, `startdate`, `enddate`, `description`) VALUES
	(1, 1, '2017-03-21 08:45:00', '2017-03-21 19:00:00', NULL),
	(9, 1, '2017-03-23 17:56:11', '2017-03-24 14:00:56', NULL),
	(10, 1, '2017-03-24 14:20:57', '2017-03-24 15:27:48', NULL),
	(11, 1, '2017-04-08 23:13:12', '2017-04-09 20:55:47', NULL),
  (12, 1, '2017-04-09 21:18:18', '2017-04-09 23:08:19', NULL),
  (13, 1, '2017-04-10 11:22:49', '2017-04-10 19:34:05', NULL),
  (14, 1, '2017-04-11 08:50:00', '2017-04-11 20:22:59', NULL),
  (15, 2, '2017-04-11 14:03:09', '2017-04-12 21:00:00', NULL),
  (16, 1, '2017-04-12 09:13:15', '2017-04-12 19:46:05', NULL),
  (17, 2, '2017-04-12 10:34:09', '2017-04-12 17:21:53', NULL),
  (18, 1, '2017-04-13 08:45:09', NULL, NULL),
  (19, 2, '2017-04-13 11:44:14', NULL, NULL);
/*!40000 ALTER TABLE `daysheets` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.daysheetsxdailytasks
CREATE TABLE IF NOT EXISTS `daysheetsxdailytasks` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Linktabelle daysheetsXdailytasks',
  `daysheet_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, der den Tageszettel referenziert',
  `dailytask_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, der die Aufgabe referenziert',
  PRIMARY KEY (`pk`),
  KEY `FK_daysheet_X` (`daysheet_pk`),
  KEY `FK_dailytask_X` (`dailytask_pk`),
  CONSTRAINT `FK_dailytask_X` FOREIGN KEY (`dailytask_pk`) REFERENCES `dailytasks` (`pk`),
  CONSTRAINT `FK_daysheet_X` FOREIGN KEY (`daysheet_pk`) REFERENCES `daysheets` (`pk`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 46
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin
  COMMENT = 'Linktabelle, die den Arbeitszeitnachweisen einzelne Tagesaufgaben zuordnet';

-- Exportiere Daten aus Tabelle time_management.daysheetsxdailytasks: ~42 rows (ungefähr)
/*!40000 ALTER TABLE `daysheetsxdailytasks` DISABLE KEYS */;
INSERT INTO `daysheetsxdailytasks` (`pk`, `daysheet_pk`, `dailytask_pk`) VALUES
	(2, 1, 3),
	(3, 1, 4),
	(4, 1, 5),
	(5, 1, 6),
	(6, 1, 7),
	(8, 1, 8),
	(10, 9, 15),
	(11, 9, 21),
	(12, 9, 22),
	(13, 10, 23),
	(14, 10, 24),
	(15, 11, 25),
	(16, 11, 26),
	(17, 11, 27),
	(18, 11, 28),
	(19, 12, 29),
  (20, 12, 30),
  (21, 13, 31),
  (22, 13, 32),
  (23, 13, 33),
  (24, 13, 34),
  (25, 14, 35),
  (26, 14, 36),
  (27, 14, 37),
  (28, 14, 38),
  (29, 14, 39),
  (30, 14, 40),
  (31, 14, 41),
  (32, 15, 42),
  (33, 15, 43),
  (34, 15, 44),
  (35, 14, 45),
  (36, 16, 46),
  (37, 17, 47),
  (38, 16, 48),
  (39, 16, 49),
  (40, 17, 50),
  (41, 18, 51),
  (42, 18, 52),
  (43, 18, 53),
  (44, 18, 54),
  (45, 18, 55);
/*!40000 ALTER TABLE `daysheetsxdailytasks` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.departments
CREATE TABLE IF NOT EXISTS `departments` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle departments',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung der Abteilung',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung der Abteilung',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tabelle, die die Abteilungen auffuehrt, welche in der Anwendung enthalten sind';

-- Exportiere Daten aus Tabelle time_management.departments: ~2 rows (ungefähr)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`pk`, `designation`, `description`) VALUES
	(1, 'Satz', 'Abteilung für Schriftsatz der Beltz grafischen Betriebe'),
	(2, 'IT', 'IT-Abteilung der Beltz Grafischen Betriebe');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle jobs',
  `customer_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, der dem Auftrag einen Kunden zuordnet',
  `jobnr` int(11) NOT NULL COMMENT 'Identifikationsnummer des Auftrags',
  `designation` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung bzw. Titel des Auftrags',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Erstelldatum des Auftrags',
  `deletedate` timestamp NULL DEFAULT NULL COMMENT 'Loeschdatum des Auftrags',
  `recieptdate` date NOT NULL COMMENT 'Eingangsdatum des Auftrags',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung zum Auftrag',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `jobnr` (`jobnr`),
  KEY `FK_job_customer` (`customer_pk`),
  CONSTRAINT `FK_job_customer` FOREIGN KEY (`customer_pk`) REFERENCES `customers` (`pk`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin
  COMMENT = 'Tabelle, die die Auftraege aus dem Management Informationssystem beinhaltet';

-- Exportiere Daten aus Tabelle time_management.jobs: ~5 rows (ungefähr)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`pk`, `customer_pk`, `jobnr`, `designation`, `createdate`, `deletedate`, `recieptdate`, `description`) VALUES
	(1, 1, 2164187, 'Neues Dummie Layout', '2017-02-07 09:42:13', NULL, '2016-10-13', NULL),
  (3, -1, 0, 'Vorbereitung', '2017-03-22 09:12:53', NULL, '0000-00-00', NULL),
  (4, -1, -2, 'Digitalisierter Arbeitszeitnachweis', '2017-03-22 09:13:47', NULL, '2017-01-15', NULL),
  (5, -1, -1, 'Homepage', '2017-04-11 08:54:40', NULL, '2016-11-01', NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.materials
CREATE TABLE IF NOT EXISTS `materials` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle materials',
  `identifier` int(11) NOT NULL COMMENT 'Identifikationsnummer des Materials',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung des Materials',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Erstelldatum des Materials',
  `deletedate` timestamp NULL DEFAULT NULL COMMENT 'Loeschdatum des Materials',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`),
  UNIQUE KEY `identifier` (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tabelle, die die Materialien beinhaltet';

-- Exportiere Daten aus Tabelle time_management.materials: ~0 rows (ungefähr)
/*!40000 ALTER TABLE `materials` DISABLE KEYS */;
INSERT INTO `materials` (`pk`, `identifier`, `designation`, `createdate`, `deletedate`) VALUES
	(1, 515570, 'DRUCKPLATTEN Kodak E.XD C300/7928385 - Rapida 162', '2017-02-07 09:47:01', NULL);
/*!40000 ALTER TABLE `materials` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.partjobs
CREATE TABLE IF NOT EXISTS `partjobs` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle partjobs',
  `job_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, der den Teilauftrag einem Gesamtauftrag zuordnet',
  `identifier` bigint(20) NOT NULL COMMENT 'Identifikationsnummer des Teilauftrags',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung des Teilauftrags',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Erstelldatum des Teilauftrags',
  `deletedate` timestamp NULL DEFAULT NULL COMMENT 'Loeschdatum des Teilauftrags',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `identifier` (`identifier`),
  KEY `FK_partjobs_jobs` (`job_pk`),
  CONSTRAINT `FK_partjobs_jobs` FOREIGN KEY (`job_pk`) REFERENCES `jobs` (`pk`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin
  COMMENT = 'Teilauftraege aus dem Management Informationssystem';

-- Exportiere Daten aus Tabelle time_management.partjobs: ~3 rows (ungefähr)
/*!40000 ALTER TABLE `partjobs` DISABLE KEYS */;
INSERT INTO `partjobs` (`pk`, `job_pk`, `identifier`, `designation`, `createdate`, `deletedate`) VALUES
  (1, 1, 1000353087, 'Andruck', '2017-02-07 09:49:36', NULL),
  (2, 5, -1, 'Vorbereitung', '2017-04-11 08:55:21', NULL),
  (3, 5, -2, 'Erstellung', '2017-04-11 08:55:34', NULL);
/*!40000 ALTER TABLE `partjobs` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle time_management.positions
CREATE TABLE IF NOT EXISTS `positions` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle positions',
  `department_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, welcher die zu der Stelle gehoerenden Abteilung referenziert',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung der Stelle',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung der Stelle',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`),
  KEY `FK_pos_dep` (`department_pk`),
  CONSTRAINT `FK_pos_dep` FOREIGN KEY (`department_pk`) REFERENCES `departments` (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Verzeichnet die in der Anwendung aufgefuehrten Stellen (Positionen)';

-- Exportiere Daten aus Tabelle time_management.positions: ~8 rows (ungefähr)
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` (`pk`, `department_pk`, `designation`, `description`) VALUES
	(1, 2, 'auszubildende/r AnwendungsentwicklerIn', 'FachinformatikerIn für Anwendungsentwicklung in der Ausbildung'),
	(2, 2, 'auszubildende/r SystemintegratorIn', 'FachinformatikerIn für Systemintegration in der Ausbildung'),
	(3, 2, 'SystemadministratorIn', 'AdministratorIn der internen Systeme'),
	(4, 2, 'Software Developer', 'ProgrammiererIn'),
	(5, 1, 'auszubildende/r MediengestalterIn', 'MediengestalterIn in der Ausbildung'),
	(6, 1, 'MediengestalterIn', 'MediengestalterIn'),
	(7, 1, 'SchriftsätzerIn', 'SchriftsätzerIn für Word'),
	(8, 1, 'DatenprüferIn', 'Prüfung der Druckdaten');
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.powers
CREATE TABLE IF NOT EXISTS `powers` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschuessel der Tabelle powers',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung des Rechtes',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung des Benutzerrechtes',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Macht der einer Rolle zugeordneten Benutzerrechte';

-- Exportiere Daten aus Tabelle time_management.powers: ~7 rows (ungefähr)
/*!40000 ALTER TABLE `powers` DISABLE KEYS */;
INSERT INTO `powers` (`pk`, `designation`, `description`) VALUES
  (1, 'CREATE_JOBS', 'Darf Aufträge erstellen'),
	(2, 'ACCESS_OTHER_DAYSHEETS', 'Darf Tageszettel anderer Mitarbeiter einsehen'),
	(3, 'OFFLINE_WORKING', 'Darf aktuelle Tageszettel manuell bearbeiten'),
  (4, 'OFFLINE_WORKING_MASTER', 'Darf alte Tageszettel bearbeiten'),
	(5, 'EDIT_USERS', 'Darf Benutzer bearbeiten, hinzufügen oder löschen'),
	(6, 'EDIT_SETTINGS', 'Darf andere Einstellungen bearbeiten'),
	(7, 'CHANGE_PATH', 'Darf andere Unterverzeichnisse einsehen');
/*!40000 ALTER TABLE `powers` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle roles',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung der Benutzerrolle',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung der Benutzerrolle',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tabelle, welche die Bezeichnungen der Benutzerrollen in der Anwendung bereithaelt';

-- Exportiere Daten aus Tabelle time_management.roles: ~5 rows (ungefähr)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`pk`, `designation`, `description`) VALUES
	(1, 'Administrator', 'Administrator der Anwendung'),
	(20, 'Abteilungsleiter', 'Leiter einer Abteilung'),
	(21, 'Mitarbeiter', NULL),
	(22, 'Auszubildender', NULL),
	(23, 'Gast', NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.rolesxpowers
CREATE TABLE IF NOT EXISTS `rolesxpowers` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Linktabelle rolesXpowers',
  `role_pk` bigint(20) NOT NULL COMMENT 'Fremdschlüssel, der die zugewiesene Rolle referenziert',
  `power_pk` bigint(20) NOT NULL COMMENT 'Fremdschlüssel, der das zugewiesene Recht referenziert',
  `value` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Macht des Benutzerrechtes',
  PRIMARY KEY (`pk`),
  KEY `FK_roles_X` (`role_pk`),
  KEY `FK_powers_X` (`power_pk`),
  CONSTRAINT `FK_powers_X` FOREIGN KEY (`power_pk`) REFERENCES `powers` (`pk`),
  CONSTRAINT `FK_roles_X` FOREIGN KEY (`role_pk`) REFERENCES `roles` (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Linktabelle, welche der Rolle einzelne Rechte zuweist';

-- Exportiere Daten aus Tabelle time_management.rolesxpowers: ~35 rows (ungefähr)
/*!40000 ALTER TABLE `rolesxpowers` DISABLE KEYS */;
INSERT INTO `rolesxpowers` (`pk`, `role_pk`, `power_pk`, `value`) VALUES
	(1, 1, 2, 255),
	(2, 1, 1, 255),
	(3, 1, 6, 255),
	(4, 1, 5, 255),
	(5, 1, 3, 255),
	(6, 1, 4, 255),
	(7, 1, 7, 255),
  (15, 20, 1, 200),
  (16, 20, 2, 200),
	(17, 20, 3, 255),
	(18, 20, 4, 255),
  (19, 20, 5, 200),
  (20, 20, 6, 150),
  (21, 20, 7, 255),
	(22, 21, 1, 0),
	(23, 21, 2, 0),
	(24, 21, 3, 0),
	(25, 21, 4, 0),
	(26, 21, 5, 0),
	(27, 21, 6, 0),
	(28, 21, 7, 100),
	(29, 22, 1, 0),
	(30, 22, 2, 0),
	(31, 22, 3, 0),
	(32, 22, 4, 0),
	(33, 22, 5, 0),
	(34, 22, 6, 0),
	(35, 22, 7, 50),
	(36, 23, 1, 0),
	(37, 23, 2, 0),
	(38, 23, 3, 0),
	(39, 23, 4, 0),
	(40, 23, 5, 0),
	(41, 23, 6, 0),
	(42, 23, 7, 10);
/*!40000 ALTER TABLE `rolesxpowers` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.salutations
CREATE TABLE IF NOT EXISTS `salutations` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschuessel der Tabelle salutations',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung der Anrede',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung der Anrede',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Anreden der Benutzer';

-- Exportiere Daten aus Tabelle time_management.salutations: ~2 rows (ungefähr)
/*!40000 ALTER TABLE `salutations` DISABLE KEYS */;
INSERT INTO `salutations` (`pk`, `designation`, `description`) VALUES
	(1, 'Herr', 'Sehr geehrter'),
	(2, 'Frau', 'Sehr geehrte');
/*!40000 ALTER TABLE `salutations` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.status
CREATE TABLE IF NOT EXISTS `status` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschuessel der Tabelle status',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung des Status',
  `icon` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT 'CSS Klasse des Status',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung des Status',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin
  COMMENT = 'Tabelle, welche den Aktivitaetsstatus des Benutzers beinhaltet';

-- Exportiere Daten aus Tabelle time_management.status: ~3 rows (ungefähr)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` (`pk`, `designation`, `icon`, `description`) VALUES
	(1, 'at Work', NULL, 'Befindet sich auf Arbeit'),
	(2, 'deseased', NULL, 'Ist krank geschrieben'),
	(3, 'On leave', NULL, 'Ist beurlaubt');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.timetypes
CREATE TABLE IF NOT EXISTS `timetypes` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle timetypes',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung der Zeitart',
  `identifier` int(11) NOT NULL COMMENT 'Identifikationsnummer der Zeitart',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Erstelldatum der Zeitart',
  `deletedate` timestamp NULL DEFAULT NULL COMMENT 'Loeschdatum der Zeitart',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `designation` (`designation`),
  UNIQUE KEY `identifier` (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tabelle, die die Zeitarten aus dem Management-Informationssystem beinhaltet';

-- Exportiere Daten aus Tabelle time_management.timetypes: ~3 rows (ungefähr)
/*!40000 ALTER TABLE `timetypes` DISABLE KEYS */;
INSERT INTO `timetypes` (`pk`, `designation`, `identifier`, `createdate`, `deletedate`) VALUES
	(1, 'SCHULUNG/LEHRLINGSAUSBILDUNG', 94, '2017-02-07 09:52:25', NULL),
	(2, 'KRANKHEIT/ARZTBESUCH', 97, '2017-02-07 09:52:46', NULL),
	(3, 'HILFSSTUNDEN', 60, '2017-02-07 09:53:11', NULL);
/*!40000 ALTER TABLE `timetypes` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.users
CREATE TABLE IF NOT EXISTS `users` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle users',
  `position_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, welcher die Position des Benutzers referenziert',
  `department_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, welcher die Abteilung des Benutzers referenziert',
  `role_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, welcher auf die Rolle des Benutzers verweist',
  `status_pk` bigint(20) DEFAULT NULL COMMENT 'Fremdschluessel, welcher auf den Status des Benutzers verweist',
  `salutation_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, der auf die Anrede der Person verweist',
  `picture` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT 'Dateipfad zu einem Profilbild',
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT 'Titel der Person (z.B. Dr. oder Prof.)',
  `firstname` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Vorname der Person',
  `sec_firstname` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT 'Zweiter (und dritter, vierter, ...) Vorname der Person ',
  `lastname` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Nachname der Person',
  `personelnr` varchar(5) COLLATE utf8_bin NOT NULL COMMENT 'Personalnummer der Person',
  `password` varchar(40) COLLATE utf8_bin NOT NULL COMMENT 'Benutzerpasswort',
  `email_business` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Geschäftliche E-Mail Adresse des Benutzers',
  `email_private` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT 'Private E-Mail Adresse des Benutzers',
  `phone_business` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT 'Geschäftliche Telefonnummer des Benutzers',
  `phone_private` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT 'Private Telefonnummer des Benutzers',
  `working_hours_per_day` int(11) NOT NULL DEFAULT '8' COMMENT 'Anzahl der taeglichen Soll-Arbeitsstunden',
  `overtime_limit` int(11) NOT NULL DEFAULT '10' COMMENT 'Maximale Anzahl der Mehrarbeitsstunden',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Erstelldatum des Datenbankeintrags',
  `changedate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Aenderungsdatum des Benutzers',
  `deletedate` timestamp NULL DEFAULT NULL COMMENT 'Loeschdatum des Benutzers',
  `isActive` bit(1) NOT NULL DEFAULT b'0' COMMENT 'Status, ob der Benutzer zur Zeit aktiv an einem Auftrag arbeitet',
  `overtime_value` double NOT NULL DEFAULT '0' COMMENT 'Anzahl der aktuellen Ueberstunden',
  `token` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `tokenExpire` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pk`),
  UNIQUE KEY `personelnr` (`personelnr`),
  KEY `FK_user_position` (`position_pk`),
  KEY `FK_user_department` (`department_pk`),
  KEY `FK_user_role` (`role_pk`),
  KEY `FK_user_status` (`status_pk`),
  KEY `FK_user_salutations` (`salutation_pk`),
  CONSTRAINT `FK_user_department` FOREIGN KEY (`department_pk`) REFERENCES `departments` (`pk`),
  CONSTRAINT `FK_user_position` FOREIGN KEY (`position_pk`) REFERENCES `positions` (`pk`),
  CONSTRAINT `FK_user_role` FOREIGN KEY (`role_pk`) REFERENCES `roles` (`pk`),
  CONSTRAINT `FK_user_salutations` FOREIGN KEY (`salutation_pk`) REFERENCES `salutations` (`pk`),
  CONSTRAINT `FK_user_status` FOREIGN KEY (`status_pk`) REFERENCES `status` (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tabelle, die die Benutzer der Anwendung verzeichnet.';

-- Exportiere Daten aus Tabelle time_management.users: ~2 rows (ungefähr)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`pk`, `position_pk`, `department_pk`, `role_pk`, `status_pk`, `salutation_pk`, `picture`, `prefix`, `firstname`, `sec_firstname`, `lastname`, `personelnr`, `password`, `email_business`, `email_private`, `phone_business`, `phone_private`, `working_hours_per_day`, `overtime_limit`, `createdate`, `changedate`, `deletedate`, `isActive`, `overtime_value`, `token`, `tokenExpire`) VALUES
  (1, 1, 2, 1, NULL, 1, NULL, NULL, 'Maximilian', NULL, 'Beck', '2269', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',
                                                                'm.beck@beltz.de', 'maximilianbmail@gmail.com', NULL,
                                                                NULL, 8, 10, '2017-02-07 09:04:21',
                                                                '2017-04-13 17:57:38', NULL, b'0', 15.4,
   '12ec3cafeef2be7cfb202b5908520ba2cb5d4488', '2017-04-13 18:57:38'),
  (2, 5, 1, 20, NULL, 2, NULL, NULL, 'Test', NULL, 'Bärbel', '0000', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',
                                                             't.baerbel@beltz.de', NULL, NULL, NULL, 8, 10,
                                                             '2017-03-22 16:03:31', '2017-04-13 12:34:15', NULL, b'0',
   -22.34, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.vacationdays
CREATE TABLE IF NOT EXISTS `vacationdays` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle vacationdays',
  `user_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, der den Benutzer referenziert',
  `year` year(4) NOT NULL COMMENT 'Gibt das zugewiesene Jahr an',
  `ammount` tinyint(3) unsigned NOT NULL COMMENT 'Gibt die Anzahl der Urlaubstage an',
  PRIMARY KEY (`pk`),
  KEY `FK_vacationdays_user` (`user_pk`),
  CONSTRAINT `FK_vacationdays_user` FOREIGN KEY (`user_pk`) REFERENCES `users` (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Anzahl der Urlaubstage eines Benutzers pro Jahr';

-- Exportiere Daten aus Tabelle time_management.vacationdays: ~0 rows (ungefähr)
/*!40000 ALTER TABLE `vacationdays` DISABLE KEYS */;
INSERT INTO `vacationdays` (`pk`, `user_pk`, `year`, `ammount`) VALUES
	(1, 1, '2017', 8);
/*!40000 ALTER TABLE `vacationdays` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.vacationrequests
CREATE TABLE IF NOT EXISTS `vacationrequests` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle vacationrequests',
  `vacationtype_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, welcher auf den Typ der Urlaubsanfrage verweist',
  `requesting_user_pk` bigint(20) NOT NULL COMMENT 'Fremdschluessel, welcher auf den anfragenden Benutzer verweist',
  `representating_user_pk` bigint(20) DEFAULT NULL COMMENT 'Fremdschlüssel, welcher auf den vertretenden Benutzer verweist',
  `startdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Startdatum des Urlaubsantrags',
  `enddate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Enddatum des Urlaubsantrags',
  `working_days_ammount` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Anzahl der beurlaubten Arbeitstage',
  `confirmation_representation` timestamp NULL DEFAULT NULL COMMENT 'Datum der eingegangenen Bestaetigung der Vertretung',
  `confirmation_department` timestamp NULL DEFAULT NULL COMMENT 'Datum der eingegangenen Bestaetigung des Abteilungsleiters',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Erstelldatum des Urlaubsantrags',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung des Urlaubsantrags',
  PRIMARY KEY (`pk`),
  KEY `FK_vacationrequests_vacationtypes` (`vacationtype_pk`),
  KEY `FK_vacationrequests_user_request` (`requesting_user_pk`),
  KEY `FK_vacationrequests_user_represent` (`representating_user_pk`),
  CONSTRAINT `FK_vacationrequests_user_represent` FOREIGN KEY (`representating_user_pk`) REFERENCES `users` (`pk`),
  CONSTRAINT `FK_vacationrequests_user_request` FOREIGN KEY (`requesting_user_pk`) REFERENCES `users` (`pk`),
  CONSTRAINT `FK_vacationrequests_vacationtypes` FOREIGN KEY (`vacationtype_pk`) REFERENCES `vacationtypes` (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Urlaubsanfragen';

-- Exportiere Daten aus Tabelle time_management.vacationrequests: ~0 rows (ungefähr)
/*!40000 ALTER TABLE `vacationrequests` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacationrequests` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle time_management.vacationtypes
CREATE TABLE IF NOT EXISTS `vacationtypes` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primaerschluessel der Tabelle vacationtypes',
  `designation` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Bezeichnung des Freistellungstypen',
  `description` text COLLATE utf8_bin COMMENT 'Beschreibung des Freistellungstypen',
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tabelle, welche Freistellungstypen beinhaltet';

-- Exportiere Daten aus Tabelle time_management.vacationtypes: ~5 rows (ungefähr)
/*!40000 ALTER TABLE `vacationtypes` DISABLE KEYS */;
INSERT INTO `vacationtypes` (`pk`, `designation`, `description`) VALUES
	(1, 'Urlaub', 'Befindet sich im Urlaub'),
	(2, 'Schule', 'Befindet sich in der Schule'),
	(3, 'Universität', 'Befindet sich in der Universität'),
	(4, 'Weiterbildung extern', 'Befindet sich auf einer externen Weiterbildung'),
	(5, 'Weiterbildung intern', 'Befindet sich auf einer internen Weiterbildung');
/*!40000 ALTER TABLE `vacationtypes` ENABLE KEYS */;

-- Exportiere Struktur von View time_management.v_dailytasks
-- Erstelle temporäre Tabelle um View Abhängigkeiten zuvorzukommen
CREATE TABLE `v_dailytasks` (
	`id` BIGINT(20) NOT NULL COMMENT 'Primaerschluessel der Tabelle dailytasks',
	`jobId` BIGINT(20) NULL COMMENT 'Fremdschluessel, welcher den bearbeiteten Auftrag referenziert',
	`timetypeId` BIGINT(20) NULL COMMENT 'Fremdschluessel, welcher die Zeitart der Taetigkeit referenziert',
	`costcenterId` BIGINT(20) NOT NULL COMMENT 'Fremdschluessel, welcher die Kostenstelle der Taetigkeit referenziert',
	`materialId` BIGINT(20) NULL COMMENT 'Fremdschluessel, welcher das genutzte Material referenziert',
	`startdate` TIMESTAMP NOT NULL COMMENT 'Anfangszeit der Taetigkeit',
	`enddate` TIMESTAMP NULL COMMENT 'Abschlusszeit der Taetigkeit',
	`pages` INT(11) NULL COMMENT 'Anzahl der Seiten',
	`description` TEXT NULL COMMENT 'Beschreibung der Taetigkeit' COLLATE 'utf8_bin',
	`jobnumber` INT(11) NULL COMMENT 'Identifikationsnummer des Auftrags',
	`jobtitle` VARCHAR(100) NULL COMMENT 'Bezeichnung bzw. Titel des Auftrags' COLLATE 'utf8_bin',
	`timetypeNumber` INT(11) NULL COMMENT 'Identifikationsnummer der Zeitart',
	`timetypeName` VARCHAR(50) NULL COMMENT 'Bezeichnung der Zeitart' COLLATE 'utf8_bin',
	`costcenterNumber` INT(11) NOT NULL COMMENT 'Identifiketionsnummer der Kostenstelle',
	`costcenterName` VARCHAR(100) NOT NULL COMMENT 'Bezeichnung der Kostenstelle' COLLATE 'utf8_bin',
	`materialIdentifier` INT(11) NULL COMMENT 'Identifikationsnummer des Materials',
	`materialName` VARCHAR(50) NULL COMMENT 'Bezeichnung des Materials' COLLATE 'utf8_bin'
) ENGINE=MyISAM;

-- Exportiere Struktur von View time_management.v_jobs
-- Erstelle temporäre Tabelle um View Abhängigkeiten zuvorzukommen
CREATE TABLE `v_jobs` (
	`id` BIGINT(20) NOT NULL COMMENT 'Primaerschluessel der Tabelle jobs',
	`customerId` BIGINT(20) NOT NULL COMMENT 'Fremdschluessel, der dem Auftrag einen Kunden zuordnet',
	`jobnumber` INT(11) NOT NULL COMMENT 'Identifikationsnummer des Auftrags',
	`deletedate` TIMESTAMP NULL COMMENT 'Loeschdatum des Auftrags',
	`jobDesignation` VARCHAR(100) NOT NULL COMMENT 'Bezeichnung bzw. Titel des Auftrags' COLLATE 'utf8_bin',
	`recieptdate` DATE NOT NULL COMMENT 'Eingangsdatum des Auftrags',
	`jobDescription` TEXT NULL COMMENT 'Beschreibung zum Auftrag' COLLATE 'utf8_bin',
	`customerDesignation` VARCHAR(50) NOT NULL COMMENT 'Bezeichnung des Kunden' COLLATE 'utf8_bin',
	`customerDescription` TEXT NULL COMMENT 'Beschreibung des Kunden' COLLATE 'utf8_bin'
) ENGINE=MyISAM;

-- Exportiere Struktur von View time_management.v_powers
-- Erstelle temporäre Tabelle um View Abhängigkeiten zuvorzukommen
CREATE TABLE `v_powers` (
  `roleId`      BIGINT(20)          NOT NULL COMMENT 'Fremdschlüssel, der die zugewiesene Rolle referenziert',
  `powerId`     BIGINT(20)          NOT NULL COMMENT 'Fremdschlüssel, der das zugewiesene Recht referenziert',
  `value`       TINYINT(3) UNSIGNED NOT NULL COMMENT 'Macht des Benutzerrechtes',
  `name`        VARCHAR(50)         NOT NULL
  COMMENT 'Bezeichnung des Rechtes'
  COLLATE 'utf8_bin',
  `description` TEXT                NULL
  COMMENT 'Beschreibung des Benutzerrechtes'
  COLLATE 'utf8_bin'
) ENGINE=MyISAM;

-- Exportiere Struktur von View time_management.v_users
-- Erstelle temporäre Tabelle um View Abhängigkeiten zuvorzukommen
CREATE TABLE `v_users` (
	`id` BIGINT(20) NOT NULL COMMENT 'Primaerschluessel der Tabelle users',
	`positionId` BIGINT(20) NOT NULL COMMENT 'Fremdschluessel, welcher die Position des Benutzers referenziert',
	`departmentId` BIGINT(20) NOT NULL COMMENT 'Fremdschluessel, welcher die Abteilung des Benutzers referenziert',
	`roleId` BIGINT(20) NOT NULL COMMENT 'Fremdschluessel, welcher auf die Rolle des Benutzers verweist',
	`statusId` BIGINT(20) NULL COMMENT 'Fremdschluessel, welcher auf den Status des Benutzers verweist',
	`salutationId` BIGINT(20) NOT NULL COMMENT 'Fremdschluessel, der auf die Anrede der Person verweist',
	`picture` VARCHAR(100) NULL COMMENT 'Dateipfad zu einem Profilbild' COLLATE 'utf8_bin',
	`prefix` VARCHAR(20) NULL COMMENT 'Titel der Person (z.B. Dr. oder Prof.)' COLLATE 'utf8_bin',
	`firstname` VARCHAR(50) NOT NULL COMMENT 'Vorname der Person' COLLATE 'utf8_bin',
	`secondFirstname` VARCHAR(100) NULL COMMENT 'Zweiter (und dritter, vierter, ...) Vorname der Person ' COLLATE 'utf8_bin',
	`lastname` VARCHAR(50) NOT NULL COMMENT 'Nachname der Person' COLLATE 'utf8_bin',
	`personelNumber` VARCHAR(5) NOT NULL COMMENT 'Personalnummer der Person' COLLATE 'utf8_bin',
	`emailBusiness` VARCHAR(100) NOT NULL COMMENT 'Geschäftliche E-Mail Adresse des Benutzers' COLLATE 'utf8_bin',
	`emailPrivate` VARCHAR(100) NULL COMMENT 'Private E-Mail Adresse des Benutzers' COLLATE 'utf8_bin',
	`phoneBusiness` VARCHAR(20) NULL COMMENT 'Geschäftliche Telefonnummer des Benutzers' COLLATE 'utf8_bin',
	`phonePrivate` VARCHAR(20) NULL COMMENT 'Private Telefonnummer des Benutzers' COLLATE 'utf8_bin',
	`workingHoursPerDay` INT(11) NOT NULL COMMENT 'Anzahl der taeglichen Soll-Arbeitsstunden',
	`overtimeLimit` INT(11) NOT NULL COMMENT 'Maximale Anzahl der Mehrarbeitsstunden',
	`deletedate` TIMESTAMP NULL COMMENT 'Loeschdatum des Benutzers',
	`password` VARCHAR(40) NOT NULL COMMENT 'Benutzerpasswort' COLLATE 'utf8_bin',
	`isActive` BIT(1) NOT NULL COMMENT 'Status, ob der Benutzer zur Zeit aktiv an einem Auftrag arbeitet',
	`token` VARCHAR(250) NULL COLLATE 'utf8_bin',
	`tokenExpire` TIMESTAMP NULL,
	`overtimeValue` DOUBLE NOT NULL COMMENT 'Anzahl der aktuellen Ueberstunden',
	`department` VARCHAR(50) NOT NULL COMMENT 'Bezeichnung der Abteilung' COLLATE 'utf8_bin',
	`position` VARCHAR(50) NOT NULL COMMENT 'Bezeichnung der Stelle' COLLATE 'utf8_bin',
	`role` VARCHAR(50) NOT NULL COMMENT 'Bezeichnung der Benutzerrolle' COLLATE 'utf8_bin'
) ENGINE=MyISAM;

-- Exportiere Struktur von View time_management.v_dailytasks
-- Entferne temporäre Tabelle und erstelle die eigentliche View
DROP TABLE IF EXISTS `v_dailytasks`;
CREATE ALGORITHM = UNDEFINED
  DEFINER =`root`@`localhost` VIEW `v_dailytasks` AS
  SELECT
    `dailytasks`.`pk`             AS `id`,
    `dailytasks`.`job_pk`         AS `jobId`,
    `dailytasks`.`timetype_pk`    AS `timetypeId`,
    `dailytasks`.`cost_center_pk` AS `costcenterId`,
    `dailytasks`.`material_pk`    AS `materialId`,
    `dailytasks`.`startdate`      AS `startdate`,
    `dailytasks`.`enddate`        AS `enddate`,
    `dailytasks`.`pages`          AS `pages`,
    `dailytasks`.`description`    AS `description`,
    `jobs`.`jobnr`                AS `jobnumber`,
    `jobs`.`designation`          AS `jobtitle`,
    `timetypes`.`identifier`      AS `timetypeNumber`,
    `timetypes`.`designation`     AS `timetypeName`,
    `costcenters`.`identifier`    AS `costcenterNumber`,
    `costcenters`.`designation`   AS `costcenterName`,
    `materials`.`identifier`      AS `materialIdentifier`,
    `materials`.`designation`     AS `materialName`
  FROM ((((`dailytasks`
    LEFT JOIN `jobs` ON (((`dailytasks`.`job_pk` IS NOT NULL) AND (`dailytasks`.`job_pk` = `jobs`.`pk`)))) JOIN
    `costcenters` ON ((`dailytasks`.`cost_center_pk` = `costcenters`.`pk`))) LEFT JOIN `timetypes`
      ON (((`dailytasks`.`timetype_pk` IS NOT NULL) AND (`timetypes`.`pk` = `dailytasks`.`timetype_pk`)))) LEFT JOIN
    `materials` ON (((`dailytasks`.`material_pk` IS NOT NULL) AND (`materials`.`pk` = `dailytasks`.`material_pk`))));

-- Exportiere Struktur von View time_management.v_jobs
-- Entferne temporäre Tabelle und erstelle die eigentliche View
DROP TABLE IF EXISTS `v_jobs`;
CREATE ALGORITHM = UNDEFINED
  DEFINER =`root`@`localhost` VIEW `v_jobs` AS
  SELECT
    `jobs`.`pk`               AS `id`,
    `jobs`.`customer_pk`      AS `customerId`,
    `jobs`.`jobnr`            AS `jobnumber`,
    `jobs`.`deletedate`       AS `deletedate`,
    `jobs`.`designation`      AS `jobDesignation`,
    `jobs`.`recieptdate`      AS `recieptdate`,
    `jobs`.`description`      AS `jobDescription`,
    `customers`.`designation` AS `customerDesignation`,
    `customers`.`description` AS `customerDescription`
  FROM (`jobs`
    JOIN `customers` ON ((`jobs`.`customer_pk` = `customers`.`pk`)));

-- Exportiere Struktur von View time_management.v_powers
-- Entferne temporäre Tabelle und erstelle die eigentliche View
DROP TABLE IF EXISTS `v_powers`;
CREATE ALGORITHM = UNDEFINED
  DEFINER =`root`@`localhost` VIEW `v_powers` AS
  SELECT
    `rolesxpowers`.`role_pk`  AS `roleId`,
    `rolesxpowers`.`power_pk` AS `powerId`,
    `rolesxpowers`.`value`    AS `value`,
    `powers`.`designation`    AS `name`,
    powers.description
  FROM (`rolesxpowers`
    JOIN `powers` ON ((`rolesxpowers`.`power_pk` = `powers`.`pk`)));

-- Exportiere Struktur von View time_management.v_users
-- Entferne temporäre Tabelle und erstelle die eigentliche View
DROP TABLE IF EXISTS `v_users`;
CREATE ALGORITHM = UNDEFINED
  DEFINER =`root`@`localhost` VIEW `v_users` AS
  SELECT
    `users`.`pk`                    AS `id`,
    `users`.`position_pk`           AS `positionId`,
    `users`.`department_pk`         AS `departmentId`,
    `users`.`role_pk`               AS `roleId`,
    `users`.`status_pk`             AS `statusId`,
    `users`.`salutation_pk`         AS `salutationId`,
    `users`.`picture`               AS `picture`,
    `users`.`prefix`                AS `prefix`,
    `users`.`firstname`             AS `firstname`,
    `users`.`sec_firstname`         AS `secondFirstname`,
    `users`.`lastname`              AS `lastname`,
    `users`.`personelnr`            AS `personelNumber`,
    `users`.`email_business`        AS `emailBusiness`,
    `users`.`email_private`         AS `emailPrivate`,
    `users`.`phone_business`        AS `phoneBusiness`,
    `users`.`phone_private`         AS `phonePrivate`,
    `users`.`working_hours_per_day` AS `workingHoursPerDay`,
    `users`.`overtime_limit`        AS `overtimeLimit`,
    `users`.`deletedate`            AS `deletedate`,
    `users`.`password`              AS `password`,
    `users`.`isActive`              AS `isActive`,
    `users`.`token`                 AS `token`,
    `users`.`tokenExpire`           AS `tokenExpire`,
    `users`.`overtime_value`        AS `overtimeValue`,
    `departments`.`designation`     AS `department`,
    `positions`.`designation`       AS `position`,
    `roles`.`designation`           AS `role`
  FROM (((`users`
    JOIN `departments` ON ((`users`.`department_pk` = `departments`.`pk`))) JOIN `positions`
      ON ((`users`.`position_pk` = `positions`.`pk`))) JOIN `roles` ON ((`users`.`role_pk` = `roles`.`pk`)));
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
