<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for materials database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class MaterialsController extends RestController
{

    /**
     * Selects all materials or the material by id
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        if ($id)
            $materials = $this->loader->select("materials", [
                "pk" => "id",
                "designation" => "name",
                "identifier"
            ], [
                "deletedate" => "NULL",
                "pk" => $id
            ]);
        else
            $materials = $this->loader->select("materials", [
                "pk" => "id",
                "designation" => "name",
                "identifier"
            ], [
                "deletedate" => "NULL"
            ]);

        return $response->getBody()->write(json_encode($materials));
    }

    /**
     * POST function for materials
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * PUT function for materials
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function put(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * DELETE function for materials
     * (not available)
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function delete(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }
}