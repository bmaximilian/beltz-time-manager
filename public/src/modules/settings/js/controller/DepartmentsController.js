import Helper from '../../../app/js/classes/Helper.class';
import DepartmentsDialogController from '../controller/DepartmentsDialogController'

var allDepartments = [];

/**
 * controller for the department management of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function ($scope, $mdDialog, $mdToast, BaseUrl, DepartmentsFactory) {
    "use strict";

    $scope.departmentsToggle = false;
    $scope.departments = [];

    DepartmentsFactory.getDepartments(null, function (data) {
        $scope.departments = data;
    });

    $scope.changeDepartmentsToggle = function () {
        $scope.departmentsToggle = ($scope.departmentsToggle ? false : true);
    };

    if (!$scope.text)
        $scope.text = {};
    $scope.text.departmentsSearchInput = '';

    $scope.changeDepartmentsSearch = function () {
        if ($scope.text.departmentsSearchInput.length > 0) {
            if (allDepartments.length === 0)
                allDepartments = $scope.departments;

            $scope.departments = allDepartments.filter(function (department) {
                if (department.description)
                    return department.name.toLowerCase().includes($scope.text.departmentsSearchInput.toLowerCase())
                        || department.description.toLowerCase().includes($scope.text.departmentsSearchInput.toLowerCase());
                else
                    return department.name.toLowerCase().includes($scope.text.departmentsSearchInput.toLowerCase());
            });
        } else {
            $scope.departments = allDepartments;
        }
    };

    $scope.openDepartmentsEditDialog = function (ev, department) {
        $mdDialog.show({
            controller: DepartmentsDialogController,
            templateUrl: BaseUrl.viewPath + "settings/views/departmentsDialog.html",
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                department: department,
                action: 'edit'
            }
        })
            .then(function (answer) {
                $scope.departments = answer.data;
                Helper.standardToast($mdToast, answer.message, 3000, "bottom left");
            });
    };

    $scope.openDepartmentsAddDialog = function (ev) {
        $mdDialog.show({
            controller: DepartmentsDialogController,
            templateUrl: BaseUrl.viewPath + "settings/views/departmentsDialog.html",
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                department: null,
                action: 'add'
            }
        })
            .then(function (answer) {
                $scope.departments = answer.data;
                Helper.standardToast($mdToast, answer.message, 3000, "bottom left");
            });
    };

    $scope.deleteDepartment = function (id) {
        DepartmentsFactory.deleteDepartment(id, function () {
            DepartmentsFactory.getDepartments(null, function (departments) {
                $scope.departments = departments;
                Helper.standardToast($mdToast, "Abteilung erfolgreich gelöscht", 3000, "bottom left");
            });
        });
    };

}