<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for user database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class UsersController extends RestController
{

    use DatabaseUser;

    /**
     * GET route for users
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        if ($id)
            $result = $this->loader->select("v_users", null, [
                "deletedate" => "NULL",
                "id" => $id
            ]);
        else
            $result = $this->loader->select("v_users", null, [
                "deletedate" => "NULL"
            ]);

        $out = [];

        foreach ($result as $r) {
            $r['name'] = $this->getNameForUser($r);
            array_push($out, $r);
        }

        return $response->getBody()->write(json_encode($out));
    }

    /**
     * Returns all users of one department
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return int
     */
    public function getByDepartment(Request $request, Response $response)
    {
        $departmentId = $request->getAttribute('id');

        $result = $this->loader->select("v_users", null, [
            "deletedate" => "NULL",
            "departmentId" => $departmentId
        ]);

        $out = [];

        foreach ($result as $r) {
            $r['name'] = $this->getNameForUser($r);
            array_push($out, $r);
        }

        return $response->getBody()->write(json_encode($out));
    }

    /**
     * PUT route for users
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function put(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        $result = $this->loader->update("v_users", $this->getRequestBody($request), [
            "id" => $id
        ]);

        return $response->getBody()->write(json_encode($result));
    }

    /**
     * Resolves the body for post and put requests
     *
     * @param Request $request
     *
     * @return array
     */
    private function getRequestBody(Request $request)
    {
        $ret = array(
            'positionId' => @$request->getParsedBody()['positionId'],
            'departmentId' => @$request->getParsedBody()['departmentId'],
            'roleId' => @$request->getParsedBody()['roleId'],
            'statusId' => @$request->getParsedBody()['statusId'],
            'salutationId' => @$request->getParsedBody()['salutationId'],
            'picture' => @$request->getParsedBody()['picture'],
            'prefix' => @$request->getParsedBody()['prefix'],
            'firstname' => @$request->getParsedBody()['firstname'],
            'secondFirstname' => @$request->getParsedBody()['secondFirstname'],
            'lastname' => @$request->getParsedBody()['lastname'],
            'personelNumber' => @$request->getParsedBody()['personelNumber'],
            'emailBusiness' => @$request->getParsedBody()['emailBusiness'],
            'emailPrivate' => @$request->getParsedBody()['emailPrivate'],
            'phoneBusiness' => @$request->getParsedBody()['phoneBusiness'],
            'phonePrivate' => @$request->getParsedBody()['phonePrivate'],
            'workingHoursPerDay' => @$request->getParsedBody()['workingHoursPerDay'],
            'overtimeLimit' => @$request->getParsedBody()['overtimeLimit'],
            'overtimeValue' => @$request->getParsedBody()['overtimeValue']
        );

        if (@$request->getParsedBody()['password'])
            $ret['password'] = sha1(@$request->getParsedBody()['password']);

        return $ret;
    }

    /**
     * POST route for users
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        $result = $this->loader->insert("v_users", $this->getRequestBody($request));

        return $response->getBody()->write(json_encode($result));
    }


    /**
     * DELETE route for users
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function delete(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        /*$result = $this->loader->delete("v_users", [
            "id" => $id
        ]);*/

        $result = $this->loader->update("v_users", [
            "deletedate" => date("Y-m-d H:i:s")
        ], [
            "id" => $id
        ]);

        return $response->getBody()->write(json_encode($result));
    }

}