import UsersController from '../controller/UsersController'

/**
 * directive for the users panel
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function (BaseUrl) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: BaseUrl.viewPath + "settings/views/usersView.html",
        controller: UsersController
    }
};