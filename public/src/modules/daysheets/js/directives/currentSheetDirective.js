import CurrentSheetController from '../controller/CurrentSheetController'

/**
 * directive to manage the Daysheet for the current day
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function (BaseUrl) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: BaseUrl.viewPath + "daysheets/views/currentSheet.html",
        controller: CurrentSheetController
    }
};