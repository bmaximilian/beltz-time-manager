<?php
/**
 * Controller for rendering view functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
abstract class ViewBaseController extends \BaseController {

    protected $view;

    /**
     * ViewBaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $view = $this->container->get("view");
        if ($view instanceof \Slim\Views\PhpRenderer) {
            $this->view = $view;
        }
    }



}