/**
 * Base class for object oriented controllers
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class ControllerBase {

    /**
     * Returns the Item passed by name
     * as JSON Object from local storage
     *
     * @param name
     * @returns {*}
     * @protected
     */
    static _getStorageItem(name) {
        "use strict";
        let item = null;
        try {
            item = JSON.parse(localStorage.getItem(name));
        } catch (e) {
        }
        return item;
    }
}

export default ControllerBase;