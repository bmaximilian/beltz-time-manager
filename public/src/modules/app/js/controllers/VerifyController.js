import Helper from '../classes/Helper.class'
import sha1 from 'sha1'

/**
 * controller for the user manasgement of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
class VerifyController {

    /**
     * Checks the AngularJS form if it contains errors
     *
     * @param $scope
     * @returns {boolean}
     * @private
     */
    static _noFormError($scope) {
        let out = false;

        try {
            out = Helper.isEmpty($scope.verifyForm.$error);
        } catch (e) {
        }

        return out;
    }

    /**
     * VerifyController
     *
     * @param $mdDialog
     * @param $scope
     * @param password
     */
    static controller($mdDialog, $scope, password) {
        $scope.cancelPrompt = function () {
            $mdDialog.cancel();
        };

        $scope.verify = {
            password: null
        };

        $scope.submitPrompt = function () {
            if (VerifyController._noFormError($scope)) {
                if (sha1($scope.verify.password) == password) {
                    $mdDialog.hide(true);
                }
            }
        };
    }
}

export default VerifyController.controller