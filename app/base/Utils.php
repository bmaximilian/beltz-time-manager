<?php

/**
 * Helper class for the application
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
abstract class Utils
{

    /**
     * Function for parsing an ini file
     *
     * @param $ini_file - name of INI File
     * @param $ini_path - path to INI file (default is bin/)
     * @return array        - returns parsed ini
     */
    public static function getIni($ini_file, $ini_path = "bin/")
    {
        $ini_path = realpath("./".$ini_path)."/";

        if (strpos($ini_file, '/') === false)
            return parse_ini_file($ini_path . $ini_file, TRUE);
        else
            return parse_ini_file($ini_file);
    }


    /**
     * Requires all PHP files for the app
     *
     * @param $baseDir
     */
    public static function requireApp($baseDir) {
        $files = Utils::getPhpFiles($baseDir);

        foreach ($files as $file) {
            require_once $file;
        }
    }

    /**
     * Returns an Array containing the Paths to all PHP Files
     * in the App structure
     *
     * @param $baseDir
     * @return array
     */
    private static function getPhpFiles($baseDir) {
        $files = [];
        $appDirs = array_filter(glob($baseDir . '/app/*'), 'is_dir');
        $routeDirs = array_filter(glob($baseDir . '/routing/*'), 'is_dir');

        foreach (glob($baseDir . "/app/traits/*.php") as $filename) {
            array_push($files, $filename);
        }

        foreach (glob($baseDir . "/app/*.php") as $filename) {
            array_push($files, $filename);
        }

        foreach ($appDirs as $dir) {
            if (!Utils::endsWith($dir, "traits"))
                foreach (glob($dir . "/*.php") as $filename) {
                    array_push($files, $filename);
                }
        }

        foreach (glob($baseDir . "/routing/*.php") as $filename) {
            array_push($files, $filename);
        }

        foreach ($routeDirs as $dir) {
            foreach (glob($dir . "/*.php") as $filename) {
                array_push($files, $filename);
            }
        }

        return $files;
    }

    /**
     * Checks string if it starts with given characters
     *
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function startsWith($haystack, $needle) {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * Checks string if it ends with given characters
     *
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function endsWith($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

}