<?php
/**
 * Controller for database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
abstract class DatabaseBaseController extends \BaseController {

    protected $loader;

    /**
     * DatabaseBaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $loader = $this->container->get("loader");
        if ($loader instanceof Contentloader) {
            $this->loader = $loader;
        }
    }



}