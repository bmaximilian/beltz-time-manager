<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for roles database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class RolesController extends RestController
{

    public  function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $out = [];

        if ($id)
            $roles = $this->loader->select("roles", [
                "pk" => "id",
                "designation" => "name",
                "description"
            ], [
                "pk" => $id
            ]);
        else
            $roles = $this->loader->select("roles", [
                "pk" => "id",
                "designation" => "name",
                "description"
            ]);

        foreach ($roles as $role) {
            $powers = $this->loader->select("v_powers", null, [
                "roleId" => $role['id']
            ]);
            $role['powers'] = $powers;
            array_push($out, $role);
        }

        return $response->getBody()->write(json_encode($out));
    }

    public  function post(Request $request, Response $response)
    {

        $result = $this->loader->insert("roles", $this->getRequestBody($request));

        $powers = @$request->getParsedBody()['powers'];
        $powersResult = array();

        if ($powers)
            foreach ($powers as $power) {
                $powerId = @$power['id'];

                if (!$powerId)
                    $powerId = $this->loader->select("powers", [
                        "pk" => "id"
                    ], [
                        "designation" => $power['designation']
                    ])['id'];

                if ($result['pk'] && $powerId && isset($power['value'])) {
                    $powerResult = $this->loader->insert("v_powers", [
                        "roleId" => $result['pk'],
                        "powerId" => $powerId,
                        "value" => $power['value']
                    ]);
                    array_push($powersResult, $powerResult);
                }
            }

        $result['powers'] = $powersResult;

        return $response->getBody()->write(json_encode($result));
    }

    public function getRequestBody(Request $request)
    {
        return array(
            "designation" => @$request->getParsedBody()['name'],
            "description" => @$request->getParsedBody()['description']
        );
    }

    public  function put(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        $result = $this->loader->update("roles", $this->getRequestBody($request), [
            "pk" => $id
        ]);

        $powers = @$request->getParsedBody()['powers'];
        $powersResult = array();

        if ($powers)
            foreach ($powers as $power) {
                $powerId = @$power['id'];

                if (!$powerId)
                    $powerId = @$this->loader->select("powers", [
                        "pk" => "id"
                    ], [
                        "designation" => $power['name']
                    ])[0]['id'];

                if ($id && $powerId && $power['value'] != null) {
                    $powerResult = $this->loader->update("v_powers", [
                        "value" => $power['value']
                    ], [
                        "roleId" => $result['pk'],
                        "powerId" => $powerId
                    ]);
                    array_push($powersResult, $powerResult);
                }
            }

        $result['powers'] = $powersResult;

        return $response->getBody()->write(json_encode($result));
    }

    public  function delete(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');

        $result = $this->loader->delete("rolesxpowers", [
            "role_pk" => $id
        ]);

        if ($result)
            $result = $this->loader->delete("roles", [
                "pk" => $id
            ]);

        return $response->getBody()->write(json_encode($result));
    }

}