import OldSheetsController from '../controller/OldSheetsController'

/**
 * directive to manage old Daysheets of one user
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
export default function (BaseUrl) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: BaseUrl.viewPath + "daysheets/views/oldSheets.html",
        controller: OldSheetsController,
        scope: {
            ownerOfSheet: "=ownerOfSheet"
        }
    }
};