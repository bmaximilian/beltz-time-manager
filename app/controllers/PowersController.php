<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Controller for powers database functions
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class PowersController extends RestController
{

    /**
     * GET function for powers
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function get(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $out = [];

        if ($id)
            $powers = $this->loader->select("powers", [
                "pk" => "id",
                "designation" => "name",
                "description"
            ], [
                "pk" => $id
            ]);
        else
            $powers = $this->loader->select("powers", [
                "pk" => "id",
                "designation" => "name",
                "description"
            ]);

        return $response->getBody()->write(json_encode($powers));
    }

    /**
     * POST function for powers
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function post(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

    /**
     * PUT function for powers
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function put(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }


    /**
     * DELETE function for powers
     *
     * @param Request $request
     * @param Response $response
     * @return int
     */
    public  function delete(Request $request, Response $response)
    {
        return $response->getBody()->write(json_encode(false));
    }

}