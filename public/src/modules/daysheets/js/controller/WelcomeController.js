/**
 * controller for managing the welcome message
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
import ControllerBase from '../classes/ControllerBase'
import Daysheet from '../classes/Daysheet.class'

class WelcomeController extends ControllerBase {

    /**
     * Returns the first welcome message or instructions to sign in
     *
     * @param user
     * @returns {string}
     * @private
     */
    static _getWelcomeMessage(user) {
        "use strict";
        return (user && user.isLoggedIn ?
            "Sie haben sich erfolgreich an der Beltz Zeiterfassungssoftware angemeldet" :
            "Sie müssen sich anmelden um auf die Anwendung zuzugreifen.");
    }

    /**
     * Returns the greeting when signed in
     *
     * @param user
     * @returns {string}
     * @private
     */
    static _getGreeting(user) {
        "use strict";
        return (user ? "Hallo, " + user.name : "")
    }

    /**
     * Returns the overtime sentence
     *
     * @param user
     * @returns {string}
     * @private
     */
    static _getOvertime(user) {
        "use strict";
        return (user ? "Sie haben " + user.overtimeValue + " Überstunden" : "")
    }

    /**
     * Checks if the localStorage "activeUser" has changed
     * and updates the controller
     *
     * @param scope
     * @returns {Function}
     * @private
     */
    static _checkUser(scope) {
        return function () {
            let user = WelcomeController._getStorageItem("activeUser");
            let daysheet = WelcomeController._getStorageItem("activeDaysheet");

            if (user != scope.welcome.user) {

                if (daysheet && user) {
                    if (daysheet.userId != user.id) {
                        daysheet = null;
                        localStorage.removeItem("activeDaysheet")
                    }
                }

                scope.welcome.user = user;
                scope.welcome.daysheet = daysheet;

                scope.welcome.message = WelcomeController._getWelcomeMessage(scope.welcome.user);
                scope.welcome.greeting = WelcomeController._getGreeting(scope.welcome.user);
                scope.welcome.overtime = WelcomeController._getOvertime(scope.welcome.user);
                scope.welcome.time = WelcomeController._getWorkingTime(daysheet)
            }
        };
    }

    /**
     * Returns the sentence saying how long the current working day was/is
     *
     * @param daysheet
     * @returns {string}
     * @private
     */
    static _getWorkingTime(daysheet) {
        let outString = "";

        if (daysheet) {
            let starttime = new Date(daysheet.startdate);
            let endtime = daysheet.enddate;
            let time = Daysheet.returnWorkedTimeFromTasks(daysheet.tasks, false);

            let minuteString = (time.minutes > 0 ? ` und ${time.minutes} Minuten` : ``);
            
            outString = `Sie haben heute ${time.hours} Stunden${minuteString} gearbeitet`;
        }

        return outString;
    }

    /**
     * WelcomeController
     *
     * @param $scope
     * @param $interval
     */
    static controller($scope, $interval) {
        "use strict";
        let user = WelcomeController._getStorageItem("activeUser");
        let daysheet = WelcomeController._getStorageItem("activeDaysheet");

        $scope.welcome = {
            user: user,
            daysheet: daysheet,
            message: WelcomeController._getWelcomeMessage(user),
            greeting: WelcomeController._getGreeting(user),
            overtime: WelcomeController._getOvertime(user),
            time: WelcomeController._getWorkingTime(daysheet)
        };

        $interval(WelcomeController._checkUser($scope), 1000);
    }

}


export default WelcomeController.controller;