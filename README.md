# Installation Manual
The App requires **Composer** and an webserver with **PHP 5.6** support. **PHP PDO** for MySQL needs to be installed.
The webserver needs to support *rewrite rules* from the *.htdocs*. It should be an **apache2** with **modRewrite** Rewrite Engine.

The database server should be **MariaDB 10.1**.

The mail server must support **SMTP**.


### Install PHP Dependencies
The PHP API requires some external plugins. These plugins can be loaded via **Composer**. Make sure you have **Composer** installed and globally available in your cli.

You need to run the following command from your project directory:
```bash
composer install
```

### Install JavaScript Dependencies
To get the JavaScript Frontend working some **NodeJS** modules are required. Therefore you need to have **NodeJS** and the **Node Package Manager** installed.

You need to run the following commands from your project directory:
```bash
cd public
npm install
```

### Configure the Settings

Edit the **slim.ini** in the */bin/* directory. Paste the links to your database and mail server.