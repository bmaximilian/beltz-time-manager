<?php

include_once '../app/base/BaseRouter.php';
include_once '../routing/Router.php';
include_once '../vendor/autoload.php';

/**
 * Class RouterTest
 *
 * Test for the application router
 */
class RouterTest extends PHPUnit_Framework_TestCase
{

    /**
     * Tests if router is creatable and if the functions are available
     *
     * @throws Exception
     */
    public function testRouter()
    {
        $result = false;
        try {
            $router = new Router(new Slim\App());
            $router->register();
            $router->route();
            $result = true;
        } catch (Exception $e) {
            $result = false;
            throw $e;
        }

        $this->assertEquals(
            true,
            $result
        );
    }

}
