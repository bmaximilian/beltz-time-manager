import Helper from '../../../app/js/classes/Helper.class'
import VerifyController from '../../../app/js/controllers/VerifyController'

/**
 * controller for the user management of the settings module
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
class OwnUserController {

    /**
     * Detects errors in AngularJS form
     *
     * @param $scope
     * @returns {boolean}
     * @private
     */
    static _noFormError($scope) {
        let out = false;

        try {
            out = Helper.isEmpty($scope.ownUserForm.$error);
        } catch (e) {
        }

        return out;
    }

    /**
     * Validates user input
     *
     * @param $scope
     * @returns {null|Document.userForm.firstname|Document.ownUserForm.firstname|Document.ownUserForm.lastname|Document.userForm.lastname|Document.userForm.emailBusiness|*}
     * @private
     */
    static _validate($scope) {
        return ($scope.own.user.salutationId
            && $scope.own.user.firstname
            && $scope.own.user.lastname
            && $scope.own.user.emailBusiness
            && $scope.own.user.password == $scope.own.user._passwordTest
        );
    }

    /**
     * Submitts dialog and sends the input data to the backend
     *
     * @param Users
     * @param $scope
     * @param $mdDialog
     * @param BaseUrl
     * @param password
     * @returns {Function}
     * @private
     */
    static _submit(Users, $scope, $mdDialog, BaseUrl, password) {
        return function () {

            var prompt = {
                controller: VerifyController,
                templateUrl: BaseUrl.viewPath + 'app/views/verifyDialogView.html',
                parent: angular.element(document.body),
                targetEvent: null,
                multiple: true,
                locals: {
                    password: password
                }
            };

            $mdDialog.show(prompt).then(function (ok) {
                if (ok) {
                    if (OwnUserController._validate($scope) && OwnUserController._noFormError($scope)) {
                        if (!$scope.own.user.password || $scope.own.user.password == '')
                            delete $scope.own.user.password;

                        Users.updateUser($scope.own.user.id, $scope.own.user, function () {
                            Users.getUsers($scope.own.user.id, function (user) {
                                if (user && user.length == 1) {
                                    user[0].isLoggedIn = $scope.own.user.isLoggedIn;
                                    user[0].powers = $scope.own.user.powers;
                                    $mdDialog.hide({
                                        message: `Benutzer ${user[0].name} erfolgreich bearbeitet`,
                                        data: user[0]
                                    });
                                }
                            })
                        });
                    }
                }
            }, function () {

            });
        }
    }

    /**
     * OwnUserController
     *
     * @param $mdDialog
     * @param $scope
     * @param user
     * @param Users
     * @param SalutationsFactory
     * @param BaseUrl
     */
    static controller($mdDialog, $scope, user, Users, SalutationsFactory, BaseUrl) {
        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        const oldPassword = user.password;

        $scope.submit = OwnUserController._submit(Users, $scope, $mdDialog, BaseUrl, oldPassword);

        user.password = null;

        $scope.own = {
            user: user,
            headline: `${user.name} (${user.personelNumber})`,
            salutations: [{
                id: user.salutationId,
                name: user.salutation
            }]
        };

        SalutationsFactory.getSalutations(null, function (data) {
            $scope.own.salutations = data;
        });
    }
}

export default OwnUserController.controller