/**
 * Test JavaScript file for main application container
 *
 * @author <m.beck@beltz.de>
 */
import Helper from '../../src/modules/app/js/classes/Helper.class'

describe("testKarma", function () {

    it("works", function () {
        expect(1).toEqual(1);
    });

    it("recognizes empty object", function () {
        "use strict";
        const map = {};

        expect(Helper.isEmpty(map)).toBe(true);
    });

    it("can cleanup objects", function () {
        "use strict";
        const map = {
            foo: null,
            bar: ''
        };

        expect(Helper.isEmpty(Helper.cleanup(map))).toBe(true);
    });
});