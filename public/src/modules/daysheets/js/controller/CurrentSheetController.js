import Daysheet from '../classes/Daysheet.class'
import Helper from '../../../app/js/classes/Helper.class'
import ControllerBase from '../classes/ControllerBase'
import AddTaskDialogController from './AddTaskDialogController'

/**
 * controller for managing the Daysheet for the current day
 *
 * @author Maximilian Beck <m.beck@beltz.de>
 *
 */
class CurrentSheetController extends ControllerBase {

    /**
     * Opens dialog to add or edit tasks
     *
     * @param $mdDialog
     * @param ev
     * @param BaseUrl
     * @param locals
     * @param callback
     * @private
     */
    static _showTaskDialog($mdDialog, ev, BaseUrl, locals, callback) {
        $mdDialog.show({
            controller: AddTaskDialogController,
            templateUrl: BaseUrl.viewPath + "daysheets/views/addTaskDialog.html",
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: locals
        })
            .then(callback);
    }

    /**
     * Checks if the last daysheet (of the previous day) has no enddate
     * and updates it
     *
     * @param $mdDialog
     * @param daysheet
     * @param sheet
     * @param scope
     * @private
     */
    static _checkForgotCheckout($mdDialog, daysheet, sheet, scope) {
        let time = Daysheet.returnWorkedTime(daysheet.startdate, daysheet.enddate, false);
        if ((time.hours + time.minutes / 60) > 20) {
            var prompt = $mdDialog.prompt()
                .title('Vergessen letzten Tageszettel abzuschicken')
                .textContent('Wann endete Ihr letzter arbeitstag?')
                .placeholder('Uhrzeit eingeben (hh:mm)')
                .ariaLabel('Uhrzeit')
                .targetEvent(null)
                .ok('Abschicken')
                .cancel(null);

            $mdDialog.show(prompt).then(function (value) {
                const pattern = /^[0-2]\d:[0-5]\d$/;
                if (pattern.test(value)) {
                    daysheet.enddate = Daysheet.parseTimeToTimestamp(value, daysheet.startdate);
                    if (daysheet.tasks && daysheet.tasks.length > 0 && daysheet.tasks[daysheet.tasks.length - 1].enddate == null)
                        daysheet.tasks[daysheet.tasks.length - 1].enddate = daysheet.enddate;

                    sheet.endDay(daysheet.id, function (answer) {
                        if (answer) {
                            console.log(`Tageszettel mit Uhrzeit ${value} abgeschickt`);
                            scope.currentSheet.startDay();
                        } else
                            console.error(`Fehler beim abschicken des Tageszettels mit der Uhrzeit ${value}`);
                    }, daysheet.enddate);
                } else {
                    CurrentSheetController._checkForgotCheckout($mdDialog, daysheet);
                }
            }, function () {
                CurrentSheetController._checkForgotCheckout($mdDialog, daysheet);
            });
        }
    }

    /**
     * CurrentSheetController
     *
     * @param $scope
     * @param $interval
     * @param $mdToast
     * @param $mdDialog
     * @param BaseUrl
     * @param DaysheetFactory
     */
    static controller($scope, $interval, $mdToast, $mdDialog, BaseUrl, DaysheetFactory) {

        var activeUser = CurrentSheetController._getStorageItem("activeUser");
        $scope.currentSheet = {};

        $scope.currentSheet.toggle = true;
        $scope.currentSheet.changeToggle = function () {
            $scope.currentSheet.toggle = ($scope.currentSheet.toggle ? false : true);
        };

        var sheet = new Daysheet(DaysheetFactory);
        $scope.currentSheet.submitted = false;

        $scope.currentSheet.startDay = function (newSheet = false) {
            sheet.startDay(activeUser.id, newSheet, function (daysheet) {
                $scope.currentSheet.daysheet = daysheet;

                localStorage.setItem("activeDaysheet", JSON.stringify(daysheet));

                $scope.currentSheet.submitted = !(!daysheet.enddate);

                $interval(function () {
                    var sh = $scope.currentSheet.daysheet;
                    if (sh.tasks)
                        for (var i = 0; i < sh.tasks.length; i++) {
                            sh.tasks[i].time = Daysheet.calculate(sh.tasks[i].startdate, sh.tasks[i].enddate);
                        }

                    if (sh.tasks && sh.tasks.length > 0) {
                        sh.time = Daysheet.calculateTasks(sh.tasks);
                    }

                    $scope.currentSheet.daysheet = sh;
                }, 1000);

                CurrentSheetController._checkForgotCheckout($mdDialog, daysheet, sheet, $scope);
            });
        };

        if (activeUser) {
            $scope.currentSheet.startDay();
        }

        $scope.currentSheet.checkout = function (ev) {

            var confirm = $mdDialog.confirm()
                .title('Tageszettel abschicken?')
                .textContent('Sind Sie sicher, dass Sie Ihren Tageszettel abschicken möchten?')
                .ariaLabel('Bestätigung')
                .targetEvent(ev)
                .ok('Ja, Abschicken!')
                .cancel('Verwerfen');

            $mdDialog.show(confirm).then(function () {
                sheet.endDay($scope.currentSheet.daysheet.id, function (answer) {
                    if (answer) {
                        $scope.currentSheet.submitted = true;

                        if ($scope.currentSheet.daysheet.tasks && $scope.currentSheet.daysheet.tasks.length > 0)
                            $scope.currentSheet.daysheet.tasks[$scope.currentSheet.daysheet.tasks.length - 1].enddate = Daysheet._dateToTimestamp(new Date());

                        Helper.standardToast($mdToast, "Tageszettel verschickt");
                    } else
                        Helper.standardToast($mdToast, "Fehler beim verschicken des Tageszettels");
                });
            }, function () {
            });
        };

        $scope.currentSheet.openTaskAddDialog = function (ev) {
            let task = null;
            var callback = function (answer) {
                $scope.currentSheet.daysheet = answer.data;
                localStorage.setItem("activeDaysheet", JSON.stringify(answer.data));
                Helper.standardToast($mdToast, answer.message);
            };

            if ($scope.currentSheet.daysheet.tasks &&
                $scope.currentSheet.daysheet.tasks.length > 0 &&
                $scope.currentSheet.daysheet.tasks[$scope.currentSheet.daysheet.tasks.length - 1].costcenterId < 0) {

                var confirm = $mdDialog.confirm()
                    .title('Vorherige Aufgabe fortsetzen?')
                    .textContent('Möchten Sie die Aufgabe, die Sie vor der Pause bearbeitet haben fortsetzen?')
                    .ariaLabel('Aufgabe fortsetzen')
                    .targetEvent(ev)
                    .ok('Ja')
                    .cancel('Nein');

                $mdDialog.show(confirm).then(function () {
                    let oldTask = $scope.currentSheet.daysheet.tasks[$scope.currentSheet.daysheet.tasks.length - 2];
                    task = {
                        costcenterId: oldTask.costcenterId,
                        costcenterName: oldTask.costcenterName,
                        costcenterNumber: oldTask.costcenterNumber,
                        description: oldTask.description,
                        jobId: oldTask.jobId,
                        jobnumber: oldTask.jobnumber,
                        materialId: oldTask.materialId,
                        materialIdentifier: oldTask.materialIdentifier,
                        materialName: oldTask.materialName,
                        pages: oldTask.pages,
                        partjobId: oldTask.partjobId,
                        partjobName: oldTask.partjobName,
                        partjobNumber: oldTask.partjobNumber,
                        timetypeId: oldTask.timetypeId,
                        timetypeName: oldTask.timetypeName,
                        timetypeNumber: oldTask.timetypeNumber
                    };

                    CurrentSheetController._showTaskDialog($mdDialog, ev, BaseUrl, {
                        sheet: $scope.currentSheet.daysheet,
                        task: task
                    }, callback);
                }, function () {
                    CurrentSheetController._showTaskDialog($mdDialog, ev, BaseUrl, {
                        sheet: $scope.currentSheet.daysheet,
                        task: task
                    }, callback);
                });
            } else {
                CurrentSheetController._showTaskDialog($mdDialog, ev, BaseUrl, {
                    sheet: $scope.currentSheet.daysheet,
                    task: task
                }, callback);
            }
        };

        $scope.currentSheet.openTaskEditDialog = function (ev, task) {
            if (activeUser.powers.OFFLINE_WORKING >= 200) {
                $mdDialog.show({
                    controller: AddTaskDialogController,
                    templateUrl: BaseUrl.viewPath + "daysheets/views/addTaskDialog.html",
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    locals: {
                        sheet: $scope.currentSheet.daysheet,
                        task: task
                    }
                })
                    .then(function (answer) {
                        $scope.currentSheet.daysheet = answer.data;
                        localStorage.setItem("activeDaysheet", JSON.stringify(answer.data));
                        Helper.standardToast($mdToast, answer.message);
                    });
            }
        };
    }

}

export default CurrentSheetController.controller